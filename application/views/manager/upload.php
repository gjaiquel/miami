<section class="content-header">
    <h1>
        Administración
        <small>Subir planila</small>
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href=""><i class="fa fa-dashboard"></i> Administración</a></li>
    </ol>
</section>
<section class="content">
    <?php if($upload_page) { ?>
    <div id="upload-box" class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Subir planilla</h3>
            <div class="box-tools pull-right" style="display: none;">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
            Elige un archivo de extensión 'csv' descargado de Reservation.com
            <form class="form" method="post" action="">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Reprocesar existentes</label>
                                <input class="form-control" name="force_reprocess" id="force_reprocess" type="checkbox"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label></label>
                            <input class="form-control file" name="file_upload" id="file_upload" type="file"/>
                        </div>
                    </div>
                </div>
                <div class="alert alert-danger alert-dismissable" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Error</h4>
                    <p>Error Message</p>
                </div>
            </form>
        </div>
        <div class="overlay" style="display: none;">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
    <?php } ?>
    <div id="warning-box" class="box box-warning" style="display: none;">
        <div class="box-header with-border">
          <h3 class="box-title">Confirme los datos siguientes si quiere continuar</h3>
          <div class="box-tools pull-right">
          </div>
        </div><!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive">
            <table class="table no-margin repeated" style="display: none">
              <thead>
                <tr>
                  <th>Reservas repetidas</th>
                  <th>Datos actuales</th>
                  <th>Datos subidos</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
              <br/>
            <table class="table no-margin canceled" style="display: none">
              <thead>
                <tr>
                  <th>Reservas canceladas</th>
                  <th>Depto</th>
                  <th>CheckIn (yyyy-mm-dd)</th>
                  <th>CheckOut (yyyy-mm-dd)</th>
                  <th>Agente</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div><!-- /.table-responsive -->
        </div><!-- /.box-body -->
        <div class="box-footer clearfix">
          <a href="javascript:;" id="continue-btn" class="btn btn-sm btn-warning btn-flat pull-left">Continuar</a>
          <!-- <a href="javascript:;" class="btn btn-sm btn-default btn-flat pull-right">Cancelar</a> -->
        </div><!-- /.box-footer -->
        <div class="overlay" style="display: none;">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
    <div id="success-box" class="box box-success" style="display: none;">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>
          <div class="box-tools pull-right">
          </div>
        </div><!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive">
            <table class="table no-margin">
              <thead>
                <tr>
                  <th style="width: 10%;">Cant.</th>
                  <th>Reservas</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td id="new-count"></td>
                  <td>Nuevas</td>
                  </tr>
                <tr>
                  <td id="updated-count"></td>
                  <td>Actualizadas</td>
                </tr>
                <tr>
                  <td id="processed-count"></td>
                  <td>Procesadas</td>
                </tr>
                <tr>
                  <td id="ignored-count"></td>
                  <td>Sin cambios</td>
                </tr>
            </tbody>
            </table>
          </div><!-- /.table-responsive -->
        </div><!-- /.box-body -->
        <!-- /.box-footer -->
    </div>
    <div id="processed-box" class="box box-<?= $upload_page ? 'box-danger' : ''?>" style="<?= $upload_page ? 'display: none;' : ''?>">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>
          <div class="box-tools pull-right">
          </div>
        </div><!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive">
            <table class="table no-margin no-border">
              <tbody>
                
            </tbody>
            </table>
          </div><!-- /.table-responsive -->
        </div><!-- /.box-body -->
        <!-- /.box-footer -->
        <div class="overlay" style="<?= $upload_page ? 'display: none;' : ''?>">
            <i class="fa fa-refresh fa-spin"></i>
        </div>
    </div>
    
    
</section>
<div id="delete-reservation" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" onclick="app.closeDeleteReservationConfirm()">×</span></button>
          <h4 class="modal-title">Eliminar reserva?</h4>
        </div>
        <div class="modal-body">
          <p></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="app.closeDeleteReservationConfirm();">Cancelar</button>
          <a type="button" class="btn btn-primary" data-reservation="" onclick="app.removeReservation();">Eliminar</a>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<div id="reservation-details" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" onclick="app.closeReservationDetails()">×</span></button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
        </div>
        <div class="overlay" style="display: none;position: absolute;top: 0;left: 0;width: 100%;height: 100%;z-index: 1010;background: rgba(255,255,255,0.7);border-radius: 3px;">
            <i class="fa fa-refresh fa-spin" style="position: absolute;top: 50%;left: 50%;margin-left: -15px;margin-top: -15px;color: #000;font-size: 30px;"></i>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="app.closeReservationDetails()">Cerrar</button>
        </div>
      </div>
    </div>
</div>
<div id="popup">
</div>