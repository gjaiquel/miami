<section class="content-header">
    <h1>
        Usuarios
        <small><?= isset($edit) ? lang('Edit') : lang('Add') ?></small>
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href=""><i class="fa fa-dashboard"></i> Configuración</a></li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Usuarios</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <form class="form" action="" method="post">
                <input name="user_id" type="hidden" value="<?= (isset($edit) && !empty($edit['user_id']) ) ? $edit['user_id'] : '' ?>"/>

                <div class="row">
                    <div class="col-sm-3">

                        <div class="form-group">
                            <label><?= lang('Name') ?></label>
                            <input class="form-control" name="user_firstname" type="text" value="<?= (isset($edit) && $edit['user_firstname'] ) ? $edit['user_firstname'] : '' ?>"/>
                        </div>
                        <div class="form-group">
                            <label><?= lang('Lastname') ?></label>
                            <input class="form-control" name="user_lastname" type="text" value="<?= (isset($edit) && $edit['user_lastname'] ) ? $edit['user_lastname'] : '' ?>"/>
                        </div>
                        <div class="form-group">
                            <label><?= lang('Username') ?></label>
                            <input class="form-control" name="user_name" type="text" value="<?= (isset($edit) && $edit['user_name'] ) ? $edit['user_name'] : '' ?>"/>
                        </div>
                        <div class="form-group">
                            <label><?= lang('Email') ?></label>
                            <input class="form-control" name="user_email" type="text" value="<?= (isset($edit) && $edit['user_email']) ? $edit['user_email'] : '' ?>"/>
                        </div>
                        <div class="form-group">
                            <label>Tipo de Usuario</label>
                            <select class="form-control" name="user_type" required="">
                                <option value="" <?= isset($edit) ? '' : 'selected' ?>>Seleccionar</option>
                                <option value="admin" <?= (isset($edit) && $edit['user_type'] === 'admin') ? 'selected' : '' ?>>Administrador</option>
                                <option value="guest" <?= (isset($edit) && $edit['user_type'] === 'guest') ? 'selected' : '' ?>>Invitado</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label><?= lang('Password') ?></label>
                            <input class="form-control" name="user_password" type="password" value=""/>
                        </div>
                        <div class="form-group">
                            <label><?= lang('Password Repeat') ?></label>
                            <input class="form-control" name="user_password_repeat" type="password" value=""/>
                        </div>
                    </div>
                </div>
                <?if(!empty($errors)){?>
                <div class="clearfix">
                    <div class="alert alert-danger">
                        <?=$errors?>
                    </div>
                </div>
                <?}?>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="pull-right">
                            <button class="btn btn-primary"><?= isset($edit) ? lang('Save') : lang('Add') ?></button>
                            <a class="btn" href="<?= site_url('manager/users') ?>"><?= lang('Cancel') ?></a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>