<section class="content-header">
    <h1>
        Departamentos
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href=""><i class="fa fa-dashboard"></i> Configuración</a></li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Lista</h3>
            <?php if($this->session->userdata('user_type') === 'admin') { ?>
            <div class="box-tools pull-right">
                <a href="./apartment" class="btn btn-block btn-primary">Nuevo Departamento</a>
            </div><!-- /.box-tools -->
            <?php } ?>
        </div><!-- /.box-header -->
        <div class="box-body">
            <?php
            if (!empty($list)) {
            ?>
            <table class="table table-bordered table-hover datatable_noconfig">
                <thead style="background-color: #dcdcdc">
                    <tr class="top">
                        <th>ID</th>
                        <th>Nombre en Reservation</th>
                        <th>Nombre corto</th>
                        <th>Propietario</th>
                        <th>Check In</th>
                        <?php if($this->session->userdata('user_type') === 'admin') { ?>
                        <th>Acciones</th>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($list as $apartment) {
                    ?>
                        <tr class="reg_<?= $apartment['apartment_id'] ?>">
                            <td><?= $apartment['apartment_id'] ?></td>
                            <td><?= $apartment['apartment_reservation_name'] ?></td>
                            <td><?= $apartment['apartment_short_name'] ?></td>
                            <td><?= $apartment['apartment_agent_name'] ?></td>
                            <td><?= $apartment['apartment_agent_name_checkin'] ?></td>
                            <?php if($this->session->userdata('user_type') === 'admin') { ?>
                            <td>
                                <a href="<?= site_url('manager/apartment/' . $apartment['apartment_id']) ?>" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></a>&nbsp;
                                <?php if($apartment['apartment_active']) { ?>
                                <a href="<?= site_url('manager/apartment_active/' . $apartment['apartment_id']) . '/0' ?>" style="color:green;" data-toggle="tooltip" data-placement="top" title="Desactivar"><i class="fa fa-check-circle-o" aria-hidden="true"></i></a>
                                <?php } else { ?>
                                <a href="<?= site_url('manager/apartment_active/' . $apartment['apartment_id']) . '/1' ?>" style="color:red;" data-toggle="tooltip" data-placement="top" title="Activar"><i class="fa fa-times-circle-o" aria-hidden="true"></i></a>
                                <?php } ?>
                                &nbsp;<a href="javascript:;" style="color:red;" onclick="app.deleteApartmentConfirm(<?=$apartment['apartment_id']?>, '<?= $apartment['apartment_short_name'] ?>')" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-trash"></i></a>
                            </td>
                            <?php } ?>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
            <?php
            } else {
                echo '<p>No hay departamentos creados</p>';
            }
            ?>
        </div><!-- /.box-body -->
    </div><!-- /.box -->


</section>
<div id="delete-apartment" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" onclick="app.closeDeleteApartmentConfirm()">×</span></button>
          <h4 class="modal-title">Eliminar departamento?</h4>
        </div>
        <div class="modal-body">
          <p></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="app.closeDeleteApartmentConfirm()">Cancelar</button>
          <a type="button" class="btn btn-primary" href="">Eliminar</a>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>