<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!--[if gte mso 9]>
    <style>
      .column-top {
        mso-line-height-rule: exactly !important;
      }
    </style>
    <![endif]-->
  <meta name="robots" content="noindex,nofollow" />
<meta property="og:title" content="My First Campaign" />
</head>
  <body style="margin: 0;mso-line-height-rule: exactly;padding: 0;min-width: 100%;background-color: #fbfbfb">
      <table class="table no-border" style="width: 100%;max-width: 100%;margin-bottom: 20px;border-spacing: 0px;border-collapse: collapse;">
    <tbody>
        <tr>
            <td colspan="2" style="text-align: left;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;">
                Resumen de cuenta: <strong><?= $agent['agent_fullname'] ?></strong><br/>
                Fecha: <strong><?= $from ?> al <?= $to ?></strong>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;"><strong>SALDO ANTERIOR</strong></td>
            <td style="text-align: right; width: 10px;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;"><strong style="color: <?= $total_before >= 0 ? 'black':'red' ?>"><?= number_format($total_before, 2, ',', '.') ?></strong></td>
        </tr>
    </tbody>
</table>
<br/>
<?php
if (!empty($details)) { 
    $tableReservations = false;
    $tableExtras = false;
    $extras_subtotal = 0;
    $res_subtotal = 0;
    foreach ($details as $detail) {
        // type_id: 3 = Seña
        if($detail['reservation_number'] === NULL || $detail['type_id'] == 3) {
            $tableExtras = true;
        } else {
            $tableReservations = true;
        }
    }
?>
<?php if($tableReservations) { ?>
<table class="table table-bordered" style="width: 100%;max-width: 100%;margin-bottom: 20px;border-spacing: 0px;border-collapse: collapse;">
    <tbody>
    <tr>
      <th style="width: 10px;border: 1px solid #ddd;background-color: #dcdcdc;line-height: 1.42857143;padding: 8px;">Dpto</th>
      <th style="width: 10px;border: 1px solid #ddd;background-color: #dcdcdc;line-height: 1.42857143;padding: 8px;">Checkin</th>
      <th style="width: 10px;border: 1px solid #ddd;background-color: #dcdcdc;line-height: 1.42857143;padding: 8px;">Checkout</th>
      <th style="width: 10px;border: 1px solid #ddd;background-color: #dcdcdc;line-height: 1.42857143;padding: 8px;">Noches</th>
      <th style="text-align: left;border: 1px solid #ddd;background-color: #dcdcdc;line-height: 1.42857143;padding: 8px;">Detalle</th>
      
      <th style="width: 100px;border: 1px solid #ddd;background-color: #dcdcdc;line-height: 1.42857143;padding: 8px;">Monto</th>
    </tr>
    <?php
    foreach ($details as $detail) {
        if($detail['reservation_number'] !== NULL && $detail['type_id'] != 3) {
            $res_subtotal += $detail['amount'];
    ?>
    <tr>
      <td style="width: 10px;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;"><?= $detail['apartment_short_name'] ?></td>
      <td style="width: 10px;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;"><?= $detail['arrival_date'] ?></td>
      <td style="width: 10px;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;"><?= $detail['departure_date'] ?></td>
      <td style="width: 10px;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;"><?= $detail['number_of_nights'] ?></td>
      <td style="padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;"><?= $detail['detail'] ?></td>
      
      <td style="text-align:right;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;"><strong style="color: <?= $detail['amount'] >= 0 ? 'black':'red' ?>"><?= number_format($detail['amount'], 2, ',', '.') ?></strong></td>
    </tr>
    <?php
        }
    }
    ?>
    <tr>
        <td colspan="5" style="text-align: right;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;">TOTAL DE RENTAS LIBRE DE GASTOS</td>
        <td style="text-align: right;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;"><strong style="color: <?= $res_subtotal >= 0 ? 'black':'red' ?>"><?= number_format($res_subtotal, 2, ',', '.') ?></strong></td>
    </tr>
  </tbody>
</table>
<br/>
<?php } ?>
<?php if($tableExtras) { ?>
<table class="table table-bordered" style="width: 100%;max-width: 100%;margin-bottom: 20px;border-spacing: 0px;border-collapse: collapse;">
    <tbody>
    <tr>
      <th style="width: 10px;border: 1px solid #ddd;background-color: #dcdcdc;line-height: 1.42857143;padding: 8px;">Fecha</th>
      <th style="border: 1px solid #ddd;background-color: #dcdcdc;line-height: 1.42857143;padding: 8px;">Tipo</th>
      <th style="text-align: left;border: 1px solid #ddd;background-color: #dcdcdc;line-height: 1.42857143;padding: 8px;">Detalle</th>
      
      <th style="width: 100px;border: 1px solid #ddd;background-color: #dcdcdc;line-height: 1.42857143;padding: 8px;">Monto</th>
    </tr>
    <?php
    foreach ($details as $detail) { 
        if($detail['reservation_number'] === NULL || $detail['type_id'] == 3) {
            $extras_subtotal += $detail['amount'];
    ?>
    <tr>
      <td style="width: 10px;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;"><?= $detail['date'] ?></td>
      <td style="width: 10px;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;"><?= $detail['type_name'] ?></td>
      <td style="padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;"><?= $detail['detail'] ?></td>
      
      <td style="text-align:right;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;"><strong style="color: <?= $detail['amount'] >= 0 ? 'black':'red' ?>"><?= number_format($detail['amount'], 2, ',', '.') ?></strong></td>
    </tr>
    <?php
        }
    } ?>
    <tr>
        <td colspan="3" style="text-align: right;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;">TOTAL GASTOS</td>
        <td style="text-align: right;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;"><strong style="color: <?= $extras_subtotal >= 0 ? 'black':'red' ?>"><?= number_format($extras_subtotal, 2, ',', '.') ?></strong></td>
    </tr>
  </tbody>
</table>
<br/>
<?php } ?>
<table class="table no-border" style="width: 100%;max-width: 100%;margin-bottom: 20px;border-spacing: 0px;border-collapse: collapse;">
    <tbody>
        <tr>
            <td style="text-align: right;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;"><strong>TOTAL</strong></td>
            <td style="text-align: right; width: 10px;padding: 8px;line-height: 1.42857143;vertical-align: top;border: 1px solid #ddd;"><strong style="color: <?= $total_before + $extras_subtotal + $res_subtotal >= 0 ? 'black':'red' ?>"><?= number_format($total_before + $extras_subtotal + $res_subtotal, 2, ',', '.') ?></strong></td>
        </tr>
    </tbody>
</table>
<?php } ?>

  
</body></html>
