<script defer src="<?= base_url(MAN_JS . 'libs/fileinput_bootstrap.min.js') ?>"></script>
<script>
    app = app || {};
    app.uploadPage = <?= $upload_page ?>;
    
    $(document).ready(function () {
        if(app.uploadPage) {
            $("#file_upload").fileinput({
                uploadUrl: app.baseUrl + "manager/csv_upload",
                maxFileSize: 5000,
                showCaption: true,
                showPreview: true,
                maxFileCount: 1,
                autoReplace: true,
                allowedFileTypes: ['text'],
                allowedFileExtensions: ['csv'],
                initialPreviewFileType: 'text'

            });

            $('#file_upload').on('fileuploaded', function (event, data, previewId, index) {
                var overlay = $('#upload-box .overlay');
                overlay.show();
                if(data.response.success && data.response.success === true) {
                    $.ajax({
                        url: '<?= base_url('manager/csv_to_db') ?>',
                        data: {file_name: data.response.file_name, force_reprocess: $('#force_reprocess').prop('checked') ? 1 : 0},
                        type: 'POST',
                        dataType: 'json'
                    }).success(function(data){
                        if (data.success && data.success === true) {
                            if((data.repeated && data.repeated > 0) || (data.canceled && data.canceled > 0)) {
                                app.showReservationWarnings(data);
                            } else {
                                app.showSuccessBox(data, true);
                            }
                        } else {
                            app.showErrorMessage(data.error || 'Error al procesar el archivo, intenta de nuevo por favor', '#upload-box');
                        }
                    }).fail(function() {
                        app.showErrorMessage('Error al procesar el archivo, intenta de nuevo por favor', '#upload-box');
                    }).always(function(){
                        overlay.hide();
                    });
                } else {
                    app.showErrorMessage(data.response.error || 'Error al subir el archivo, intenta de nuevo por favor', '#upload-box');
                }
            });
        } else {
            $.ajax({
                url: '<?= base_url('manager/get_process_accounts') ?>',
                type: 'GET',
                dataType: 'json'
            }).success(function(data) {
                if (data.success && data.success === true) {
                    $('#processed-box').find('h3').text(data.processed + (data.processed !== 1 ? ' Reservas procesadas' : ' Reserva procesada' ));
                    $('#processed-box').find('tbody').append('<tr><td>Todas las reservas han sido procesadas</td></tr>');
                } else {
                    app.showReservationsErrorMessages(data);
                }
            }).always(function(){
                $('#processed-box .overlay').hide();
            });
        }

    });
    
    $('#continue-btn').on('click', function() {
        $('#warning-box .overlay').show();
        $('#continue-btn').unbind('click');
        $('#continue-btn').removeClass('btn-warning');
        $('#continue-btn').addClass('btn-default');
        var force_reprocess = $('#force_reprocess').prop('checked') ? 1 : 0
        
        $.ajax({
            url: '<?= base_url('manager/overwrite_reservations') ?>/' + force_reprocess,
            type: 'GET',
            dataType: 'json'
        }).success(function(data) {
            if (data.success && data.success === true) {
                app.showSuccessBox(data, false);
            } else {
                app.showErrorMessage("Error al procesar los datos, intenta de nuevo por favor.", '#warning-box');
            }
        }).always(function(){
            $('#warning-box .overlay').hide();
        });
    });
    
    app.showReservationWarnings = function(data) {
        var uploadBox = $('#upload-box');
        uploadBox.find('.btn.btn-box-tool:first').click();
        
        app.drawWarningsTables(data);
        $('#warning-box').show();
    };
    
    app.drawWarningsTables = function(data) {
        var i = 0;
        var tableRows = '';
        var repeatedTable = $('#warning-box').find('.repeated');
        var canceledTable = $('#warning-box').find('.canceled');
        var row = '';
        if(data.repeated > 0) {
            for(i=0; i < data.repeated; i++) {
                var res = data.reservations[i];
                var tmp = data.temporal[i];
                row = '<tr><td>' + tmp['reservation_number'] + '</td>';
                var tdData1 = '';
                var tdData2 = '';
                var notesData;
                Object.keys(tmp).forEach(function(k) {
                    if(k === 'reservation_notes') {
                        notesData = app.compareNotesData(res[k], tmp[k]);
                        notesData[0] = notesData[0].length > 0 ? '<strong>reservation_notes:</strong></br>' + notesData[0] : notesData[0];
                        notesData[1] = notesData[1].length > 0 ? '<strong>reservation_notes:</strong></br>' + notesData[1] : notesData[1];
                    } else if(res[k].trim() !== tmp[k].trim()) {
                        if(tdData1.length > 0) {
                            tdData1 = tdData1 + '</br>';
                        }
                        if(tdData2.length > 0) {
                            tdData2 = tdData2 + '</br>';
                        }
                        console.log(res[k].trim().length);
                        console.log(tmp[k].trim().length);
                        tdData1 = tdData1 + '<strong>'+k+':</strong> '+res[k];
                        tdData2 = tdData2 + '<strong>'+k+':</strong> '+tmp[k];
                    }
                });
                tdData1 = '<td>' + tdData1 + (tdData1.length > 0 ? '</br>' : '' ) + notesData[0] + '</td>';
                tdData2 = '<td>' + tdData2 + (tdData2.length > 0 ? '</br>' : '' ) + notesData[1] + '</td>';
                tableRows += row + tdData1 + tdData2 + '</tr>';
            }
            repeatedTable.find('tbody').append(tableRows);
            repeatedTable.show();
        }
        if(data.canceled > 0) {
            tableRows = '';
            for(i=0; i < data.canceled; i++) {
                var res = data.reservations_canceled[i];
                row = '<tr><td>' + res['reservation_number'] + '</td>';
                row += '<td>'+res['room']+'</td>';
                row += '<td>'+res['arrival_date']+'</td>';
                row += '<td>'+res['departure_date']+'</td>';
                row += '<td>'+res['second_guest_first_name']+'</td></tr>';
                tableRows += row;
            }
            canceledTable.find('tbody').append(tableRows);
            canceledTable.show();
        }
    };
    
    app.compareNotesData = function(res, tmp) {
        var data1 = '';
        var data2 = '';
        var isJson1 = typeof res === 'object';
        var isJson2 = typeof tmp === 'object';
        if(!isJson1 && !isJson2 && res !== tmp) {
            return [res, tmp];
        }
        if(isJson1 !== isJson2) {
            var json = isJson1 ? res : tmp;
            var aux = '';
            Object.keys(json).forEach(function(k) {
                if(aux.length > 0) {
                    aux = aux + '</br>';
                }
                if(Array.isArray(json[k])) {
                    for(var index = 0; index < json[k].length; index++) {
                        var obj = json[k][index];
                        aux = aux + '<strong>'+k+' ' + (index+1) + ':</strong>';
                        Object.keys(obj).forEach(function(j) {
                            aux = aux + '</br><strong> - '+j+':</strong> '+obj[j];
                        });
                    }
                } else {
                    aux = aux + '<strong>'+k+':</strong> '+json[k];
                }
            });
            aux = '</br>' + aux;
            if(isJson1) {
                return [aux, tmp];
            } else {
                return [res, aux];
            }
        }
        var aux1 = '';
        var aux2 = '';
        Object.keys(tmp).forEach(function(k) {
            aux1 = '';
            aux2 = '';
            if(Array.isArray(tmp[k])) {
                if(tmp[k].length > res[k].length) {
                    var index = 0;
                    for(index = 0; index < res[k].length; index++) {
                        var resObj = res[k][index];
                        var tmpObj = tmp[k][index];
                        var titleAdded = false;
                        Object.keys(resObj).forEach(function(j) {
                            if(resObj[j] !== tmpObj[j]) {
                                if(!titleAdded) {
                                    aux1 = '<strong>' + k + ' ' + (index+1) + ':</strong>';
                                    aux2 = '<strong>' + k + ' ' + (index+1) + ':</strong>';
                                    titleAdded = true;
                                }
                                aux1 = aux1 + '</br><strong> - ' + j + ':</strong> ' + resObj[j];
                                aux2 = aux2 + '</br><strong> - ' + j + ':</strong> ' + tmpObj[j];
                            }
                        });
                    }
                    for(index = res[k].length; index < tmp[k].length; index++) {
                        var tmpObj = tmp[k][index];
                        aux1 = aux1 + (aux2.length > 0 ? '</br>' : '' ) + '</br>';
                        aux2 = aux2 + (aux2.length > 0 ? '</br>' : '' ) + '<strong>' + k + ' ' + (index+1) + ':</strong>';
                        Object.keys(tmpObj).forEach(function(j) {
                            aux1 = aux1 + '</br>';
                            aux2 = aux2 + '</br><strong> - ' + j + ':</strong> ' + tmpObj[j];
                        });
                    }
                    data1 += (data1.length > 0 ? '</br>' : '') + aux1;
                    data2 += (data2.length > 0 ? '</br>' : '') + aux2;
                } else {
                    var index = 0;
                    for(index = 0; index < tmp[k].length; index++) {
                        var resObj = res[k][index];
                        var tmpObj = tmp[k][index];
                        var titleAdded = false;
                        Object.keys(tmpObj).forEach(function(j) {
                            if(resObj[j] !== tmpObj[j]) {
                                if(!titleAdded) {
                                    aux1 = '<strong>' + k + ' ' + (index+1) + ':</strong>';
                                    aux2 = '<strong>' + k + ' ' + (index+1) + ':</strong>';
                                    titleAdded = true;
                                }
                                aux1 = aux1 + '</br><strong> - ' + j + ':</strong> ' + resObj[j];
                                aux2 = aux2 + '</br><strong> - ' + j + ':</strong> ' + tmpObj[j];
                            }
                        });
                    }
                    for(index = tmp[k].length; index < res[k].length; index++) {
                        var resObj = res[k][index];
                        aux1 = aux1 + (aux1.length > 0 ? '</br>' : '' ) + '<strong>' + k + ' ' + (index+1) + ':</strong>';
                        aux2 = aux2 + (aux1.length > 0 ? '</br>' : '' );
                        Object.keys(resObj).forEach(function(j) {
                            aux1 = aux1 + '</br><strong> - ' + j + ':</strong> ' + resObj[j];
                            aux2 = aux2 + '</br>';
                        });
                    }
                    data1 += (data1.length > 0 ? '</br>' : '') + aux1;
                    data2 += (data2.length > 0 ? '</br>' : '') + aux2;
                }
            } else if((k in res) && res[k] !== tmp[k]) {
                if(data1.length > 0) {
                    data1 = data1 + '</br>';
                }
                if(data2.length > 0) {
                    data2 = data2 + '</br>';
                }
                data1 = data1 + '<strong>' + k + ':</strong> ' + res[k];
                data2 = data2 + '<strong>' + k + ':</strong> ' + tmp[k];
            } else if(!(k in res)) {
                if(data1.length > 0) {
                    data1 = data1 + '</br>';
                }
                if(data2.length > 0) {
                    data2 = data2 + '</br>';
                }
                data1 = data1 + '<strong></strong>';
                data2 = data2 + '<strong>' + k + ':</strong> ' + tmp[k];
            }
            
        });
        return [data1, data2];
    };
        
    
    app.showSuccessBox = function(data, hideFileUploader) {
        console.dir(data);
        var successBox = $('#success-box');
        $('#new-count').text(data.inserted);
        $('#updated-count').text(data.updated);
        $('#ignored-count').text(data.ignored);
        $('#processed-count').text(data.processed.processed);
        if(data.total === 1) {
            successBox.find('h3').text(data.total + ' Reserva importada correctamente');
        } else {
            successBox.find('h3').text(data.total + ' Reservas importadas correctamente');
        }
        var uploadBox = $('#upload-box');
        if(hideFileUploader) {
            uploadBox.find('.btn.btn-box-tool:first').click();
        }
        app.showReservationsErrorMessages(data.processed);
        successBox.show();
    };
    
    app.showErrorMessage = function(text, box) {
        var el = $(box + ' .box-body');
        el.append('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><p><i class="icon fa fa-ban"></i><b>'+text+'</b></p></div>');
    };
    
    app.showReservationsErrorMessages = function(data) {
        var processedBox = $('#processed-box');
        if(!data.success) {
            app.updateErrorMessagesTitle(data.errors);
            var lastRes = 0;
            for(var i = 0; i < data.errors; i++) {
                var msg = data.messages[i];
                var reservation = msg.substring(msg.indexOf(' ') + 1, msg.indexOf('.'));
                msg = msg.substring(msg.indexOf('.') + 2, msg.length);
                var row = '';
                if(lastRes === reservation) {
                    row = '<tr data-res="' + reservation + '"><td style="padding: 0px 8px;">' + msg + '</td></tr>';
                } else {
                    row = '<tr data-res="' + reservation + '"><td style="padding: 12px 8px 2px 8px;"><strong><a style="cursor:pointer;" onclick="app.showReservationDetails('+reservation+')">Reserva ' + reservation + '</a></strong> <a style="cursor:pointer;color:red;" onclick="app.deleteReservationConfirm('+reservation+')"><i class="fa fa-trash"></i></a></td></tr>';
                    row += '<tr data-res="' + reservation + '"><td style="padding: 0px 8px;">' + msg + '</td></tr>';
                }
                lastRes = reservation;
                processedBox.find('tbody').append(row);
            }
            processedBox.show();
        }
    };
    
    app.updateErrorMessagesTitle = function(errorCount) {
        var processedBox = $('#processed-box');
        if(errorCount < 0) {
            errorCount = parseInt(processedBox.find(".error-count").text()) + errorCount;
        }
        if(errorCount === 1) {
            processedBox.find('h3').html('Se encontró <span class="error-count">' + errorCount + '</span> error');
        } else {
            processedBox.find('h3').html('Se encontraron <span class="error-count">' + errorCount + '</span> errores');
        }
    };
    
    app.deleteReservationConfirm = function(resId) {
        var modal = $('#delete-reservation');
        modal.find('.modal-body p').text('Seguro quiere eliminar la reserva ' + resId);
        modal.find('.btn-primary').attr('data-reservation', resId);
        modal.show();
    };
    app.closeDeleteReservationConfirm = function() {
        var modal = $('#delete-reservation');
        modal.find('.modal-body p').text('');
        modal.find('.btn-primary').attr('data-reservation', '');
        modal.hide();
    };
    app.removeReservation = function() {
        var modal = $('#delete-reservation');
        var res = modal.find('.btn-primary').attr('data-reservation');
        $.ajax({
            url: '<?= base_url('manager/delete_reservation_by_number') ?>',
            data: {reservation: res},
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                if (data.status && data.status === 'success') {
                    var reduce = 0 - $('tr[data-res='+res+']').size() + 1;
                    $('tr[data-res='+res+']').remove();
                    app.updateErrorMessagesTitle(reduce);
                    app.closeDeleteReservationConfirm();
                    app.showPopup('alert-success', 'Éxito', 'La reserva ha sido eliminada.');
                } else {
                    app.closeDeleteReservationConfirm();
                    app.showPopup('alert-danger', 'Error', 'No se pudo eliminar la reserva.');
                }
            }
        });
    };
    app.showPopup = function(clazz, title, text) {
        var html = '<div class="alert '+clazz+' alert-dismissable" id="success-popup" style="padding: 10px;position: fixed;top: 70px;right: 10px;width: 300px;z-index: 99999;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="position: initial;">×</button><h4>	<i class="icon fa fa-check"></i> '+title+'</h4>'+text+'</div>';
        $('#popup').html(html);
    };
</script>