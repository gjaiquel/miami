<section class="content-header">
    <h1>
        Departamentos
        <small><?= isset($edit) ? lang('Edit') : lang('Add') ?></small>
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href=""><i class="fa fa-dashboard"></i> Configuración</a></li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Departamentos</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <form class="form" action="" method="post">
                <input name="apartment_id" type="hidden" value="<?= (isset($edit) && !empty($edit['apartment_id']) ) ? $edit['apartment_id'] : '' ?>"/>

                <div class="row">
                    <div class="col-sm-3">

                        <div class="form-group">
                            <label>Numero en Reservation</label>
                            <input class="form-control" name="reservation_name" type="text" value="<?= (isset($edit) && $edit['apartment_reservation_name'] ) ? $edit['apartment_reservation_name'] : '' ?>"/>
                        </div>
                        <div class="form-group">
                            <label>Numero departamento</label>
                            <input class="form-control" name="short_name" type="text" value="<?= (isset($edit) && $edit['apartment_short_name'] ) ? $edit['apartment_short_name'] : '' ?>"/>
                        </div>
                        <div class="form-group">
                            <label>Propietario</label>
                            <select name="agent_id" class="form-control">
                                <option value="">Seleccionar</option>
                                <?php
                                    foreach($owners as $owner) {
                                        $selected = (isset($edit) && $edit['apartment_agent_id'] && $edit['apartment_agent_id'] === $owner['agent_id']) ? ' selected' : '';
                                        echo '<option value="'.$owner['agent_id'].'" '.$selected.'>'.$owner['agent_fullname'].'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Agente Check In</label>
                            <select name="agent_checkin" class="form-control">
                                <option value="">Seleccionar</option>
                                <?php
                                    foreach($agents as $agent) {
                                        $selected = (isset($edit) && $edit['apartment_agent_checkin'] && $edit['apartment_agent_checkin'] === $agent['agent_id']) ? ' selected' : '';
                                        echo '<option value="'.$agent['agent_id'].'" '.$selected.'>'.$agent['agent_fullname'].'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <?php if(!empty($errors)){ ?>
                <div class="clearfix">
                    <div class="alert alert-danger">
                        <?= $errors ?>
                    </div>
                </div>
                <?php } ?>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="pull-right">
                            <button class="btn btn-primary"><?= isset($edit) ? 'Guardar' : 'Agregar' ?></button>
                            <a class="btn" href="<?= site_url('manager/apartments') ?>">Cancelar</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>