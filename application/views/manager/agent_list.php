<section class="content-header">
    <h1>
        Agentes
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href=""><i class="fa fa-dashboard"></i> Configuración</a></li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Lista</h3>
            <?php if($this->session->userdata('user_type') === 'admin') { ?>
            <div class="box-tools pull-right">
                <a href="./agent" class="btn btn-block btn-primary">Nuevo Agente</a>
            </div><!-- /.box-tools -->
            <?php } ?>
        </div><!-- /.box-header -->
        <div class="box-body">
            <?php
            if (!empty($list)) {
            ?>
            <table class="table table-bordered table-hover datatable_noconfig">
                <thead style="background-color: #dcdcdc">
                    <tr class="top">
                        <th><?= lang('Id') ?></th>
                        <th style="width: 10px;"></th>
                        <th><?= lang('Nombre y Apellido') ?></th>
                        <th><?= lang('Email') ?></th>
                        <th><?= lang('Keyword') ?></th>
                        <th><?= lang('Type') ?></th>
                        <th><?= lang('Predefined') ?></th>
                        <?php if($this->session->userdata('user_type') === 'admin') { ?>
                        <th><?= lang('Actions') ?></th>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($list as $agent) {
                    ?>
                        <tr class="reg_<?= $agent['agent_id'] ?>">
                            <td><?= $agent['agent_id'] ?></td>
                            <td>
                                <?php if(!empty($agent['agent_note']) && strlen($agent['agent_note']) > 0) { ?>
                                <a tabindex="0" class="btn btn-sm btn-note-popover" style="padding:0px;border:0px;" role="button" data-placement="top" data-toggle="popover" data-trigger="focus" title="Nota" data-content="<?= $agent['agent_note'] ?>"></a>
                                <?php } ?>
                            </td>
                            <td><?= $agent['agent_firstname'] . ' ' . $agent['agent_lastname'] ?></td>
                            <td><?= $agent['agent_email'] ?></td>
                            <td><?= $agent['agent_keyword'] ?></td>
                            <td><?= $agent['agent_type'] ?></td>
                            <td><?= $agent['agent_predefined_value'] ?></td>
                            <?php if($this->session->userdata('user_type') === 'admin') { ?>
                            <td>
                                <a href="<?= site_url('manager/agent/' . $agent['agent_id']) ?>" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></a>&nbsp;
                                <?php if($agent['agent_active']) { ?>
                                <a href="<?= site_url('manager/agent_active/' . $agent['agent_id']) . '/0' ?>" style="color:green;" data-toggle="tooltip" data-placement="top" title="Desactivar"><i class="fa fa-check-circle-o" aria-hidden="true"></i></a>
                                <?php } else { ?>
                                <a href="<?= site_url('manager/agent_active/' . $agent['agent_id']) . '/1' ?>" style="color:red;" data-toggle="tooltip" data-placement="top" title="Activar"><i class="fa fa-times-circle-o" aria-hidden="true"></i></a>
                                <?php } ?>
                                &nbsp;<a href="javascript:;" style="color:red;" onclick="app.deleteAgentConfirm(<?=$agent['agent_id']?>, '<?= $agent['agent_firstname'] . ' ' . $agent['agent_lastname'] ?>')" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-trash"></i></a>
                            </td>
                            <?php } ?>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
            <?php
            } else {
                echo '<p>No hay agentes creados</p>';
            }
            ?>
        </div><!-- /.box-body -->
    </div><!-- /.box -->


</section>
<div id="delete-agent" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" onclick="app.closeDeleteAgentConfirm()">×</span></button>
          <h4 class="modal-title">Eliminar agente?</h4>
        </div>
        <div class="modal-body">
          <p></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="app.closeDeleteAgentConfirm()">Cancelar</button>
          <a type="button" class="btn btn-primary" href="">Eliminar</a>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>