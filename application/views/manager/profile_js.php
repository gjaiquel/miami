<script defer src="<?= base_url(MAN_JS . 'libs/fileinput_bootstrap.min.js') ?>"></script>
<script>

    $(document).ready(function () {
    });
    
    $('.form').submit(function(event){
        var user_firstname = $('[name="user_firstname"]').val();
        var user_lastname = $('[name="user_lastname"]').val();
        var user_password = $('[name="user_password"]').val();
        var user_password_repeat = $('[name="user_password_repeat"]').val();
        var password_len = $.trim(user_password).length;
        
        var user_name = $('[name="user_name"]').val();
        var user_email = $('[name="user_email"]').val();
        
        var success = true;
        if($.trim(user_firstname) === ''){
            $('.form-group:has(input[name="user_firstname"])').addClass('has-error');
            success = false;
        } else {
            $('.form-group:has(input[name="user_firstname"])').removeClass('has-error');
        }
        
        if($.trim(user_lastname) === ''){
            $('.form-group:has(input[name="user_lastname"])').addClass('has-error');
            success = false;
        } else {
            $('.form-group:has(input[name="user_lastname"])').removeClass('has-error');
        }
        
        if($.trim(user_name) === ''){
            $('.form-group:has(input[name="user_name"])').addClass('has-error');
            success = false;
        } else {
            $('.form-group:has(input[name="user_name"])').removeClass('has-error');
        }
        
        if($.trim(user_email) === ''){
            $('.form-group:has(input[name="user_email"])').addClass('has-error');
            success = false;
        } else {
            $('.form-group:has(input[name="user_email"])').removeClass('has-error');
        }
        
        if( ($.trim(user_password) !== '' || $.trim(user_password_repeat) !== '') && (password_len < 6 || $.trim(user_password) !== $.trim(user_password_repeat))){
            success = false;
            if(password_len < 6){
                appendErrorMessage( $('.form-group:has(input[name="user_password"])'), 'La contraseña debe tener al menos 6 caracteres' );
            }
            if($.trim(user_password) !== $.trim(user_password_repeat)){
                appendErrorMessage( $('.form-group:has(input[name="user_password_repeat"])'), 'Las contraseñas no coinciden' );
            }
        }
        
        if(success === false){
            event.preventDefault();
        }
    });
    
    $('input[name="user_password"]').blur(function(){
        var text = $.trim( $(this).val() );
        var len = text.length;
        if(len === 0){
            removeLabelMessage($('.form-group:has(input[name="user_password"])'));
        } else if( len < 6 ){
            appendErrorMessage( $('.form-group:has(input[name="user_password"])'), '<?= lang('Password must has at least 6 characters')?>' );
        } else {
            appendSuccessMessage( $('.form-group:has(input[name="user_password"])'), 'Ok' );
        }
    });
    
    $('input[name="user_password_repeat"]').blur(function(){
        var text = $.trim( $(this).val() );
        var len = text.length;
        var pass = $.trim( $('input[name="user_password"]').val() );
        if(len === 0){
            removeLabelMessage($('.form-group:has(input[name="user_password_repeat"])'));
        } else if( text !== pass ){
            appendErrorMessage( $('.form-group:has(input[name="user_password_repeat"])'), '<?= lang('Passwords must match each other')?>' );
        } else {
            appendSuccessMessage( $('.form-group:has(input[name="user_password_repeat"])'), 'Ok' );
        }
    });
    
    appendSuccessMessage = function(element, msg) {
        element.removeClass('has-error');
        element.addClass('has-success');
        element.find('.control-label').remove();
        element.append('<label class="control-label" for="inputSuccess"><i class="fa fa-check"></i> '+msg+'</label>');
    };
    
    removeLabelMessage = function(element) {
        element.find('.control-label').remove();
        element.removeClass('has-error');
        element.removeClass('has-success');
    };
    
    appendErrorMessage = function(element, msg) {
        element.removeClass('has-success');
        element.addClass('has-error');
        element.find('.control-label').remove();
        element.append('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> '+msg+'</label>');
    };
    
    showPasswordFields = function() {
        $('.row.password').show();
        $('.row.password-link').hide();
    };
</script>