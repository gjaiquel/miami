<section class="content-header">
    <h1>
        Cuentas
        <small><?= isset($edit) ? 'Editar movimiento' : 'Cargar movimiento' ?></small>
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href=""><i class="fa fa-dashboard"></i> Administración</a></li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><?= isset($edit) ? 'Editar movimiento' : 'Cargar movimiento' ?></h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <form class="form" action="" method="post">
                <input name="detail_id" type="hidden" value="<?= (isset($edit) && !empty($edit['id']) ) ? $edit['id'] : '' ?>"/>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Fecha</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input name="detail_date" type="text" class="form-control datepicker" value="<?= (isset($edit) && $edit['date'] ) ? $edit['date'] : '' ?>" data-date-format="dd/mm/yyyy">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Agente</label>
                            <select name="detail_agent_id" class="form-control" <?= (isset($agent_id) && $agent_id > 0 ) ? 'disabled' : '' ?>>
                                <option value="">Seleccionar</option>
                                <?php
                                    foreach($agents as $agent) {
                                        $selected = ((isset($edit) && $edit['agent_id'] && $edit['agent_id'] === $agent['agent_id']) || (isset($agent_id) && $agent_id === $agent['agent_id'])) ? ' selected' : '';
                                        echo '<option value="'.$agent['agent_id'].'" '.$selected.'>'.$agent['agent_fullname'].' - '.$agent['agent_type'].'</option>';
                                    }
                                ?>
                            </select>
                            <?php
                            if(isset($agent_id) && $agent_id > 0 ){
                            ?>
                            <input name="detail_agent_id" type="hidden" value="<?= $agent_id ?>"/>
                            <?php
                            }
                            ?>
                        </div>
                        <?php if(!isset($edit) && (isset($agent_id) && $agent_id === 0)) { ?>
                        <div class="form-group">
                            <label>Para</label>
                            <select name="detail_agent_id_to" class="form-control">
                                <option value="">Seleccionar</option>
                                <?php
                                    foreach($agents as $agent) {
                                        echo '<option value="'.$agent['agent_id'].'" '.$selected.'>'.$agent['agent_fullname'].' - '.$agent['agent_type'].'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                        <?php } ?>
                        <div class="form-group" style="display: none;">
                            <label>Número Reserva</label>
                            <input type="number" class="form-control" name="reservation_number" type="text" value="<?= (isset($edit) && $edit['reservation_number'] ) ? $edit['reservation_number'] : '' ?>"/>
                        </div>
                        <div class="form-group">
                            <label>Monto</label>
                            <input type="number" step="0.01" class="form-control" name="detail_amount" type="text" value="<?= (isset($edit) && $edit['amount'] !== NULL) ? $edit['amount'] : '' ?>"/>
                        </div>
                        <div class="form-group">
                            <label>Tipo</label>
                            <select name="detail_type_id" class="form-control">
                                <option value="">Seleccionar</option>
                                <?php
                                    foreach($detail_types as $type) {
                                        $selected = (isset($edit) && $edit['type_id'] && $edit['type_id'] === $type['id']) ? ' selected' : '';
                                        echo '<option value="'.$type['id'].'" '.$selected.'>'.$type['name'].'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Detalle</label>
                            <textarea class="form-control" name="detail_text" type="text" maxlength="1000"><?= (isset($edit) && $edit['detail'] ) ? $edit['detail'] : '' ?></textarea>
                        </div>
                    </div>
                </div>
                <?if(!empty($errors)){?>
                <div class="clearfix">
                    <div class="alert alert-danger">
                        <?=$errors?>
                    </div>
                </div>
                <?}?>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="pull-right">
                            <button class="btn btn-primary"><?= isset($edit) ? lang('Save') : lang('Add') ?></button>
                            <?php
                            if(isset($agent_id) && $agent_id > 0) {
                            ?>
                            <a class="btn" href="<?= site_url('manager/account/' . $agent_id) ?>"><?= lang('Cancel') ?></a>
                            <?php
                            } else {
                            ?>
                            <a class="btn" href="<?= isset($edit) ? site_url('manager/account/' . $edit['agent_id']) : site_url('manager/accounts') ?>"><?= lang('Cancel') ?></a>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>