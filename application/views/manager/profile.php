<section class="content-header">
    <h1>
        Perfil
        <small>Actualizar</small>
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href=""><i class="fa fa-dashboard"></i> Perfil</a></li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Perfil</h3>
            <div class="box-tools pull-right">
            </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            <? if (!empty($status)) { ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <p><b><i class="icon fa fa-check"></i> Guardado correctamente</b></p>
                </div>
            <? } ?>
            <form class="form" method="post" action="">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Nombre</label>
                            <input class="form-control" name="user_firstname" type="text" value="<?= $this->session->userdata('user_firstname') ?>" maxlength="200" />
                        </div>        
                    </div>        
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Apellido</label>
                            <input class="form-control" name="user_lastname" type="text" value="<?= $this->session->userdata('user_lastname') ?>" maxlength="200"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Nombre de usuario</label>
                            <input class="form-control" name="user_name" type="text" value="<?= $this->session->userdata('user_name') ?>" maxlength="200" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>email</label>
                            <input class="form-control" name="user_email" type="text" value="<?= $this->session->userdata('user_email') ?>" maxlength="255" />
                        </div>
                    </div>
                </div>
                <div class="row password-link">
                    <div class="col-sm-3">
                        <p>
                            <a href="javascript:;" onclick="showPasswordFields();"><?= lang('Change password') ?></a>
                        </p>
                    </div>
                </div>
                <div class="row password" style="display: none;">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label><?= lang('Password') ?>
                                <i class="fa fa-fw fa-info-circle" data-toggle="tooltip" title="<?= lang('manager.profile.password.info') ?>"></i>
                            </label>
                            <input class="form-control" name="user_password" type="password">
                        </div>        
                    </div>        
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label><?= lang('Repeat Password') ?></label>
                            <input class="form-control" name="user_password_repeat" type="password"/>
                        </div>
                    </div>
                </div>
                 <? if (!empty($errors)) { ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Error</h4>
                        <?= $errors ?>
                    </div>
                <? } ?>
                <div class="clearfix">
                    <div class="pull-right">
                        <button class="btn btn-primary">Guardar</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</section>


