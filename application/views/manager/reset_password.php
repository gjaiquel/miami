<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>AdminMTE | Reestablecer contraseña</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="stylesheet" href="<?php echo base_url("assets/manager/css/plugins.css?" . filemtime("assets/manager/css/plugins.css")); ?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/manager/css/styles.css?" . filemtime("assets/manager/css/styles.css")); ?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/manager/js/libs/plugins/iCheck/all.css?" . filemtime("assets/manager/js/libs/plugins/iCheck/all.css")); ?>">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="register-page">
    <div class="register-box">
      <div class="register-logo">
        <a href="<?php echo base_url(); ?>"><b>Admin</b>MTE</a>
      </div>

      <div class="register-box-body">
        <p class="login-box-msg">Reestablecer contraseña</p>
        <?php if(!empty($reset_hash)) { ?>
        <form action="javascript:;" method="post" onsubmit="app.resetPassword()">
          <div class="form-group has-feedback">
            <input name="password" type="password" class="form-control" placeholder="Contraseña"/>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input name="password_repeat" type="password" class="form-control" placeholder="Repertir contraseña"/>
            <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
          </div>
          <input type="hidden" name="user_id" value="<?= $user_id ?>" />
          <input type="hidden" name="reset_hash" value="<?= $reset_hash ?>" />
          <div class="row">
            <div class="col-xs-8">    
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Aceptar</button>
            </div><!-- /.col -->
          </div>
        </form>
        <?php } else { ?>
        <p>Debe iniciar el proceso de reestablecer la contraseña nuevamente.</p>
        <?php } ?>
        <a href="<?php echo base_url(); ?>" class="text-center">Ir al login</a>
        <br/>
      </div><!-- /.form-box -->
    </div><!-- /.register-box -->

    <script>
        var app = {
            "baseUrl": "<?php echo base_url(); ?>",
            "language": "<?php echo $this->config->item('language'); ?>"
        };
    </script>
    <script src="<?php echo base_url("assets/manager/js/plugins.js?" . filemtime("assets/manager/js/plugins.js")); ?>"></script>
    <script src="<?php echo base_url("assets/manager/js/script.js?" . filemtime("assets/manager/js/script.js")); ?>"></script>
    <script src="<?php echo base_url("assets/manager/js/reset_password.js?" . filemtime("assets/manager/js/reset_password.js")); ?>"></script>
  </body>
</html>