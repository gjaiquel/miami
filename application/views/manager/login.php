<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>AdminMTE | Log in</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="stylesheet" href="<?php echo base_url("assets/manager/css/plugins.css?" . filemtime("assets/manager/css/plugins.css")); ?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/manager/css/styles.css?" . filemtime("assets/manager/css/styles.css")); ?>">
    <link rel="stylesheet" href="<?php echo base_url("assets/manager/js/libs/plugins/iCheck/all.css?" . filemtime("assets/manager/js/libs/plugins/iCheck/all.css")); ?>">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="<?php echo base_url(); ?>"><b>Admin</b>MTE</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Ingresa el usuario y contraseña</p>
        <form id="login-form" action="javascript:;" onsubmit="app.login();">
          <div class="form-group has-feedback">
            <input name="user" type="text" class="form-control" placeholder="Usuario" required="" autofocus=""/>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input name="password" type="password" class="form-control" placeholder="Contraseña" required=""/>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">    
              <div class="checkbox icheck">
                <label>
                  <input type="checkbox"> Remember Me
                </label>
              </div>                        
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
            </div><!-- /.col -->
          </div>
        </form>

        <a href="#" onclick="app.openForgotModal();">Olvidaste la clave?</a><br>

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
    <div id="forgot-password" class="modal">
        <form method="POST" action="javascript:;" onsubmit="app.forgotPassword();">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" onclick="app.closeForgotModal();">×</span></button>
                  <h4 class="modal-title">Reestablecer contraseña</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="email">Ingresa el email para reestablecer la contraseña</label>
                        <input type="email" name="email" class="form-control" id="email" placeholder="Ingresar email" required="">
                    </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="app.closeForgotModal();">Cancelar</button>
                  <button type="submit" class="btn btn-primary">Enviar</button>
                </div>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </form>
    </div>

    <script>
        var app = {
            "baseUrl": "<?php echo base_url(); ?>",
            "language": "<?php echo $this->config->item('language'); ?>"
        };
    </script>
    <script src="<?php echo base_url("assets/manager/js/plugins.js?" . filemtime("assets/manager/js/plugins.js")); ?>"></script>
    <script src="<?php echo base_url("assets/manager/js/script.js?" . filemtime("assets/manager/js/script.js")); ?>"></script>
    <script src="<?php echo base_url("assets/manager/js/login.js?" . filemtime("assets/manager/js/login.js")); ?>"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>