<section class="content-header">
    <h1>
        Cuentas
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href=""><i class="fa fa-dashboard"></i> Administración</a></li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><strong><?= $agent['agent_fullname'] ?></strong> - <?= $agent['agent_type'] ?>
            <?php if(!empty($agent['agent_note']) && strlen($agent['agent_note']) > 0) { ?>
                <a tabindex="0" class="btn btn-sm btn-note-popover" role="button" data-placement="top" data-toggle="popover" data-trigger="focus" title="Nota" data-content="<?= $agent['agent_note'] ?>" style="font-size:15px;"></a>
            <?php } ?>
            </h3>
            <?php if($this->session->userdata('user_type') === 'admin') { ?>
            <div class="box-tools pull-right">
                <a href="../account_detail/new/<?= $agent['agent_id'] ?>" class="btn btn-block btn-primary">Cargar movimiento</a>
            </div><!-- /.box-tools -->
            <?php } ?>
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <form class="form" action="" method="post">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input name="date_from" type="text" class="form-control datepicker" value="<?= $from ?>" data-date-format="dd/mm/yyyy">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input name="date_to" type="text" class="form-control datepicker" value="<?= $to ?>" data-date-format="dd/mm/yyyy">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <button type="submit" class="btn btn-primary">Buscar</button>
                    </div>
                </form>
            </div>
            <table class="table no-border">
                <tbody>
                    <tr>
                        <td style="text-align: right"><strong>SALDO ANTERIOR</strong></td>
                        <td style="text-align: right; width: 10px;"><strong style="color: <?= $total_before >= 0 ? 'black':'red' ?>"><?= number_format($total_before, 2, ',', '.') ?></strong></td>
                    </tr>
                </tbody>
            </table>
            <br/>
            <?php
            if (!empty($details)) { 
                $tableReservations = false;
                $tableExtras = false;
                $extras_subtotal = 0;
                $res_subtotal = 0;
                foreach ($details as $detail) {
                    // type_id: 3 = Seña
                    if($detail['reservation_number'] === NULL || $detail['type_id'] == 3) {
                        $tableExtras = true;
                    } else {
                        $tableReservations = true;
                    }
                }
            ?>
            <?php if($tableReservations) { ?>
            <table class="table table-bordered">
                <tbody>
                <tr>
                  <th style="width: 10px">Dpto</th>
                  <th style="width: 10px">Checkin</th>
                  <th style="width: 10px">Checkout</th>
                  <th style="width: 10px">Noches</th>
                  <th>Detalle</th>
                  <th style="width: 10px">Reserva</th>
                  <th style="width: 100px">Monto</th>
                  <?php if($this->session->userdata('user_type') === 'admin') { ?>
                  <th style="width: 10px">Acciones</th>
                  <?php } ?>
                </tr>
                <?php
                foreach ($details as $detail) {
                    if($detail['reservation_number'] !== NULL && $detail['type_id'] != 3) {
                        $res_subtotal += $detail['amount'];
                ?>
                <tr>
                  <td><?= $detail['apartment_short_name'] ?></td>
                  <td><?= $detail['arrival_date'] ?></td>
                  <td><?= $detail['departure_date'] ?></td>
                  <td><?= $detail['number_of_nights'] ?></td>
                  <td><?= $detail['detail'] ?></td>
                  <td><span style="cursor:pointer;" onclick="app.showReservationDetails(<?=$detail['reservation_number']?>);"><?= $detail['reservation_number']; ?></span></td>
                  <td style="text-align:right"><strong style="color: <?= $detail['amount'] >= 0 ? 'black':'red' ?>"><?= number_format($detail['amount'], 2, ',', '.') ?></strong></td>
                  <?php if($this->session->userdata('user_type') === 'admin') { ?>
                  <td>
                      <a href="<?= site_url('manager/account_detail/' . $detail['id']) ?>" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></a>
                      &nbsp;
                      <a href="javascript:;" style="color:red;" onclick="app.deleteAccountDetailConfirm(<?= $agent['agent_id']?>,<?= $detail['id']?>)" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-trash"></i></a>
                  </td>
                  <?php } ?>
                </tr>
                <?php
                    }
                }
                ?>
                <tr>
                    <td colspan="<?= $this->session->userdata('user_type') === 'admin' ? '6' : '5' ?>" style="text-align: right">TOTAL DE RENTAS LIBRE DE GASTOS</td>
                    <td style="text-align: right"><strong style="color: <?= $res_subtotal >= 0 ? 'black':'red' ?>"><?= number_format($res_subtotal, 2, ',', '.') ?></strong></td>
                    <td></td>
                </tr>
              </tbody>
            </table>
            <br/>
            <?php } ?>
            <?php if($tableExtras) { ?>
            <table class="table table-bordered">
                <tbody>
                <tr>
                  <th style="width: 10px">Fecha</th>
                  <th>Tipo</th>
                  <th>Detalle</th>
                  <th style="width: 10px">Reserva</th>
                  <th style="width: 100px">Monto</th>
                  <?php if($this->session->userdata('user_type') === 'admin') { ?>
                  <th style="width: 10px">Acciones</th>
                  <?php } ?>
                </tr>
                <?php
                foreach ($details as $detail) { 
                    if($detail['reservation_number'] === NULL || $detail['type_id'] == 3) {
                        $extras_subtotal += $detail['amount'];
                ?>
                <tr>
                  <td><?= $detail['date'] ?></td>
                  <td><?= $detail['type_name'] ?></td>
                  <td>
                        <?= $detail['detail'] ?>
                  </td>
                  <td><?php 
                        if($detail['reservation_number'] != NULL) {
                        ?>
                        <span style="cursor:pointer;" onclick="app.showReservationDetails(<?=$detail['reservation_number']?>);"><?= $detail['reservation_number']; ?></span>
                        <?php
                        }
                        ?></td>
                  <td style="text-align:right"><strong style="color: <?= $detail['amount'] >= 0 ? 'black':'red' ?>"><?= number_format($detail['amount'], 2, ',', '.') ?></strong></td>
                  <?php if($this->session->userdata('user_type') === 'admin') { ?>
                  <td>
                      <a href="<?= site_url('manager/account_detail/' . $detail['id']) ?>" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></a>
                      &nbsp;
                      <a href="javascript:;" style="color:red;" onclick="app.deleteAccountDetailConfirm(<?= $agent['agent_id']?>,<?= $detail['id']?>)" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-trash"></i></a>
                  </td>
                  <?php } ?>
                </tr>
                <?php
                    }
                } ?>
                <tr>
                    <td colspan="<?= $this->session->userdata('user_type') === 'admin' ? '4' : '3' ?>" style="text-align: right">TOTAL GASTOS</td>
                    <td style="text-align: right"><strong style="color: <?= $extras_subtotal >= 0 ? 'black':'red' ?>"><?= number_format($extras_subtotal, 2, ',', '.') ?></strong></td>
                    <td></td>
                </tr>
              </tbody>
            </table>
            <br/>
            <?php } ?>
            <table class="table no-border">
                <tbody>
                    <tr>
                        <td style="text-align: right"><strong>TOTAL A DEPOSITAR</strong></td>
                        <td style="text-align: right; width: 10px;"><strong style="color: <?= $total_before + $extras_subtotal + $res_subtotal >= 0 ? 'black':'red' ?>"><?= number_format($total_before + $extras_subtotal + $res_subtotal, 2, ',', '.') ?></strong></td>
                    </tr>
                </tbody>
            </table>
            <?php
            } else {
                echo '<p>No hay detalle disponible para el rango de fecha elegido</p>';
            }
            ?>
        </div><!-- /.box-body -->
    </div><!-- /.box -->

    
    <div class="row">
        <div class="col-sm-6">
            <a type="submit" class="btn btn-default" href="<?= site_url('manager/accounts') ?>">Volver</a>
        </div>
        <div class="col-sm-6" style="text-align:right;">
            <?php if($this->session->userdata('user_type') === 'admin') { ?>
            <a id="email-btn" class="btn btn-primary" href="javascript:;" data-agent="<?= $agent['agent_id'] ?>" data-from="<?= $from ?>" data-to="<?= $to ?>">Enviar e-mail</a>
            <?php } ?>
        </div>
    </div>
</section>

<div id="delete-account-detail" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" onclick="app.closeAccountDetailConfirm()">×</span></button>
          <h4 class="modal-title">Eliminar detalle de la cuenta?</h4>
        </div>
        <div class="modal-body">
          <p></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="app.closeAccountDetailConfirm()">Cancelar</button>
          <a type="button" class="btn btn-primary" href="">Eliminar</a>
        </div>
      </div>
    </div>
</div>
<div id="popup">
    <!--
    <div class="alert alert-success alert-dismissable" id="success-popup" style="padding: 10px;position: fixed;top: 70px;right: 0px;width: 250px;z-index: 99999;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="position: initial;">×</button>
        <h4>	<i class="icon fa fa-check"></i> Enviado!</h4>
        El e-mail ha sido enviado exitosamente.
    </div>
    -->
</div>
<div id="reservation-details" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" onclick="app.closeReservationDetails()">×</span></button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
        </div>
        <div class="overlay" style="display: none;position: absolute;top: 0;left: 0;width: 100%;height: 100%;z-index: 1010;background: rgba(255,255,255,0.7);border-radius: 3px;">
            <i class="fa fa-refresh fa-spin" style="position: absolute;top: 50%;left: 50%;margin-left: -15px;margin-top: -15px;color: #000;font-size: 30px;"></i>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="app.closeReservationDetails()">Cerrar</button>
        </div>
      </div>
    </div>
</div>