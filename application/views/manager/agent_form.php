<section class="content-header">
    <h1>
        Agentes
        <small><?= isset($edit) ? lang('Edit') : lang('Add') ?></small>
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href=""><i class="fa fa-dashboard"></i> Configuración</a></li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Agentes</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <form class="form" action="" method="post">
                <input name="agent_id" type="hidden" value="<?= (isset($edit) && !empty($edit['agent_id']) ) ? $edit['agent_id'] : '' ?>"/>

                <div class="row">
                    <div class="col-sm-3">

                        <div class="form-group">
                            <label>Nombre</label>
                            <input class="form-control" name="agent_firstname" type="text" value="<?= (isset($edit) && $edit['agent_firstname'] ) ? $edit['agent_firstname'] : '' ?>"/>
                        </div>
                        <div class="form-group">
                            <label>Apellido</label>
                            <input class="form-control" name="agent_lastname" type="text" value="<?= (isset($edit) && $edit['agent_lastname'] ) ? $edit['agent_lastname'] : '' ?>"/>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input class="form-control" name="agent_email" type="text" value="<?= (isset($edit) && $edit['agent_email']) ? $edit['agent_email'] : '' ?>"/>
                        </div>
                        <div class="form-group">
                            <label>Tipo de agente</label>
                            <select name="agent_type_id" class="form-control">
                                <option value="">Seleccionar</option>
                                <?php
                                    foreach($agent_types as $type) {
                                        $selected = (isset($edit) && $edit['agent_type_id'] && $edit['agent_type_id'] === $type['agent_type_id']) ? ' selected' : '';
                                        echo '<option value="'.$type['agent_type_id'].'" '.$selected.'>'.$type['agent_type_name'].'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Palabra clave</label>
                            <input class="form-control" name="agent_keyword" type="text" value="<?= (isset($edit) && $edit['agent_keyword'] ) ? $edit['agent_keyword'] : '' ?>"/>
                        </div>
                        <div class="form-group">
                            <label>Valor predefinido</label>
                            <input class="form-control" name="agent_predefined_value" type="text" value="<?= (isset($edit) && $edit['agent_predefined_value'] ) ? $edit['agent_predefined_value'] : '' ?>"/>
                        </div>
                        <div class="form-group">
                            <label>Nota</label>
                            <textarea class="form-control" name="agent_note" type="text" maxlength="100"><?= (isset($edit) && $edit['agent_note'] ) ? $edit['agent_note'] : '' ?></textarea>
                        </div>
                    </div>
                </div>
                <?php if(!empty($errors)){ ?>
                <div class="clearfix">
                    <div class="alert alert-danger">
                        <?= $errors ?>
                    </div>
                </div>
                <?php } ?>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="pull-right">
                            <button class="btn btn-primary"><?= isset($edit) ? 'Guardar' : 'Agregar' ?></button>
                            <a class="btn" href="<?= site_url('manager/agents') ?>">Cancelar</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>