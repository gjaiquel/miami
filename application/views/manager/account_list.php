<section class="content-header">
    <h1>
        Cuentas
    </h1>
    <ol class="breadcrumb">
        <li class="active"><a href=""><i class="fa fa-dashboard"></i> Administración</a></li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Lista</h3>
            <?php if($this->session->userdata('user_type') === 'admin') { ?>
            <div class="box-tools pull-right">
                <a href="./account_detail/new" class="btn btn-block btn-primary">Cargar movimiento</a>
            </div><!-- /.box-tools -->
            <?php } ?>
        </div><!-- /.box-header -->
        <div class="box-body">
            <?php
            if (!empty($list)) {
            ?>
            <table class="table table-bordered table-hover datatable_noconfig account-list">
                <thead style="background-color: #dcdcdc">
                    <tr class="top">
                        <th style="width:14px;"></th>
                        <th>Agente</th>
                        <th>Tipo</th>
                        <th style="text-align:right">Saldo &nbsp;&nbsp;</th>
                    </tr>
                </thead>
                
                <tbody>
                    <?php
                    foreach ($list as $account) {
                    ?>
                        <tr class="reg_<?= $account['agent_id'] ?>">
                            <td>
                                <?php if(!empty($account['agent_note']) && strlen($account['agent_note']) > 0) { ?>
                                <a tabindex="0" class="btn btn-sm btn-note-popover" style="padding:0px 0px 1px 3px;border:0px;font-size:14px;" role="button" data-placement="top" data-toggle="popover" data-trigger="focus" title="Nota" data-content="<?= $account['agent_note'] ?>"></a>
                                <?php } ?>
                            </td>
                            <td onclick="window.location.href = 'account/<?= $account['agent_id'] ?>'"><?= $account['agent_fullname'] ?></td>
                            <td onclick="window.location.href = 'account/<?= $account['agent_id'] ?>'"><?= $account['agent_type_name'] ?></td>
                            <td onclick="window.location.href = 'account/<?= $account['agent_id'] ?>'" style="text-align:right;color:<?= $account['total'] >= 0 ? 'black':'red'; ?>"><strong><?= number_format($account['total'], 2, ',', '') ?></strong></td>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
            <?php
            } else {
                echo '<p>No hay cuentas disponibles</p>';
            }
            ?>
        </div><!-- /.box-body -->
    </div><!-- /.box -->


</section>
<!--
<div id="delete-apartment" class="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" onclick="app.closeDeleteApartmentConfirm()">×</span></button>
          <h4 class="modal-title">Eliminar departamento?</h4>
        </div>
        <div class="modal-body">
          <p></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="app.closeDeleteApartmentConfirm()">Cancelar</button>
          <a type="button" class="btn btn-primary" href="">Eliminar</a>
        </div>
      </div>
    </div>
</div>
-->