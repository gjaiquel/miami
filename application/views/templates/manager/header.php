<header class="main-header">
    <!-- Logo -->
    <a href="<?= site_url().'manager'?>" class="logo">AdminMTE</a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <?php if($incomplete_reservations > 0 && $this->session->userdata('user_type') === 'admin') { ?>
                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-danger">1</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">Hay reservas sin procesar</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;">
                                <ul class="menu" style="overflow: hidden; width: 100%; height: 100%;">
                                    <li>
                                        <a href="<?= base_url('/manager/reprocess') ?>">
                                            <i class="fa fa-arrow-right text-aqua"></i> Haz click aqui para procesarlas
                                        </a>
                                    </li>
                                </ul>
                                <div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 3px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;">
                                </div>
                                <div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;">
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                <?php } ?>
              <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <span class="hidden-xs"><?=$this->session->userdata('user_firstname').' '.$this->session->userdata('user_lastname')?></span>
                </a>
                <ul class="dropdown-menu" style="height: 80px;overflow: hidden;">
                  <li>
                    <ul class="menu">
                      <li>
                        <a href="<?=site_url('manager/profile')?>" ><i class="fa fa-user text-green"></i><?= lang('Profile') ?></a>
                      </li>
                      <li>
                        <a href="<?= site_url('user/logout') ?>" ><i class="fa fa-sign-out text-red"></i><?= lang('Logout') ?></a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </li>
            </ul>
        </div>
    </nav>
</header>