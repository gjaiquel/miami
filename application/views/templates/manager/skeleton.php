<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php echo $title ?></title>
        <meta name="description" content="<?php echo $description ?>" />
        <meta name="viewport" content="width=device-width">
        <meta name="keywords" content="<?php echo $keywords ?>" />
        <meta name="author" content="<?php echo $author ?>" />

        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<?php echo base_url(MAN_CSS . "plugins.css?" . filemtime(MAN_CSS . "plugins.css")); ?>">
        <link rel="stylesheet" href="<?php echo base_url(MAN_CSS . "styles.css?" . filemtime(MAN_CSS . "styles.css")); ?>">

        <!-- extra CSS-->
        <?
        foreach ($css as $c) {
            if (!empty($c['url'])) {
                ?>
                <link rel="stylesheet" href="<?php echo base_url() . MAN_CSS . $c['url'] ?>">
                <?
            } else {
                echo $c;
            }
        }
        ?>

        <!-- Le fav and touch icons -->
        <link rel="shortcut icon" href="<?php echo base_url('assets/favicon.ico?' . filemtime('assets/favicon.ico')); ?>">
    </head>
    <body class="skin-green">

        <div class="wrapper">
            <?php echo $header ?>
            <?php echo $sidebar ?>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <?php echo $content_body ?>
            </div><!-- /.content-wrapper -->
        </div><!-- ./wrapper -->
        <?php echo $footer ?>
        <script>
            var app = {
                "baseUrl": "<?php echo base_url(); ?>",
                "language": "<?php echo $this->config->item('language'); ?>"
            };
        </script>


        <script src="<?php echo base_url(MAN_JS . "plugins.js?" . filemtime(MAN_JS . "plugins.js")); ?>"></script>
        <script src="<?php echo base_url(MAN_JS . "script.js?" . filemtime(MAN_JS . "script.js")); ?>"></script>

        <? if ($this->session->userdata('user_logged')) { ?>
            <script>
                window.setTimeout(app.keepAlive, app.keepAliveWait);
            </script>
        <? } ?>
        <!-- extra js-->
        <?php
        foreach ($javascript as $js) {
            if (!empty($js['url'])) {
        ?>
        <script defer src="<?php echo base_url() . MAN_JS . $js['url'] . '?' . filemtime(MAN_JS . $js['url'])?>"></script>
        <?php
            } else {
                echo $js;
            }
        }
        ?>
    </body>
</html>
