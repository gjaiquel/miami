<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header" style="text-transform: uppercase">Administración</li>
            <li class="<?= isset($menu_active) && $menu_active === 'index' ? 'active' : '' ?>">
                <a href="<?= site_url('manager') ?>">
                    <i class="fa fa-dashboard"></i> 
                    <span>Inicio</span>
                </a>
            </li>
            <?php if($this->session->userdata('user_type') === 'admin') { ?>
            <li class="<?= isset($menu_active) && $menu_active === 'upload' ? 'active' : '' ?>">
                <a href="<?= site_url('manager/upload') ?>">
                    <i class="fa fa-upload"></i> 
                    <span>Subir planilla</span>
                </a>
            </li>
            <?php } ?>
            <li class="<?= isset($menu_active) && $menu_active === 'accounts' ? 'active' : '' ?>">
                <a href="<?= site_url('manager/accounts') ?>">
                    <i class="fa fa-usd"></i> 
                    <span>Cuentas</span>
                </a>
            </li>
        </ul>
        <ul class="sidebar-menu">
            <li class="header" style="text-transform: uppercase">Configuración</li>
            <?php if($this->session->userdata('user_type') === 'admin') { ?>
            <li class="<?= isset($menu_active) && $menu_active === 'users' ? 'active' : '' ?>">
                <a href="<?= site_url('manager/users') ?>">
                    <i class="fa fa-users"></i>
                    <span>Usuarios</span>
                </a>
            </li>
            <?php } ?>
            <li class="<?= isset($menu_active) && $menu_active === 'agents' ? 'active' : '' ?>">
                <a href="<?= site_url('manager/agents') ?>">
                    <i class="fa fa-male"></i>
                    <span>Agentes</span>
                </a>
            </li>
            <li class="<?= isset($menu_active) && $menu_active === 'apartments' ? 'active' : '' ?>">
                <a href="<?= site_url('manager/apartments') ?>">
                    <i class="fa fa-home"></i> 
                    <span>Departamentos</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>