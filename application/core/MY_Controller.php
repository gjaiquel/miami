<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    //Page info
    protected $data = Array();
    protected $pageName = FALSE;
    protected $template = "default";
    protected $hasNav = TRUE;
    //Page contents
    //use this array to include special js plugins, only for "some" pages - 
    //when should be incluede more general plugins, for more pages use the plugins.js file on assets
    protected $javascript = array();
    //use this array to include special css plugins, only for "some" pages - 
    //when should be incluede more general plugins, for more pages use the plugins.css file on assets
    protected $css = array();
    protected $fonts = array();
    //Page Meta
    protected $title = FALSE;
    protected $description = FALSE;
    protected $keywords = FALSE;
    protected $author = FALSE;

    function __construct() {

        parent::__construct();
        $this->data["uri_segment_1"] = $this->uri->segment(1);
        $this->data["uri_segment_2"] = $this->uri->segment(2);
        $this->title = $this->config->item('site_title');
        $this->description = $this->config->item('site_description');
        $this->keywords = $this->config->item('site_keywords');
        $this->author = $this->config->item('site_author');

        $this->pageName = strtolower(get_class($this));

        //language loading
        $site_lang = $this->session->userdata('site_lang');
        if (empty($site_lang)) {
            $site_lang = 'spanish';
        }
        $this->load->helper('language');
        $this->lang->load('common', $site_lang);
        $this->load->model('User_model');
        $this->User_model->check_cookie();
		$this->User_model->load_session();
        date_default_timezone_set('America/Argentina/Buenos_Aires');
    }

    protected function _render($view, $renderData = "FULLPAGE") {
        switch ($renderData) {
            case "AJAX" :
                $this->load->view($view, $this->data);
                break;
            case "JSON" :
                echo json_encode($this->data);
                break;
            case "FULLPAGE" :
            default :
                //static
                $toTpl["javascript"] = $this->javascript;
                $toTpl["css"] = $this->css;
                $toTpl["fonts"] = $this->fonts;

                //meta
                $toTpl["title"] = $this->title;
                $toTpl["description"] = $this->description;
                $toTpl["keywords"] = $this->keywords;
                $toTpl["author"] = $this->author;

                $toTpl = array_merge($this->data, $toTpl);
                //data
                $toBody["content_body"] = $this->load->view($view, $toTpl, true);

                $toBody["header"] = $this->load->view('templates/' . $this->template . "/header", $toTpl, true);

                if ($this->template == 'manager') {
                    $toBody["sidebar"] = $this->load->view('templates/' . $this->template . "/sidebar", $toTpl, true);
                }
                
                $toBody["footer"] = $this->load->view('templates/' . $this->template . "/footer", $toTpl, true);

                //render view
                $this->load->view('templates/' . $this->template . "/skeleton", array_merge($toBody, $toTpl));
                break;
        }
    }

}
