<?php
if (!function_exists('resize_image')) {

    function resize_image($path, $file, $new_folder=false, $w = 200, $h = 114) {
        $CI = & get_instance();
        $data['json'] = json_decode($file);
        $file_info = array();
        $file_info['file_title'] = $data['json']->{'file_name'};
        $file_info['file_ext'] = $data['json']->{'file_ext'};
        $file_info['file_size'] = $data['json']->{'file_size'};

        /* THUMBS */
        $folder = realpath($path);
        $config['image_library'] = 'gd2';
        $config['source_image'] = $folder . '/' . $file_info['file_title'];
        $config['maintain_ratio'] = TRUE;
        if ($new_folder)
            $config['new_image'] = $folder . $new_folder . '/' . $file_info['file_title'];
        $config['width'] = $w;
        $config['height'] = $h;
        $CI->load->library('image_lib', $config);
        if (!$CI->image_lib->resize()) {
            $response['error_msg'] = $CI->image_lib->display_errors();
            $response['error'] = '1';
            return $response;
        }

        if ($new_folder) {
            list($w, $h) = getimagesize($folder . '/' . $file_info['file_title']);
            if ($w > 1360 || $h > 768) {
                $CI->image_lib->clear();
                $double_cfg['image_library'] = 'gd2';
                $double_cfg['source_image'] = $folder . '/' . $file_info['file_title'];
                $double_cfg['maintain_ratio'] = TRUE;
                $double_cfg['width'] = 800;
                $double_cfg['height'] = 600;
                $CI->image_lib->initialize($double_cfg);
                if (!$CI->image_lib->resize()) {
                    $response['error_msg'] = $CI->image_lib->display_errors();
                    $response['error'] = '1';
                    return $response;
                }
            }
        }
    }

}
?>