<?php
/*
    Document   : common_lang
    Author     : Rodrigo E. Torres
    Web & Mobile Developer
    rtorres@sobrelaweb.com | torresrodrigoe@gmail.com
*/

$lang['user'] = 'user';
$lang['admin_user'] = 'Admin user';
$lang['login'] = 'Login';
$lang['name'] = 'Name';
$lang['firstname'] = 'First name';
$lang['lastname'] = 'Last name';
$lang['password'] = 'Password';
$lang['repeat-password'] = 'Repeat password';
$lang['email'] = 'Email';
$lang['email2'] = 'Repeat email';
$lang['phone'] = 'Phone';
$lang['message'] = 'Message';
$lang['send'] = 'Send';
$lang['register_success'] = 'Succesfully registered';
$lang['register_error'] = 'Register error';
$lang['register_error_msg'] = 'You were already registered!';
$lang['content_to_admin'] = 'Select an entry to administrate';
$lang['error_uri_exists'] = 'Url already exists, it can not be duplicated';

$lang['write_your_pass'] = 'Please write your user/password';
$lang['pass_error'] = 'Password error';
$lang['user_error'] = 'User not exists';
$lang['current_image'] = 'Current image';
$lang['video_deleted'] = 'Video eliminado';
$lang['image_deleted'] = 'Image deleted';
$lang['current_video'] = 'Video actual';
$lang['user_deleted'] = 'User deleted';
$lang['no_image'] = 'No image';
$lang['no_video'] = 'No video';

$lang['succes_saved'] = 'Saved Correctly';


$lang['Nuevo Mensaje de contacto'] = 'New contact message';
$lang['Enviado'] = 'Sent';
$lang['Remitente'] = 'Sent from';
$lang['Mensaje'] = 'Message';

$lang['home.index.popular'] = 'Cities';
$lang['home.index.services'] = 'Services';
$lang['home.index.contact'] = 'Contact';
$lang['home.index.about-us'] = 'About us';
$lang['home.index.login.msg.wrong.user'] = 'Wrong user';
$lang['home.index.login.msg.wrong.pass'] = 'Wrong password';
$lang['home.index.login.msg.type.pass'] = 'Please type your password';
$lang['home.index.login.msg.forgot.password'] = 'Forgot your password?';
$lang['home.index.forgot.info.msg'] = 'You will receive an e-mail with a link to reset your password';
$lang['home.index.signup.msg.user.exists'] = 'Sorry, the email is already in use';
$lang['home.index.signup.term.and.conditions'] = 'I agree <a href="http://renquila.com/blog/terminos-y-condiciones-de-uso/" target="_blank">terms and conditions</a>';
$lang['home.index.signup.hr.separator.text'] = 'or';
$lang['home.index.logout'] = 'Logout';
$lang['home.index.manager'] = 'Manager';
$lang['home.index.welcome.title'] = 'Welcome to renquila';
$lang['home.index.welcome.subtitle'] = 'Rent special rooms and houses in all the world';
$lang['home.index.search.place'] = 'City';
$lang['home.index.search.place.popover.title'] = 'Select a city';
$lang['home.index.search.place.popover.msg'] = 'Start typing and select an option from list';
$lang['home.index.search.type'] = 'Type';
$lang['home.index.search.type.popover.title'] = 'Select a type';
$lang['home.index.search.type.popover.msg'] = 'Select an option from list';
$lang['home.index.search.checkin'] = 'In';
$lang['home.index.search.checkout'] = 'Out';
$lang['home.index.search.submit'] = 'Search';
$lang['home.index.most.searched.cities'] = 'Most popular cities';
$lang['home.index.city.message'] = 'Houses, offices, landes, buildings and more!';
$lang['home.index.city.view'] = 'See houses »';
$lang['home.index.more.cities'] = 'More cities';
$lang['home.index.service.title'] = 'our services';
$lang['home.index.service.single'] = 'Particular';
$lang['home.index.service.single.description'] = 'Rent or sell';
$lang['home.index.service.hotel'] = 'Hotels';
$lang['home.index.service.hotel.description'] = 'Publish your hotel';
$lang['home.index.service.realstate.account'] = 'Real state';
$lang['home.index.service.realstate.account.description'] = 'Special accounts';
$lang['home.index.service.cottage.account'] = 'Cottage';
$lang['home.index.service.cottage.account.description'] = 'Publish your complex';
$lang['home.index.contact.title'] = 'Contact us';
$lang['home.index.contact.form.name'] = 'Name';
$lang['home.index.contact.form.email'] = 'e-mail';
$lang['home.index.contact.form.services'] = 'Services';
$lang['home.index.contact.form.select.nooption'] = 'Choose one:';
$lang['home.index.contact.form.select.annual'] = 'Annual Rent';
$lang['home.index.contact.form.select.temp'] = 'Temporal Rent';
$lang['home.index.contact.form.select.account'] = 'Account';
$lang['home.index.contact.form.message.title'] = 'Message';
$lang['home.index.contact.form.message.default.text'] = 'Write a message';
$lang['home.index.contact.form.submit'] = 'Send';
$lang['home.index.contact.office.information.title'] = 'Office';
$lang['home.index.contact.find.us.on.social'] = 'Find us on Social Networks';
$lang['home.index.footer.rights'] = 'All rights reserved';
$lang['email.signup.header.message1'] = 'Renquila.com account confirmation';
$lang['email.signup.header.message2'] = 'No Images?';
$lang['email.signup.header.message3'] = 'Click here';
$lang['email.signup.greeting'] = 'Hello';
$lang['email.signup.body.message1'] = 'We need to confirm your email to activate your account, please click button below.';
$lang['email.signup.body.message2'] = 'If you did not want to create an account, just dismiss this message';
$lang['email.signup.button'] = 'Activate';
$lang['email.signup.footer.message1'] = 'Your email address was used to create an account on Renquila.com';
$lang['email.reset.password.header.message1'] = 'Renquila.com password reset';
$lang['email.reset.password.header.message2'] = 'No Images?';
$lang['email.reset.password.header.message3'] = 'Click here';
$lang['email.reset.password.greeting'] = 'Hello';
$lang['email.reset.password.body.message1'] = 'We have received a reset password request for your account on Renquila.com, to create your new password please click on the button below';
$lang['email.reset.password.body.message2'] = 'If you do not want to reset your Renquila.com account password, please dismiss this message';
$lang['email.reset.password.button'] = 'Reset Password';
$lang['email.reset.password.footer.message1'] = 'Reset password on Renquila.com';
$lang['password.changed.successfully'] = 'Password has been changed successfully';
$lang['something.went.wrong'] = 'Something went wrong';
$lang['password.must.match'] = 'Passwords must match each other';
$lang['password.invalid.characters'] = 'Complete your password with valid characters.';
$lang['reset.password.wrong.email.msg'] = 'The email you entered is wrong';
$lang['password.reset.form.error.msg'] = 'Something went wrong, try initialize the process again';
$lang['account.activation'] = 'Account activation';
$lang['reset.password'] = 'Reset password';

// Manager
$lang['manager.add.property.description.summary'] = 'Title and description that will appear in your post.';
$lang['manager.add.property.price.summary'] = 'Determine the price per night your visitors will see in the post.';
$lang['manager.add.property.photos.summary'] = 'Add all the photos you like. Visitors love photos that describe in a clear way your accommodation';
$lang['manager.add.property.features.rooms.summary'] = 'Amount of rooms and toilets available.';
$lang['manager.add.property.features.most.services'] = 'Most common services that accommodations offer in Renquila.';
$lang['manager.add.property.address.summary'] = 'Your address is private and only guests holding a confirmed reservation for your accommodation will see it';
$lang['manager.home.email.confirmation.message'] = 'We have sent you and email to your inbox, please check it and go to the link in it to verify your e-mail';
$lang['manager.profile.password.info'] = "Password must have at least 6 characters";
$lang['manager.profile.phone.info'] = "This info will appear on every property you publish";
$lang['To Unpublish'] = "Unpublish";
$lang['To Publish'] = "Publish";

$lang['og.description.general.page'] = "Renquila gives you the bests properties on rent on the place you need";
$lang['og.description.search.page'] = "Find a lot of different properties on rent on one site! Get into and find the property you are looking for on Renquila.com";