<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Apartment_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function add($apartment) {
        $this->db->insert('apartments', $apartment);
        return $this->db->insert_id();
    }

    function save($tosave) {
        if ($this->input->post('apartment_id')) {
            $id = $this->input->post('apartment_id');
        }
        if (!empty($id)) {
            $this->db->where('apartment_id', $id);
            $this->db->update('apartments', $tosave);
            return $id;
        } else {
            $this->db->insert('apartments', $tosave);
            return $this->db->insert_id();
        }
    }

    function new_validation() {
        $config = array(
            array(
                'field' => 'reservation_name',
                'label' => 'numero en reservation',
                'rules' => 'required|callback_apartment_reservation_name_exists'
            ),
            array(
                'field' => 'short_name',
                'label' => 'numero departamento',
                'rules' => 'trim|required|callback_apartment_short_name_exists'
            ),
            array(
                'field' => 'agent_id',
                'label' => 'propietario',
                'rules' => 'required'
            ),
            array(
                'field' => 'agent_checkin',
                'label' => 'agente checkin',
                'rules' => 'required'
            )
        );
        return $config;
    }

    

    function read_create_apartment_post() {
        return array(
            'apartment_reservation_name'            => $this->input->post('reservation_name', true),
            'apartment_short_name'                  => $this->input->post('short_name', true),
            'apartment_agent_id'                    => $this->input->post('agent_id', true),
            'apartment_agent_checkin'               => $this->input->post('agent_checkin', true)
        );
    }
    
    function get_by_reservation_name($name, $apartment_id = false) {
        $this->db->limit(1);
        $this->db->where(array('apartment_reservation_name' => $name));
        if (!empty($apartment_id)) {
            $this->db->where('apartment_id !=', $apartment_id);
        }
        $apartment = $this->db->get('apartments')->row_array();
        return $this->load_apartment_data($apartment);
    }
    
    function get_by_short_name($name, $apartment_id = false) {
        $this->db->limit(1);
        $this->db->where(array('apartment_short_name' => $name));
        if (!empty($apartment_id)) {
            $this->db->where('apartment_id !=', $apartment_id);
        }
        $apartment = $this->db->get('apartments')->row_array();
        return $this->load_apartment_data($apartment);
    }
    
    function get_by_shortname($name) {
        $this->db->limit(1);
        $this->db->where(array('apartment_short_name' => $name));

        $apartment = $this->db->get('apartments')->row_array();
        
        return $this->load_apartment_data($apartment);
    }
    
    function get_by_agent($agentid) {
        $this->db->where(array('apartment_agent_id' => $agentid));
        $all = $this->db->get('apartments')->result_array();
        
        if (!empty($all)) {
            foreach ($all as $k => $apartment) {
                $all[$k] = $this->load_apartment_data($apartment);
            }
        }
        
        return $all;
    }

    function get($id) {
        $this->db->limit(1);
        $apartment = $this->db->get_where('apartments', array('apartment_id' => $id))->row_array();
        return $this->load_apartment_data($apartment);
    }

    function get_all() {
        $all = $this->db->get('apartments')->result_array();
        if (!empty($all)) {
            foreach ($all as $k => $apartment) {
                $all[$k] = $this->load_apartment_data($apartment);
            }
        }
        return $all;
    }
    
    function load_apartment_data($apartment) {
        if(empty($apartment)){
            return;
        }
        $this->db->limit(1);
        $agent = $this->db->get_where('agents', array('agent_id' => $apartment['apartment_agent_id']))->row_array();
        $apartment['apartment_agent_name'] = $agent['agent_firstname'] . ' ' . $agent['agent_lastname'];
        $this->db->limit(1);
        $agent2 = $this->db->get_where('agents', array('agent_id' => $apartment['apartment_agent_checkin']))->row_array();
        $apartment['apartment_agent_name_checkin'] = $agent2['agent_firstname'] . ' ' . $agent2['agent_lastname'];
        return $apartment;
    }
    
    function change_active_status($userid, $state) {
        $this->update($userid, array('apartment_active' => $state));
    }

    function del($id) {
        $this->db->delete('apartments', array('apartment_id' => $id));
    }


    function update($apartmentid, $data) {
        $this->db->where('apartment_id', $apartmentid);
        $this->db->update('apartments', $data);
    }

}
