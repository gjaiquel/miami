<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reservation_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->table = 'reservations';
    }
    
    function add($reservation) {
        $reservation['done'] = false;
        return $this->db->insert($this->table, $reservation);
    }
    
    function update($reservation) {
        $this->db->where('reservation_number', $reservation['reservation_number']);
        $this->db->update($this->table, $reservation);
        return $reservation['reservation_number'];
    }
    
    function delete_by_reservation_numbers($reservation_numbers) {
        return $this->db->delete($this->table , 'reservation_number in (' . implode(', ', $reservation_numbers) . ')');
    }
    
    function get_by_reservation_numbers($reservation_numbers) {
        $reservations = array();
        foreach ($reservation_numbers as $row) {
            $reservations[] = $row["reservation_number"];
        }
        return $this->db
                ->where_in("reservation_number", $reservations)
                ->get($this->table)->result_array();
    }
    
    function get_by_reservation_number($reservation_number) {
        return $this->db
                ->where(array("reservation_number" => $reservation_number))
                ->get($this->table)->row_array();
    }
    
    function get_not_done_reservations() {
        return $this->db
                ->where_in("done", false)
                ->get($this->table)->result_array();
    }
    
    function get_not_done_reservations_count() {
        $query = 'SELECT COUNT(*) AS count FROM ' . $this->table . ' WHERE done=0;';
        $result = $this->db->query($query)->row_array();
        return $result['count'];
    }

}
