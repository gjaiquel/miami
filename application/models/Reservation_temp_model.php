<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reservation_temp_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->table = 'reservations_temp';
    }

    function delete_data() {
        return $this->db->empty_table($this->table);
    }

    function load_from_file($path) {
        $query = "LOAD DATA LOCAL INFILE '".$path ."'"
                . " INTO TABLE reservations_temp"
                . " FIELDS TERMINATED BY ','"
                . " ENCLOSED BY '\"'"
                . " LINES TERMINATED BY '\n'"
                . " IGNORE 1 ROWS"
                // table columns
                . " (reservation_number,status,room,fraction,guest_first_name,guest_last_name,second_guest_first_name,"
                . "second_guest_last_name,guest_email,guest_address,guest_city,guest_state,guest_zip_code,guest_country,"
                . "guest_home_phone,guest_business_phone,guest_mobile_phone,guest_company,contact_notes,check_in_time,"
                . "reservation_notes,@date_booked_var,@arrival_date_var,@departure_date_var,number_of_nights,total_room_price,"
                . "discounts,fees,expenses,taxes,items_for_sale,total,payments,balance,number_of_adults,number_of_children,"
                . "created_by_user_name,pago_con_tarjeta_py,pago_con_paypal,Miami)"
                // variables
                . " SET date_booked = STR_TO_DATE(@date_booked_var,'%m/%d/%Y'),"
                . " arrival_date = STR_TO_DATE(@arrival_date_var,'%m/%d/%Y'),"
                . " departure_date = STR_TO_DATE(@departure_date_var,'%m/%d/%Y')";
        return $this->db->query($query);
    }
    
    function get_all() {
        return $this->db->get($this->table)->result_array();
    }

}
