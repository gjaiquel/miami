<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function add($user) {
        $this->db->insert('users', $user);
        return $this->db->insert_id();
    }

    function save($tosave) {
        if ($this->input->post('user_id')) {
            $id = $this->input->post('user_id');
        }
        if (!empty($id)) {
            $this->db->where('user_id', $id);
            $this->db->update('users', $tosave);
            return $id;
        } else {
            $this->db->insert('users', $tosave);
            return $this->db->insert_id();
        }
    }

    function load_session() {
        if ($this->session->userdata('user_id')) {
            $user = $this->get($this->session->userdata('user_id'));
            $this->start_session($user);
        }
    }

    function start_session($user) {
        $session = $user;        
        $session['user_logged'] = TRUE;
        $this->session->set_userdata($session);
    }

    function login_validation() {
        $config = array(
            array(
                'field' => 'user',
                'label' => 'usuario',
                'rules' => 'required'
            ),
            array(
                'field' => 'password',
                'label' => 'contraseña',
                'rules' => 'required|callback_password_check'
            )
        );
        return $config;
    }

    function start_reset_password_validation() {
        $config = array(
            array(
                'field' => 'email',
                'label' => 'email',
                'rules' => 'trim|required|strtolower'
            )
        );
        return $config;
    }

    function reset_password_validation() {
        $config = array(
            array(
                'field' => 'password',
                'label' => 'password',
                'rules' => 'trim|required|min_length[6]'
            ),
            array(
                'field' => 'password_repeat',
                'label' => 'repeat password',
                'rules' => 'trim|required|matches[password]'
            )
        );
        return $config;
    }

    function manager_validation() {
        $config = array(
            array(
                'field' => 'user_firstname',
                'label' => 'nombre',
                'rules' => 'required'
            ),
            array(
                'field' => 'user_lastname',
                'label' => 'apellido',
                'rules' => 'required'
            ),
            array(
                'field' => 'user_password',
                'label' => 'contraseña',
                'rules' => 'trim|min_length[6]'
            ),
            array(
                'field' => 'user_password_repeat',
                'label' => 'repetir contraseña',
                'rules' => 'trim|matches[user_password]'
            ),
            array(
                'field' => 'user_email',
                'label' => 'correo',
                'rules' => 'trim|required|strtolower|callback_user_email_exists'
            ),
            array(
                'field' => 'user_name',
                'label' => 'correo',
                'rules' => 'trim|required|strtolower|callback_user_name_exists'
            ),
            array(
                'field' => 'user_type',
                'label' => 'tipo de usuario',
                'rules' => 'required'
            )
        );
        return $config;
    }
    
    function profile_validation() {
        $config = array(
            array(
                'field' => 'user_firstname',
                'label' => 'nombre',
                'rules' => 'required'
            ),
            array(
                'field' => 'user_lastname',
                'label' => 'apellido',
                'rules' => 'required'
            ),
            array(
                'field' => 'user_password',
                'label' => 'contraseña',
                'rules' => 'trim|min_length[6]'
            ),
            array(
                'field' => 'user_password_repeat',
                'label' => 'repetir contraseña',
                'rules' => 'trim|matches[user_password]'
            ),
            array(
                'field' => 'user_email',
                'label' => 'correo',
                'rules' => 'trim|required|strtolower|callback_user_email_exists'
            ),
            array(
                'field' => 'user_name',
                'label' => 'correo',
                'rules' => 'trim|required|strtolower|callback_user_name_exists'
            )
        );
        return $config;
    }
    
    function create_activation_hash($email){
        return md5('activation' . $this->input->post('email'));
    }

    function _read_manager_post() {
        $post = array(
            'user_firstname'    => $this->input->post('user_firstname', true),
            'user_lastname'     => $this->input->post('user_lastname', true),
            'user_name'         => $this->input->post('user_name', true),
            'user_type'         => $this->input->post('user_type', true)
        );
        if ($this->input->post('user_email')) {
            $post['user_email'] = strtolower($this->input->post('user_email', true));
        }
        if ($this->input->post('user_password') && $this->input->post('user_password_repeat') && $this->input->post('user_password') == $this->input->post('user_password_repeat')) {
            $post['user_password'] = md5($this->input->post('user_password', true));
        }

        return $post;
    }
    
    function _read_profile_post() {
        $post = array(
            'user_firstname'    => $this->input->post('user_firstname', true),
            'user_lastname'     => $this->input->post('user_lastname', true),
            'user_name'         => $this->input->post('user_name', true)
        );
        if ($this->input->post('user_email')) {
            $post['user_email'] = strtolower($this->input->post('user_email', true));
        }
        if ($this->input->post('user_password') && $this->input->post('user_password_repeat') && $this->input->post('user_password') == $this->input->post('user_password_repeat')) {
            $post['user_password'] = md5($this->input->post('user_password', true));
        }

        return $post;
    }

    function get_by_email($useremail, $userid = false) {
        $this->db->limit(1);
        $this->db->where(array('user_email' => $useremail));

        if (!empty($userid)) {
            $this->db->where('user_id !=', $userid);
        }
        $user = $this->db->get('users')->row_array();
        
        return $this->load_user_data($user);
    }
    
    function get_by_username($username, $userid = false) {
        $this->db->limit(1);
        $this->db->where(array('user_name' => $username));
        
        if (!empty($userid)) {
            $this->db->where('user_id !=', $userid);
        }
        $user = $this->db->get('users')->row_array();
        
        return $this->load_user_data($user);
    }

    function get_by_id_activation_hash($user_id, $activation_hash) {
        $this->db->limit(1);
        $user = $this->db->get_where('users', array('user_id' => $user_id, 'user_activation_hash' => $activation_hash))->row_array();
        return $this->load_user_data($user);
    }

    function get_by_id_reset_password_hash($user_id, $reset_hash) {
        $this->db->limit(1);
        $user = $this->db->get_where('users', array('user_id' => $user_id, 'user_password_reset_hash' => $reset_hash))->row_array();
        return $this->load_user_data($user);
    }

    function get($id) {
        $this->db->limit(1);
        $user = $this->db->get_where('users', array('user_id' => $id))->row_array();
        return $this->load_user_data($user);
    }

    function get_all() {
        $all = $this->db->get('users')->result_array();
        if (!empty($all)) {
            foreach ($all as $k => $user) {
                $all[$k] = $this->load_user_data($user);
            }
        }
        return $all;
    }
    
    function load_user_data($user) {
        if(empty($user)){
            return;
        }
        
        return $user;
    }

    function del($id) {
        $this->db->delete('users', array('user_id' => $id));
    }
    
    function change_active_status($userid, $state) {
        $this->update($userid, array('user_active' => $state));
    }

    function cookie_isvalid($cookie) {
        $cookie_duration = 60 * 60 * 24 * 30;

        $user = $this->db->get_where('users', array('user_cookie_hash' => $cookie))->row_array();
        if (!empty($user)) {
            if ($user['user_cookie_time'] + $cookie_duration > time()) {
                $this->db->where('user_id', $user['user_id']);
                $this->db->update('users', array('user_cookie_time' => time()));

                $tfcookie = array(
                    'name' => 'user_logged',
                    'value' => $cookie,
                    'expire' => 2592000,
                    'domain' => site_url(),
                    'path' => '/',
                );
                $this->input->set_cookie($tfcookie);
                return $user;
            }
        }
        return false;
    }

    function check_cookie() {
        if ($this->session->userdata('user_logged')) {
            return true;
        } else {
            //checking cookie
            $sessioncookie = $this->input->cookie('user_logged', TRUE);
            if ($sessioncookie) {
                $this->load->model('User_model');
                $user = $this->User_model->cookie_isvalid($sessioncookie);
                if ($user) {
                    //cookie exits - create session
                    $this->User_model->start_session($user);
                    return true;
                }
            }
        }
    }

    function update($userid, $data) {
        $this->db->where('user_id', $userid);
        $this->db->update('users', $data);
    }

}
