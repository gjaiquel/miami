<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Account_detail_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->table = 'account_details';
    }
    
    /*********************************************************
     * DATA BASE OPERATIONS
     *********************************************************/
    
    function add($detail) {
        $detail = $this->before_save($detail);
        $this->db->insert($this->table, $detail);
        return $this->db->insert_id();
    }
    
    function update($detail) {
        $detail = $this->before_save($detail);
        $this->db->where('id', $detail['id']);
        $this->db->update($this->table, $detail);
        return $detail['id'];
    }
    
    function save($tosave) {
        if (array_key_exists('id', $tosave)) {
            return $this->update($tosave);
        } else {
            return $this->add($tosave);
        }
    }
    
    function delete_by_reservation_number($reservation_number) {
        return $this->db->delete($this->table , 'reservation_number='.$reservation_number);
    }
    
    function delete_by_id($id) {
        return $this->db->delete($this->table , 'id=' . $id);
    }
    
    function get_by_reservation_numbers($reservation_numbers) {
        $reservations = array();
        foreach ($reservation_numbers as $row) {
            $reservations[] = $row["reservation_number"];
        }
        $all = $this->db
                ->where_in("reservation_number", $reservations)
                ->get($this->table)->result_array();
        foreach ($all as $k => $detail) {
            $all[$k] = $this->after_load($detail);
        }
        return $all;
    }
    
    function get_by_id($id) {
        $array = $this->db
                ->where_in("id", $id)
                ->get($this->table)->row_array();
        return $this->after_load($array);
    }
    
    function get_accounts_resume() {
        $rows = $this->db->query('SELECT '
                . 'a.agent_id,agent_type_name,agent_firstname,agent_lastname,agent_note,sum(amount) AS total '
                . 'FROM account_details d '
                . 'INNER JOIN agents a ON a.agent_id=d.agent_id '
                . 'INNER JOIN agent_types t ON t.agent_type_id=a.agent_type_id '
                . 'WHERE a.agent_active=1 GROUP BY agent_id')->result_array();
        foreach ($rows as $k => $row) {
            $rows[$k]['agent_fullname'] = $row['agent_firstname'] . ' ' . $row['agent_lastname'];
        }
        return $rows;
    }
    
    function get_details_by_agent_id($agent_id, $from, $to) {
        
        $from = $this->parse_date_format_to_mysql($from);
        $to = $this->parse_date_format_to_mysql($to);
        $rows = $this->db->query("SELECT * FROM account_details d LEFT JOIN reservations r ON d.reservation_number=r.reservation_number WHERE agent_id=".$agent_id." AND date >= '".$from."' AND date <= '".$to."' ORDER BY date ASC" )->result_array();
        $this->load->model('Apartment_model');
        $apartments = $this->Apartment_model->get_all();
        
        foreach ($rows as $k => $row) {
            $rows[$k] = $this->after_load($row);
            if($rows[$k]['reservation_number'] !== NULL) {
                $rows[$k]['arrival_date'] = $this->parse_date_format_to_visible($row['arrival_date']);
                $rows[$k]['departure_date'] = $this->parse_date_format_to_visible($row['departure_date']);
                $rows[$k]['apartment_short_name'] = $this->get_apartment_short_name($apartments, $row['room']);
            }
        }
        return $rows;
    }
    
    function get_total_before_date($agent_id, $date) {
        $date = $this->parse_date_format_to_mysql($date);
        $result = $this->db->query("SELECT SUM(amount) as total FROM account_details WHERE agent_id=".$agent_id." AND date < '".$date."'" )->row_array();
        return $result['total'];
    }
    
    function get_account_detail_types() {
        return $this->db->get('account_detail_types')->result_array();
    }
    
    /*********************************************************
     * READ POSTS & VALIDATIONS
     *********************************************************/
    
    function read_create_account_detail_post() {
        $result = array(
            'date'                 => $this->input->post('detail_date', true),
            'detail'               => $this->input->post('detail_text', true),
            'reservation_number'   => $this->input->post('reservation_number', true),
            'agent_id'             => $this->input->post('detail_agent_id', true),
            'amount'               => $this->input->post('detail_amount', true),
            'type_id'              => $this->input->post('detail_type_id', true),
        );
        if ($this->input->post('detail_id')) {
            $result['id'] = $this->input->post('detail_id', true);
        }
        return $result;
    }
    
    function new_validation() {
        $config = array(
            array(
                'field' => 'detail_date',
                'label' => 'fecha',
                'rules' => 'required'
            ),
            array(
                'field' => 'detail_text',
                'label' => 'descripción del detalle',
                'rules' => 'required|max_length[1000]'
            ),
            array(
                'field' => 'detail_agent_id',
                'label' => 'agente',
                'rules' => 'required'
            ),
            array(
                'field' => 'detail_amount',
                'label' => 'monto',
                'rules' => 'required'
            ),
            array(
                'field' => 'detail_type_id',
                'label' => 'tipo',
                'rules' => 'required'
            ),
        );
        return $config;
    }
    
    /*********************************************************
     * HELPERS
     *********************************************************/
    
    function before_save($detail) {
        $detail['reservation_number'] = $detail['reservation_number'] === '' ? NULL : $detail['reservation_number'];
        $detail['date'] = $this->parse_date_format_to_mysql($detail['date']);
        return $detail;
    }
     function after_load($detail) {
        $type = $this->db->get_where('account_detail_types', array('id' => $detail['type_id']))->row_array();
        $detail['type_name'] = $type['name'];
        $detail['date'] = $this->parse_date_format_to_visible($detail['date']);
        return $detail;
     }
     
     function parse_date_format_to_mysql($date) {
        $array = explode("/", $date);
        if(count($array) == 3) {
            return $array[2] . '-' . $array[1] . '-' . $array[0];
        }
        else {
            return $date;
        }
     }
     
     function parse_date_format_to_visible($date) {
        $array = explode("-", $date);
        if(count($array) == 3) {
            return $array[2] . '/' . $array[1] . '/' . $array[0];
        } else {
            return $date;
        }
     }
     
     function get_apartment_short_name($apartments, $name) {
         foreach ($apartments as $ap) {
             if($ap['apartment_reservation_name'] === $name) {
                 return $ap['apartment_short_name'];
             }
         }
         return $name;
     }
}
