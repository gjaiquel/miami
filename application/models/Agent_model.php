<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Agent_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function add($agent) {
        $this->db->insert('agents', $agent);
        return $this->db->insert_id();
    }

    function save($tosave) {
        if ($this->input->post('agent_id')) {
            $id = $this->input->post('agent_id');
        }
        if (!empty($id)) {
            $this->db->where('agent_id', $id);
            $this->db->update('agents', $tosave);
            return $id;
        } else {
            $this->db->insert('agents', $tosave);
            return $this->db->insert_id();
        }
    }

    function new_validation() {
        $config = array(
            array(
                'field' => 'agent_firstname',
                'label' => 'nombre',
                'rules' => 'required'
            ),
            array(
                'field' => 'agent_lastname',
                'label' => 'apellido',
                'rules' => 'required'
            ),
            array(
                'field' => 'agent_email',
                'label' => 'correo',
                'rules' => 'trim|required|strtolower'
            ),
            array(
                'field' => 'agent_type_id',
                'label' => 'tipo',
                'rules' => 'required'
            ),
            array(
                'field' => 'agent_keyword',
                'label' => 'palabra clave',
                'rules' => 'trim|required|min_length[3]|callback_agent_keyword_no_exist'
            )
        );
        return $config;
    }

    

    function read_create_agent_post() {
        return array(
            'agent_firstname'                 => $this->input->post('agent_firstname', true),
            'agent_lastname'                  => $this->input->post('agent_lastname', true),
            'agent_email'                     => $this->input->post('agent_email', true),
            'agent_type_id'                   => $this->input->post('agent_type_id', true),
            'agent_keyword'                   => $this->input->post('agent_keyword', true),
            'agent_predefined_value'          => $this->input->post('agent_predefined_value', true),
            'agent_note'                      => $this->input->post('agent_note', true)
        );
    }
    
    function get_by_email($agentemail, $agentid = false) {
        $this->db->limit(1);
        $this->db->where(array('agent_email' => $agentemail));

        if (!empty($agentid)) {
            $this->db->where('agent_id !=', $agentid);
        }
        $agent = $this->db->get('agents')->row_array();
        
        return $this->load_agent_data($agent);
    }
    
    function get_by_keyword($keyword, $agentid = false) {
        $this->db->limit(1);
        $this->db->where(array('agent_keyword' => $keyword));

        if (!empty($agentid)) {
            $this->db->where('agent_id !=', $agentid);
        }
        $agent = $this->db->get('agents')->row_array();
        
        return $this->load_agent_data($agent);
    }
    
    function get_appartment_owner($apartment) {
        $query = "SELECT ag.* FROM agents ag INNER JOIN apartments ap ON ag.agent_id = ap.apartment_agent_id WHERE ap.apartment_reservation_name = '" . $apartment . "'";
        $agent = $this->db->query($query)->row_array();
        return $this->load_agent_data($agent);
    }
    
    function get_appartment_checkin_agent($apartment) {
        $query = "SELECT ag.* FROM agents ag INNER JOIN apartments ap ON ag.agent_id = ap.apartment_agent_checkin WHERE ap.apartment_reservation_name = '" . $apartment . "'";
        $agent = $this->db->query($query)->row_array();
        return $this->load_agent_data($agent);
    }
    
    function get_by_type($type_id) {
        $all = $this->db->where('agent_type_id', $type_id)->get('agents')->result_array();
        if (!empty($all)) {
            foreach ($all as $k => $agent) {
                $all[$k] = $this->load_agent_data($agent);
            }
        }
        return $all;
    }
    
    function get_by_active_status($status) {
        $all = $this->db->where('agent_active', $status)->get('agents')->result_array();
        if (!empty($all)) {
            foreach ($all as $k => $agent) {
                $all[$k] = $this->load_agent_data($agent);
            }
        }
        return $all;
    }

    function get($id) {
        $this->db->limit(1);
        $agent = $this->db->get_where('agents', array('agent_id' => $id))->row_array();
        return $this->load_agent_data($agent);
    }

    function get_all() {
        $all = $this->db->get('agents')->result_array();
        if (!empty($all)) {
            foreach ($all as $k => $agent) {
                $all[$k] = $this->load_agent_data($agent);
            }
        }
        return $all;
    }
    
    function get_agent_types() {
        return $this->db->get('agent_types')->result_array();
    }
    
    function load_agent_data($agent) {
        if(empty($agent)){
            return;
        }
        $this->db->limit(1);
        $agent_type = $this->db->get_where('agent_types', array('agent_type_id' => $agent['agent_type_id']))->row_array();
        $agent['agent_fullname'] = $agent['agent_firstname'] . ' ' . $agent['agent_lastname'];
        $agent['agent_type'] = $agent_type['agent_type_name'];
        return $agent;
    }
    
    function change_active_status($userid, $state) {
        $this->update($userid, array('agent_active' => $state));
    }

    function del($id) {
        $this->db->delete('agents', array('agent_id' => $id));
    }


    function update($agentid, $data) {
        $this->db->where('agent_id', $agentid);
        $this->db->update('agents', $data);
    }

}
