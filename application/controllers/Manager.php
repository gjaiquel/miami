<?php

class Manager extends MY_Controller {

    public function __construct() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {
            die();
        }
        parent::__construct();
        $this->load->model('Reservation_model');
        if (!$this->session->userdata('user_logged')) {
            redirect('/login', 'refresh');
        }
        $this->template = 'manager';
        $this->agents_apartments = array();
        $this->agents_keyword = array();
        $this->fees_apartments = array();
        $this->data['incomplete_reservations'] = $this->Reservation_model->get_not_done_reservations_count();
        $this->reservation_status[] = 'Reserva Confirmada';
        $this->reservation_status[] = 'Reserva Incompleta';
        $this->reservation_status[] = 'Reserva REGISTRADA';
        $this->reservation_status[] = 'Renta de DE BELEN';
        $this->reservation_status[] = 'Renta de agente';
        $this->reservation_status[] = 'reserva lista';
        $this->reservation_status[] = 'Renta del duenio';
        $this->reservation_status[] = 'Checked Out';
        $this->reservation_status[] = 'Reserva a confirmar';
        $this->reservation_status[] = 'Reserva en alerta';
        $this->reservation_status[] = 'Renta cancelada';
        $this->reservation_status[] = 'Presupuesto';
    }
    
    /*************************************************************************
     * PAGES
     *************************************************************************/

    public function index() {
        $this->data['menu_active'] = 'index';
        $this->_render('manager/home');
    }
    
    public function users() {
        if($this->session->userdata('user_type') !== 'admin') {
            redirect('/manager', 'refresh');
        }
        $this->load->model('User_model');
        $this->data['menu_active'] = 'users';
        $this->data['list'] = $this->User_model->get_all();

        $this->css[] = array('url' => 'libs/dataTables.bootstrap.min.css');
        $this->javascript[] = array('url' => 'libs/jquery.dataTables.min.js');
        $this->javascript[] = array('url' => 'libs/dataTables.bootstrap.min.js');
        $this->_render('manager/user_list');
    }
    
    public function agents() {
        $this->load->model('Agent_model');
        $this->data['menu_active'] = 'agents';
        $this->data['list'] = $this->Agent_model->get_all();

        $this->css[] = array('url' => 'libs/dataTables.bootstrap.min.css');
        $this->javascript[] = array('url' => 'libs/jquery.dataTables.min.js');
        $this->javascript[] = array('url' => 'libs/dataTables.bootstrap.min.js');
        $this->_render('manager/agent_list');
    }
    
    public function apartments() {
        $this->load->model('Apartment_model');
        $this->data['menu_active'] = 'apartments';
        $this->data['list'] = $this->Apartment_model->get_all();

        $this->css[] = array('url' => 'libs/dataTables.bootstrap.min.css');
        $this->javascript[] = array('url' => 'libs/jquery.dataTables.min.js');
        $this->javascript[] = array('url' => 'libs/dataTables.bootstrap.min.js');
        $this->_render('manager/apartment_list');
    }
    
    public function upload() {
        if($this->session->userdata('user_type') !== 'admin') {
            redirect('/manager', 'refresh');
        }
        $this->data['menu_active'] = 'upload';
        $this->css[] = array('url' => 'libs/fileinput.min.css');
        $this->data['upload_page'] = 1;
        $this->javascript[] = $this->load->view('manager/upload_js', $this->data, true);
        $this->_render('manager/upload');
    }
    
    public function reprocess() {
        if($this->session->userdata('user_type') !== 'admin') {
            redirect('/manager', 'refresh');
        }
        $this->css[] = array('url' => 'libs/fileinput.min.css');
        $this->data['upload_page'] = 0;
        $this->javascript[] = $this->load->view('manager/upload_js', $this->data, true);
        $this->_render('manager/upload');
    }
    
    public function accounts() {
        $this->load->model('Account_detail_model');
        $this->data['menu_active'] = 'accounts';
        
        $this->data['list'] = $this->Account_detail_model->get_accounts_resume();
        
        $this->css[] = array('url' => 'libs/dataTables.bootstrap.min.css');
        $this->javascript[] = array('url' => 'libs/jquery.dataTables.min.js');
        $this->javascript[] = array('url' => 'libs/dataTables.bootstrap.min.js');
        $this->_render('manager/account_list');
    }
    /*************************************************************************
     * PAGES + METHODS
     *************************************************************************/

    public function account($agent_id = NULL) {
        $this->data['menu_active'] = 'accounts';
        if(empty($agent_id)) {
            redirect('/manager/accounts', 'refresh');
            return;
        }
        $this->load->model('Account_detail_model');
        $this->load->model('Agent_model');
        $agent = $this->Agent_model->get($agent_id);
        if(empty($agent)) {
            redirect('/manager/accounts', 'refresh');
            return;
        }
        $this->data['agent'] = $agent;
        
        if($this->session->userdata('date_from')) {
            $from = $this->session->userdata('date_from');
            $to = $this->session->userdata('date_to');
        } else {
            $from = date("d/m/Y", strtotime("first day of -1 month"));
            $to = date("d/m/Y", strtotime("last day of -1 month"));
        }
        if(!empty($this->input->post())) {
            $from = $this->input->post('date_from', true);
            $to = $this->input->post('date_to', true);
        }
        $usersession = $this->session->userdata();
        $usersession['date_from'] = $from;
        $usersession['date_to'] = $to;
        $this->session->set_userdata($usersession);
        
        
        $this->data['from'] = $from;
        $this->data['to'] = $to;
        $this->data['details'] = $this->Account_detail_model->get_details_by_agent_id($agent_id, $from, $to);
        $this->data['total_before'] = $this->Account_detail_model->get_total_before_date($agent_id, $from);

        $this->css[] = array('url' => 'libs/dataTables.bootstrap.min.css');
        $this->css[] = array('url' => 'libs/datepicker3.css');
        $this->javascript[] = array('url' => 'libs/plugins/datepicker/bootstrap-datepicker.js');
        $this->javascript[] = array('url' => 'libs/jquery.dataTables.min.js');
        $this->javascript[] = array('url' => 'libs/dataTables.bootstrap.min.js');
        $this->javascript[] = array('url' => 'account_resume.js');
        $this->_render('manager/account_resume');
    }
    
    public function account_detail($detail_id = NULL, $agent_id = 0) {
        if($this->session->userdata('user_type') !== 'admin') {
            redirect('/manager', 'refresh');
        }
        $this->load->model('Account_detail_model');
        $this->load->model('Agent_model');
        $this->data['menu_active'] = 'accounts';
        
        if(!empty($this->input->post())) { // saving detail received
            $this->load->library('form_validation');
            $_readedpost = $this->Account_detail_model->read_create_account_detail_post();
            $this->form_validation->set_rules($this->Account_detail_model->new_validation());
            
            if ($this->form_validation->run() == TRUE) {
                $this->Account_detail_model->save($_readedpost);
                $to_agent = $this->input->post('detail_agent_id_to', true);
                if(isset($to_agent) && $to_agent > 0) {
                    $_readedpost['agent_id'] = $to_agent;
                    $_readedpost['amount'] = 0 - $_readedpost['amount'];
                    $this->Account_detail_model->save($_readedpost);
                }
                redirect('/manager/account/' . $_readedpost['agent_id'], 'refresh');
            } else {
                $this->data['edit'] = $_readedpost;
                $this->data['errors'] = validation_errors();
            }
        } else { //render new user form or edit user
            if (!empty($detail_id)) {
                if(strcmp($detail_id, 'new') === 0) {
                    $this->data['agent_id'] = $agent_id;
                } else {
                    $this->data['edit'] = $this->Account_detail_model->get_by_id($detail_id);
                }
            }
        }
        
        $this->css[] = array('url' => 'libs/datepicker3.css');
        $this->javascript[] = array('url' => 'libs/plugins/datepicker/bootstrap-datepicker.js');
        $this->javascript[] = array('url' => 'account_detail.js');
        $this->data['agents'] = $this->Agent_model->get_by_active_status(1);
        $this->data['detail_types'] = $this->Account_detail_model->get_account_detail_types();
        $this->_render('manager/account_detail');
    }
    
    public function profile() {
        if (!empty($this->input->post())) { //saving user received
            $this->load->library('form_validation');
            $_readedpost = $this->User_model->_read_profile_post();
            $_POST['user_id'] = $this->session->userdata('user_id');
            $this->form_validation->set_rules($this->User_model->profile_validation());

            if ($this->form_validation->run() == TRUE) {
                $_POST['user_id'] = $this->session->userdata('user_id');
                $this->User_model->save($_readedpost);
                $this->User_model->load_session();
                $this->data['status'] = lang('Profile saved successfully');
            } else {
                $this->data['errors'] = validation_errors();
            }
        }
        
        $this->css[] = array('url' => 'libs/fileinput.min.css');
        $this->javascript[] = $this->load->view('manager/profile_js', $this->data, true);
        $this->_render('manager/profile');
    }
    
    public function user($user_id = false) {
        if($this->session->userdata('user_type') !== 'admin') {
            redirect('/manager', 'refresh');
        }
        $this->load->model('User_model');
        $this->data['menu_active'] = 'users';

        if (!empty($this->input->post())) { //saving user received
            $this->load->library('form_validation');
            $_readedpost = $this->User_model->_read_manager_post();
            $this->form_validation->set_rules($this->User_model->manager_validation());

            if ($this->form_validation->run() == TRUE) {
                $this->User_model->save($_readedpost);
                redirect('/manager/users', 'refresh');
            } else {
                $_readedpost['user_id'] = $this->input->post('user_id', true);
                $this->data['edit'] = $_readedpost;
                $this->data['errors'] = validation_errors();
            }
        } else { //render new user form or edit user
            if (!empty($user_id)) {
                $this->data['edit'] = $this->User_model->get($user_id);
            }
        }
        $this->_render('manager/user_form');
    }
    
    public function agent($agent_id = false) {
        if($this->session->userdata('user_type') !== 'admin') {
            redirect('/manager', 'refresh');
        }
        $this->load->model('Agent_model');
        $this->data['menu_active'] = 'agents';

        if (!empty($this->input->post())) { //saving user received
            $this->load->library('form_validation');
            $_readedpost = $this->Agent_model->read_create_agent_post();
            $this->form_validation->set_rules($this->Agent_model->new_validation());

            if ($this->form_validation->run() == TRUE) {
                $this->Agent_model->save($_readedpost);
                redirect('/manager/agents', 'refresh');
            } else {
                $_readedpost['agent_id'] = $this->input->post('agent_id', true);
                $this->data['edit'] = $_readedpost;
                $this->data['errors'] = validation_errors();
            }
        } else { //render new user form or edit user
            if (!empty($agent_id)) {
                $this->data['edit'] = $this->Agent_model->get($agent_id);
            }
        }
        $this->data['agent_types'] = $this->Agent_model->get_agent_types();
        $this->_render('manager/agent_form');
    }
    
    public function apartment($apartment_id = false) {
        if($this->session->userdata('user_type') !== 'admin') {
            redirect('/manager', 'refresh');
        }
        $this->load->model('Apartment_model');
        $this->data['menu_active'] = 'apartments';
        
        if (!empty($this->input->post())) { //saving user received
            $this->load->library('form_validation');
            $_readedpost = $this->Apartment_model->read_create_apartment_post();
            $this->form_validation->set_rules($this->Apartment_model->new_validation());

            if ($this->form_validation->run() == TRUE) {
                $this->Apartment_model->save($_readedpost);
                redirect('/manager/apartments', 'refresh');
            } else {
                $_readedpost['apartment_id'] = $this->input->post('apartment_id', true);
                $this->data['edit'] = $_readedpost;
                $this->data['errors'] = validation_errors();
            }
        } else { //render new user form or edit user
            if (!empty($apartment_id)) {
                $this->data['edit'] = $this->Apartment_model->get($apartment_id);
            }
        }
        $this->load->model('Agent_model');
        $this->data['owners'] = $this->Agent_model->get_by_type(1);
        $this->data['agents'] = $this->Agent_model->get_by_type(4);
        $this->_render('manager/apartment_form');
    }

    /*************************************************************************
     * METHODS
     *************************************************************************/
    
    public function user_email_exists() {
        $useremail = strtolower($this->input->post('user_email', true));

        $user = $this->User_model->get_by_email($useremail, $this->input->post('user_id', true));
        if (empty($user)) {
            return true;
        } else {
            $this->form_validation->set_message('user_email_exists', 'El email "' . $useremail . '" ya existe');
            return false;
        }
    }
    
    public function user_name_exists() {
        $username = strtolower($this->input->post('user_name', true));

        $user = $this->User_model->get_by_username($username, $this->input->post('user_id', true));
        if (empty($user)) {
            return true;
        } else {
            $this->form_validation->set_message('user_name_exists', 'El nombre de usuario "' . $username . '" ya existe');
            return false;
        }
    }
    
    public function agent_keyword_no_exist() {
        $keyword = $this->input->post('agent_keyword', true);
                
        $agent = $this->Agent_model->get_by_keyword($keyword, $agent_id = $this->input->post('agent_id', true));
        if (empty($agent)) {
            return true;
        } else {
            $this->form_validation->set_message('agent_keyword_no_exist', 'La palabra clave "' . $keyword . '" ya existe para el agente ' . $agent['agent_fullname']);
            return false;
        }
    }
    
    public function apartment_reservation_name_exists() {
        $name = $this->input->post('reservation_name', true);
        $apartment = $this->Apartment_model->get_by_reservation_name($name, $this->input->post('apartment_id', true));
        if(empty($apartment)) {
            return true;
        } else {
            $this->form_validation->set_message('apartment_reservation_name_exists', 'El numero en reservation "' . $name . '" ya existe');
            return false;
        }
    }
    
    public function apartment_short_name_exists() {
        $name = $this->input->post('short_name', true);
        $apartment = $this->Apartment_model->get_by_short_name($name, $this->input->post('apartment_id', true));
        if(empty($apartment)) {
            return true;
        } else {
            $this->form_validation->set_message('apartment_short_name_exists', 'El numero de departamento "' . $name . '" ya existe');
            return false;
        }
    }

    function user_remove($user_id) {
        if($this->session->userdata('user_type') !== 'admin') {
            redirect('/manager', 'refresh');
        }
        $this->load->model('User_model');
        $this->User_model->del($user_id);
        redirect('/manager/users', 'refresh');
    }
    
    function user_active($user_id, $state) {
        if($this->session->userdata('user_type') !== 'admin') {
            redirect('/manager', 'refresh');
        }
        $this->load->model('User_model');
        $this->User_model->change_active_status($user_id, $state);
        redirect('/manager/users', 'refresh');
    }
    
    function agent_remove($agent_id) {
        if($this->session->userdata('user_type') !== 'admin') {
            redirect('/manager', 'refresh');
        }
        $this->load->model('Agent_model');
        $this->Agent_model->del($agent_id);
        redirect('/manager/agents', 'refresh');
    }
    
    function agent_active($agent_id, $state) {
        if($this->session->userdata('user_type') !== 'admin') {
            redirect('/manager', 'refresh');
        }
        $this->load->model('Agent_model');
        $this->Agent_model->change_active_status($agent_id, $state);
        redirect('/manager/agents', 'refresh');
    }
    
    function apartment_remove($apartment_id) {
        if($this->session->userdata('user_type') !== 'admin') {
            redirect('/manager', 'refresh');
        }
        $this->load->model('Apartment_model');
        $this->Apartment_model->del($apartment_id);
        redirect('/manager/apartments', 'refresh');
    }
    
    function apartment_active($apartment_id, $state) {
        if($this->session->userdata('user_type') !== 'admin') {
            redirect('/manager', 'refresh');
        }
        $this->load->model('Apartment_model');
        $this->Apartment_model->change_active_status($apartment_id, $state);
        redirect('/manager/apartments', 'refresh');
    }
    
    function account_detail_remove($agent_id, $account_detail_id) {
        if($this->session->userdata('user_type') !== 'admin') {
            redirect('/manager', 'refresh');
        }
        $this->load->model('Account_detail_model');
        echo $this->Account_detail_model->delete_by_id($account_detail_id);
        redirect('/manager/account/' . $agent_id, 'refresh');
    }

    public function csv_upload() {
        if($this->session->userdata('user_type') !== 'admin') {
            redirect('/manager', 'refresh');
        }
        ini_set('upload_max_size', '5M');
        ini_set('post_max_size', '5M');
        ini_set('max_execution_time', '500');
        $response = array();
        $config = array();
        $config['upload_path'] = dirname(dirname(dirname(__FILE__))) . '/uploads/csv';
        $config['allowed_types'] = 'csv';

        $fileElementName = 'file_upload';
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($fileElementName)) {
            $response['success'] = false;
            $response['error'] = $this->upload->display_errors();
        } else {
            $data = $this->upload->data();
            $response['success'] = true;
            $response['file_name'] = $data['file_name'];
        }
        echo json_encode($response);
    }
    
    /**
     * Function called after file is uploaded
     */
    public function csv_to_db() {
        if($this->session->userdata('user_type') !== 'admin') {
            redirect('/manager', 'refresh');
        }
        $response['success'] = false;
        $this->load->model('Reservation_temp_model');
        $upload_path = dirname(dirname(dirname(__FILE__))) . '/uploads/csv/';
        $file_name = $this->input->post('file_name', true);
        $force_reprocess = $this->input->post('force_reprocess', true);
        // 1. Eliminar datos temporales de la BD
        if($this->Reservation_temp_model->delete_data() === true) {
            // 2. Importar CSV a la BD (tabla temporal)
            //$response['success'] = $this->Reservation_temp_model->load_from_file($upload_path . $file_name);
            $loaded = $this->Reservation_temp_model->load_from_file($upload_path . $file_name);
            if($loaded) {
                // 3. Hay reservas para updatear?
                $res_warnings = $this->get_reservations_warnings($force_reprocess);
                if($res_warnings['repeated'] > 0 || $res_warnings['canceled'] > 0) {
                    $response = array_merge($response, $res_warnings);
                } else {
                    $response = array_merge($response, $this->process_reservations($force_reprocess));
                }
                $response['success'] = true;
            } else {
                $response['success'] = false;
                $response['error'] = 'Error al importar archivo a la BD';
            }
        }
        unlink($upload_path . $file_name);
        echo json_encode($response);
    }
    
    public function get_reservations_warnings($force_reprocess) {
        $result = array();
        $result['repeated'] = 0;
        $result['canceled'] = 0;
        $result['reservations_canceled'] = array();
        $result['reservations'] = array();
        $result['temporal'] = array();
        $this->load->model('Reservation_temp_model');
        $this->load->model('Reservation_model');
        $reservation_temp = $this->Reservation_temp_model->get_all();
        if (!empty($reservation_temp)) {
            $reservations = $this->Reservation_model->get_by_reservation_numbers($reservation_temp);
            $repeated = array();
            $repeated_temp = array();
            $reservations_canceled = array();
            foreach ($reservations as $res) {
                foreach ($reservation_temp as $k => $temp) {
                    if($res['reservation_number'] === $temp['reservation_number']) {
                        $reservation_temp[$k]['repeated'] = 1;
                        if($force_reprocess == true || $this->compare_reservations($res, $temp) === false) {
                            $note1 = htmlspecialchars_decode(htmlspecialchars_decode($res['reservation_notes']));
                            $json1 = json_decode($note1, true);
                            if($json1 !== NULL) {
                                $res['reservation_notes'] = $json1;
                            }
                            $repeated[] = $res;
                            $note2 = htmlspecialchars_decode(htmlspecialchars_decode($temp['reservation_notes']));
                            $json2 = json_decode($note2, true);
                            if($json2 !== NULL) {
                                $temp['reservation_notes'] = $json2;
                            }
                            $repeated_temp[] = $temp;
                        }
                    }
                }
            }
            foreach ($reservation_temp as $temp) {
                if(strcmp($temp['status'], 'Reserva cancelada') === 0 && array_key_exists('repeated', $temp) === false) {
                    $reservations_canceled[] = $temp;
                }
            }
            $result['repeated'] = count($repeated);
            $result['canceled'] = count($reservations_canceled);
            $result['reservations_canceled'] = $reservations_canceled;
            $result['reservations'] = $repeated;
            $result['temporal'] = $repeated_temp;
        }
        return $result;
    }
    
    /**
     * 
     * @param type $res
     * @param type $temp
     * @return boolean true if all reservations fields are exactly the same, false if not
     */
    public function compare_reservations($res, $temp) {
        $result = true;
        foreach ($temp as $key => $value) {
            if(trim($value) !== trim($res[$key])) {
                $result = false;
            }
        }
        return $result;
    }
    
    public function process_reservations($force_reprocess) {
        $updated = 0;
        $inserted = 0;
        $ignored = 0;
        $this->load->model('Reservation_temp_model');
        $this->load->model('Reservation_model');
        
        // 1. Obtener reservas remporales
        $reservation_temp = $this->Reservation_temp_model->get_all();
        if(!empty($reservation_temp)) {
            // 2. Eliminar temporales
            $this->Reservation_temp_model->delete_data();
            // 3. Obtener reservas existentes
            $reservations = $this->Reservation_model->get_by_reservation_numbers($reservation_temp);
            // 4. Si ya existe la reserva:
            // - Updatear si tiene cambios
            // - Ignorar si es igual
            foreach ($reservations as $res) {
                foreach ($reservation_temp as $k => $temp) {
                    if($res['reservation_number'] === $temp['reservation_number']) {
                        if($force_reprocess == true || $this->compare_reservations($res, $temp) === false) {
                            $temp['done'] = false;
                            $this->Reservation_model->update($temp);
                            $updated++;
                        } else {
                            $ignored++;
                        }
                        // (marcar con 0 para saber que es repedito)
                        $reservation_temp[$k]['reservation_number'] = 0;
                    }
                }
            }

            foreach ($reservation_temp as $temp) {
                if($temp['reservation_number'] > 0) {
                    $this->Reservation_model->add($temp);
                    $inserted++;
                }
            }
            
        }
        $result = array();
        $result['updated'] = $updated;
        $result['inserted'] = $inserted;
        $result['ignored'] = $ignored;
        $result['total'] = $updated+$inserted+$ignored;
        $result['processed'] = json_decode($this->process_accounts());
        return $result;
    }
    
    public function process_accounts() {
        $this->load->model('Reservation_model');
        $this->load->model('Agent_model');
        $this->load->model('Account_detail_model');
        $processed = 0;
        $error = 0;
        $errorMessages = array();
        $this->agents_apartments = array();
        $this->agents_keyword = array();
        $reservations = $this->Reservation_model->get_not_done_reservations();
        foreach ($reservations as $res) {
            $hasError = false;
            $otherApartment = false;

            $note = htmlspecialchars_decode(htmlspecialchars_decode($res['reservation_notes']));
            $json = json_decode($note, true);
            if(strlen($note) > 0 && $json === NULL) {
                // tested
                $hasError = true;
                $error++;
                $errorMessages[] = 'Reserva ' . $res['reservation_number'] . '. Los datos de las notas no son correctos.';
            }
            $owner = array();
            $otherCheckin = array();
            if(strpos(strtolower($res['room']), 'otro') !== FALSE) {
                $otherApartment = true;
                if($json !== NULL && key_exists('otherOwner', $json) === TRUE) {
                    $owner = $this->Agent_model->get_by_keyword($json['otherOwner']);
                    if(empty($owner)) {
                        // tested
                        $hasError = true;
                        $error++;
                        $errorMessages[] = 'Reserva ' . $res['reservation_number'] . '. El propietario "' . $json['otherOwner'] . '" del departamento "' . $res['room'] . '" no existe';
                    }
                } else {
                    // tested
                    $hasError = true;
                    $error++;
                    $errorMessages[] = 'Reserva ' . $res['reservation_number'] . '. No se especificó el dueño del departamento.';
                }
                if($json !== NULL && key_exists('otherCheckin', $json) === TRUE) {
                    $otherCheckin = $this->Agent_model->get_by_keyword($json['otherCheckin']);
                    if(empty($otherCheckin)) {
                        // tested
                        $hasError = true;
                        $error++;
                        $errorMessages[] = 'Reserva ' . $res['reservation_number'] . '. El agente "' . $json['otherCheckin'] . '" del checkin no existe.';
                    }
                } else {
                    // tested
                    $hasError = true;
                    $error++;
                    $errorMessages[] = 'Reserva ' . $res['reservation_number'] . '. No se especificó el checkin del departamento.';
                }
                if($json !== NULL && key_exists('otherName', $json) === TRUE) {
                    if(strlen($json['otherName']) == 0) {
                        // tested
                        $hasError = true;
                        $error++;
                        $errorMessages[] = 'Reserva ' . $res['reservation_number'] . '. No se especificó el nombre del departamento.';
                    }
                } else {
                    // tested
                    $hasError = true;
                    $error++;
                    $errorMessages[] = 'Reserva ' . $res['reservation_number'] . '. No se especificó el nombre del departamento.';
                }
            } else {
                $owner = $this->get_agent_by_apartment($res['room']);
                if(empty($owner)) {
                    // tested
                    $hasError = true;
                    $error++;
                    $errorMessages[] = 'Reserva ' . $res['reservation_number'] . '. El propietario del departamento "' . $res['room'] . '" no existe.';
                }
            }
            $agent_keyword = strlen($res['second_guest_first_name']) > 0 ? $res['second_guest_first_name'] : NULL;
            $agent = array();
            if($agent_keyword !== null) {
                // tested
                $agent = $this->get_agent_by_keyword($agent_keyword);
                if(empty($agent)) {
                    // tested
                    $hasError = true;
                    $error++;
                    $errorMessages[] = 'Reserva ' . $res['reservation_number'] . '. No se pudo obtener los datos del agente "' . $res['second_guest_first_name'] . '".';
                }
            }
            if($hasError === false) {
                $this->Account_detail_model->delete_by_reservation_number($res['reservation_number']);
                switch (strtolower($res['status'])) {
                    case 'renta cancelada':
                        if($json !== NULL && key_exists('canceledPrice', $json) === TRUE && is_numeric($json['canceledPrice'])) {
                            $totalPayments = 0;
                            if($json !== NULL && key_exists('payments', $json)) {
                                foreach ($json['payments'] as $payment) {
                                    if(strlen($payment['detail']) > 0 && is_numeric($payment['value']) === true && strlen($payment['agent']) > 0) {
                                        $agentPayment = $this->get_agent_by_keyword($payment['agent']);
                                        if(!is_array($agentPayment)) {
                                            // tested
                                            $hasError = true;
                                            $error++;
                                            $errorMessages[] = 'Reserva ' . $res['reservation_number'] . '. El agente "' . $payment['agent'] . '" especificado en el pago no existe.';
                                            break;
                                        }
                                    } else if(strlen($payment['detail']) > 0 || is_numeric($payment['value']) === true || strlen($payment['agent']) > 0) {
                                        
                                        $hasError = true;
                                        $error++;
                                        $errorMessages[] = 'Reserva ' . $res['reservation_number'] . '. Hay pagos con datos incompletos.';
                                        break;
                                    }
                                    $totalPayments += $payment['value'];
                                }
                            }
                            if(!$hasError) {
                                // payments
                                if($json !== NULL && key_exists('payments', $json)) {
                                    foreach ($json['payments'] as $payment) {
                                        if(strlen($payment['detail']) > 0 && is_numeric($payment['value']) === true && strlen($payment['agent']) > 0) {
                                            $agentPayment = $this->get_agent_by_keyword($payment['agent']);
                                            $this->add_account_detail($res['arrival_date'], "Pagos: " . $payment['detail'], $res['reservation_number'], $agentPayment['agent_id'], 0 - $payment['value'], 3);
                                        }
                                    }
                                }
                                if($json['canceledPrice'] != 0) {
                                    $this->add_account_detail($res['arrival_date'], "Reserva cancelada", $res['reservation_number'], $owner['agent_id'], $json['canceledPrice'], 4);
                                }
                                if( ($totalPayments - $json['canceledPrice']) != 0 ) {
                                    $this->add_account_detail($res['arrival_date'], "Reserva cancelada", $res['reservation_number'], 1, $totalPayments - $json['canceledPrice'], 4);
                                }
                                $processed++;
                            }
                            
                            // DONE: de payments, obtengo el cobro por esa cancelacion y quien tiene esa plata. La diff (pos o negativa) de payments con el de cancelacion, es a favor de mte
                        } else {
                            // tested
                            $hasError = true;
                            $error++;
                            $errorMessages[] = 'Reserva ' . $res['reservation_number'] . '. No se especificó el precio por cancelación de reserva.';
                        }
                        break;
                    case 'renta del duenio':
                        $hasError = true;
                        $error++;
                        $errorMessages[] = 'Reserva ' . $res['reservation_number'] . '. El estado es "Renta del dueño", por favor pasarla a "Renta del dueño lista".';
                        break;
                    case 'renta del duenio lista':
                        // tested
                        if((float)$res['balance'] === 0.0 || (float)$res['balance'] === (float)$res['fees']) {
                            $agent_note = strlen($res['second_guest_last_name']) > 0 ? ' (' . $res['second_guest_last_name'] . ')' : '';
                            $detail = empty($agent) ? 'Reserva del propietario' : 'Reserva de ' . $agent['agent_fullname'] . $agent_note;
                            $this->add_account_detail($res['arrival_date'], $detail, $res['reservation_number'], $owner['agent_id'], 0, 4);
                            if((float)$res['fees'] > 0 && (float)$res['balance'] === 0.0) {
                                $this->add_account_detail($res['arrival_date'], $detail . ' - Fees', $res['reservation_number'], $owner['agent_id'], 0 - $res['fees'], 4);
                                $feeAgent = $this->get_check_in_agent_by_appartment($res['room']);
                                $this->add_account_detail($res['arrival_date'], "Fees", $res['reservation_number'], $feeAgent['agent_id'], $res['fees'], 4);
                            }
                        } else {
                            $hasError = true;
                            $error++;
                            $errorMessages[] = 'Reserva ' . $res['reservation_number'] . '. El balance tiene que ser cero o igual al fee. Balance: ' . $res['balance'] . ' Fees: ' . $res['fees'];
                        }
                        
                        /* 
                         * DONE
                         * si hay fee: es plata para el que hace el checkin
                         * - si el balance es 0, es plata que el dueño le debe al que hizo el checkin (agregarlo como gasto, no como reserva)
                         * - sino, ya lo cobró el fee el que hizo el checkin y no hace falta que quede el registro del fee
                         * (agregar un check: balance es 0 o igual al fee)
                         * */
                        $processed++;
                        break;
                    default:
                        $nightOneValue = $json !== NULL && key_exists('nightOneValue', $json) === TRUE && is_numeric($json['nightOneValue']) ? $json['nightOneValue'] : NULL;
                        if($nightOneValue === NULL || is_numeric($nightOneValue) === false) {
                            // tested
                            $hasError = true;
                            $error++;
                            $errorMessages[] = 'Reserva ' . $res['reservation_number'] . '. No se especificó el valor de la noche.';
                        }
                        $nightTwoValue = $json !== NULL && key_exists('nightTwoValue', $json) === TRUE && is_numeric($json['nightTwoValue']) ? $json['nightTwoValue'] : NULL;
                        $nightsTwoCount = $json !== NULL && key_exists('nightsTwoCount', $json) === TRUE && is_numeric($json['nightsTwoCount']) ? $json['nightsTwoCount'] : NULL;
                        if($nightTwoValue !== NULL || $nightsTwoCount !== NULL) {
                            if(is_numeric($nightTwoValue) === FALSE) {
                                // tested
                                $hasError = true;
                                $error++;
                                $errorMessages[] = 'Reserva ' . $res['reservation_number'] . '. El Valor Noche 2 no es válido.';
                            }
                            if(is_numeric($nightsTwoCount) === FALSE) {
                                // tested
                                $hasError = true;
                                $error++;
                                $errorMessages[] = 'Reserva ' . $res['reservation_number'] . '. Los días a Valor Noche 2 no es válido.';
                            }
                        }
                        $nightThreeValue = $json !== NULL && key_exists('nightThreeValue', $json) === TRUE && is_numeric($json['nightThreeValue']) ? $json['nightThreeValue'] : NULL;
                        $nightsThreeCount = $json !== NULL && key_exists('nightsThreeCount', $json) === TRUE && is_numeric($json['nightsThreeCount']) ? $json['nightsThreeCount'] : NULL;
                        if($nightThreeValue !== NULL || $nightsThreeCount !== NULL) {
                            if(is_numeric($nightThreeValue) === FALSE) {
                                // tested
                                $hasError = true;
                                $error++;
                                $errorMessages[] = 'Reserva ' . $res['reservation_number'] . '. El Valor Noche 3 no es válido.';
                            }
                            if(is_numeric($nightsThreeCount) === FALSE) {
                                // tested
                                $hasError = true;
                                $error++;
                                $errorMessages[] = 'Reserva ' . $res['reservation_number'] . '. Los días a Valor Noche 3 no es válido.';
                            }
                            if($nightTwoValue == NULL || $nightsTwoCount == NULL) {
                                // tested
                                $hasError = true;
                                $error++;
                                $errorMessages[] = 'Reserva ' . $res['reservation_number'] . '. El valor Noche-3 no puede ser usado si no hay valor Noche-2.';
                            }
                        }
                        $nightsOneCount = $res['number_of_nights'] - ($nightsTwoCount !== NULL ? $nightsTwoCount : 0) - ($nightsThreeCount !== NULL ? $nightsThreeCount : 0);
                        if($nightsOneCount < 1) {
                            $hasError = true;
                            $error++;
                            $errorMessages[] = 'Reserva ' . $res['reservation_number'] . '. La cantidad de noches en el detalle no coincide con la cantidad de noches reservadas.';
                        }
                        $commissionAgent = NULL;
                        if($json !== NULL && key_exists('commissionAgent', $json) === TRUE && strlen($json['commissionAgent']) > 0) {
                            $commissionAgent = $this->get_agent_by_keyword($json['commissionAgent']);
                            if(is_array($commissionAgent) === false) {
                                // tested
                                $hasError = true;
                                $error++;
                                $errorMessages[] = 'Reserva ' . $res['reservation_number'] . '. El comisionista "' . $json['commissionAgent'] . '" no existe.';
                            }
                        }
                        $earlier = $json !== NULL &&  key_exists('earlier', $json) === TRUE && is_numeric($json['earlier']) ? $json['earlier'] : 0;
                        $late = $json !== NULL &&  key_exists('late', $json) === TRUE && is_numeric($json['late']) ? $json['late'] : 0;
                        $totalPayments = 0;
                        if($json !== NULL && key_exists('payments', $json)) {
                            foreach ($json['payments'] as $payment) {
                                if(strlen($payment['detail']) > 0 && is_numeric($payment['value']) === true && strlen($payment['agent']) > 0) {
                                    $agentPayment = $this->get_agent_by_keyword($payment['agent']);
                                    if(!is_array($agentPayment)) {
                                        // tested
                                        $hasError = true;
                                        $error++;
                                        $errorMessages[] = 'Reserva ' . $res['reservation_number'] . '. El agente "' . $payment['agent'] . '" especificado en el pago no existe.';
                                        break;
                                    }
                                } else if(strlen($payment['detail']) > 0 || is_numeric($payment['value']) === true || strlen($payment['agent']) > 0) {
                                    // tested
                                    $hasError = true;
                                    $error++;
                                    $errorMessages[] = 'Reserva ' . $res['reservation_number'] . '. Hay pagos con datos incompletos.';
                                    break;
                                }
                                $totalPayments += $payment['value'];
                            }
                        }
                        if((float)$res['payments'] !== (float)$totalPayments) {
                            // tested
                            $hasError = true;
                            $error++;
                            $errorMessages[] = 'Reserva ' . $res['reservation_number'] . '. Los pagos detallados no coinciden con el total de pagos recibidos. ';
                        }
                        $totalExpenses = 0;
                        if($json !== NULL && key_exists('expenses', $json)) {
                            foreach ($json['expenses'] as $expense) {
                                if(strlen($expense['detail']) > 0 && is_numeric($expense['value']) === true && strlen($expense['paidBy']) > 0 && strlen($expense['ofAgent']) > 0) {
                                    if(strcasecmp($expense['paidBy'], $expense['ofAgent']) === 0 ) {
                                        // tested
                                        $hasError = true;
                                        $error++;
                                        $errorMessages[] = 'Reserva ' . $res['reservation_number'] . '. Un pago no puede ser hecho por la misma persona de quien lo es.';
                                        break;
                                    }
                                    $agentPaidBy = $this->get_agent_by_keyword($expense['paidBy']);
                                    if(empty($agentPaidBy)) {
                                        // tested
                                        $hasError = true;
                                        $error++;
                                        $errorMessages[] = 'Reserva ' . $res['reservation_number'] . '. El agente "' . $expense['paidBy'] . '" detallado en un gasto no existe.';
                                        break;
                                    }
                                    $expenseOfAgent = $this->get_agent_by_keyword($expense['ofAgent']);
                                    if(empty($expenseOfAgent)) {
                                        // tested
                                        $hasError = true;
                                        $error++;
                                        $errorMessages[] = 'Reserva ' . $res['reservation_number'] . '. El agente "' . $expense['ofAgent'] . '" detallado en un gasto no existe.';
                                        break;
                                    }
                                } else if(strlen($expense['detail']) > 0 || is_numeric($expense['value']) === true || strlen($expense['paidBy']) > 0 || strlen($expense['ofAgent']) > 0) {
                                    // tested
                                    $hasError = true;
                                    $error++;
                                    $errorMessages[] = 'Reserva ' . $res['reservation_number'] . '. Hay gastos con datos incompletos.';
                                    break;
                                }
                                $totalExpenses += $expense['value'];
                            }
                        }
                        if($hasError) {
                            break;
                        }
                        $commission = 0;
                        if($commissionAgent !== NULL) {
                            $commission = $res['total_room_price'] * 3.0 / 100;
                        }
                        $valueToOwner = 0;
                        $description = '';
                        if($nightThreeValue !== NULL) {
                            $valueToOwner = 
                                    $nightsOneCount * $nightOneValue 
                                    + $nightsTwoCount * $nightTwoValue
                                    + $nightsThreeCount * $nightThreeValue;
                            $description = 
                                    $nightsOneCount . ' Noche' . ($nightsOneCount > 1 ? 's' : '' ) . ' a $' . number_format($nightOneValue, 2, ',', '.') 
                                    . ', ' . $nightsTwoCount . ' Noche' . ($nightsTwoCount > 1 ? 's' : '') . ' a $' . number_format($nightTwoValue, 2, ',', '.')
                                    . ' y ' . $nightsThreeCount . ' Noche' . ($nightsThreeCount > 1 ? 's' : '') . ' a $' . number_format($nightThreeValue, 2, ',', '.');
                        } else if($nightTwoValue !== NULL) {
                            $valueToOwner = 
                                    $nightsOneCount * $nightOneValue 
                                    + $nightsTwoCount * $nightTwoValue;
                            $description = 
                                    $nightsOneCount . ' Noche' . ($nightsOneCount > 1 ? 's' : '') . ' a $' . number_format($nightOneValue, 2, ',', '.')
                                    . ' y ' . $nightsTwoCount . ' Noche' . ($nightsTwoCount > 1 ? 's' : '') . ' a $' . number_format($nightTwoValue, 2, ',', '.');
                        } else {
                            $valueToOwner = $nightsOneCount * $nightOneValue;
                            $description = $nightsOneCount . ' Noche' . ($nightsOneCount > 1 ? 's' : '') . ' a $' . number_format($nightOneValue, 2, ',', '.');
                        }
                        $calculatedExpenses = $res['total'] - $valueToOwner - $late - $earlier - $res['fees'] - $totalExpenses - $commission;
                        $diff = $calculatedExpenses > $res['expenses'] ? $calculatedExpenses - $res['expenses'] : $res['expenses']- $calculatedExpenses;
                        if( $diff > 0.009) {
                            $hasError = true;
                            $error++;
                            $errorMessages[] = 'Reserva ' . $res['reservation_number'] . '. El valor de Expenses no coincide con los datos de la reserva. Calculado:' . number_format($calculatedExpenses, 2, ',', '.') . " - Expenses (en reservation): ".$res['expenses'] . " - Diferencia:" . number_format($diff, 2, ',', '.') ;
                            break;
                        }
                        if(!$hasError) {
                            // REPARTO DE LA PLATA A FAVOR: Propietario, CheckIn, MTE
                            // Para el propietario
                            $this->add_account_detail($res['arrival_date'], $description, $res['reservation_number'], $owner['agent_id'], $valueToOwner, 4);
                            if($earlier > 0) {
                                $this->add_account_detail($res['arrival_date'], "Earlier Check-In", $res['reservation_number'], $owner['agent_id'], $earlier, 4);
                            }
                            if($late > 0) {
                                $this->add_account_detail($res['arrival_date'], "Late Check-Out", $res['reservation_number'], $owner['agent_id'], $late, 4);
                            }
                            // Comisionista: se lleva siempre el 3.0% del Total room price - gastos
                            if($commissionAgent !== NULL) {
                                $this->add_account_detail($res['arrival_date'], "Comisión", $res['reservation_number'], $commissionAgent['agent_id'], $commission, 4);
                            }
                            // Fees: el valor de Fees es para el que hace el checkin
                            // Si el Dpto es "otros": el fee es para $otherCheckin
                            if($otherApartment) {
                                $this->add_account_detail($res['arrival_date'], "Fees", $res['reservation_number'], $otherCheckin['agent_id'], $res['fees'], 4);
                            } else {
                                $feeAgent = $this->get_check_in_agent_by_appartment($res['room']);
                                $this->add_account_detail($res['arrival_date'], "Fees", $res['reservation_number'], $feeAgent['agent_id'], $res['fees'], 4);
                            }
                            // Ganancia para MTE
                            $this->add_account_detail($res['arrival_date'], "Ganancia", $res['reservation_number'], 1, $res['expenses'], 4);
                            
                            // REPARTO DE LA PLATA GASTADA
                            // expenses
                            if($json !== NULL && key_exists('expenses', $json)) {
                                foreach ($json['expenses'] as $expense) {
                                    if(strlen($expense['detail']) > 0 && is_numeric($expense['value']) === true && strlen($expense['paidBy']) > 0 && strlen($expense['ofAgent']) > 0) {
                                        $agentPaidBy = $this->get_agent_by_keyword($expense['paidBy']);
                                        $this->add_account_detail($res['arrival_date'], $expense['detail'], $res['reservation_number'], $agentPaidBy['agent_id'], $expense['value'], 5);
                                        if(strcmp(strtolower($expense['ofAgent']), 'mte') !== 0) {
                                            $expenseOfAgent = $this->get_agent_by_keyword($expense['ofAgent']);
                                            $this->add_account_detail($res['arrival_date'], "Gastos: " . $expense['detail'], $res['reservation_number'], $expenseOfAgent['agent_id'], 0 - $expense['value'], 5);
                                        }
                                    }
                                }
                            }
                            
                            // REPARTO DE LA PLATA COBRADA
                            // Balance: lo cobra la persona que hace el checking, es plata que se debe enviar a MTE
                            if($otherApartment) {
                                $this->add_account_detail($res['arrival_date'], "Cobrado en Check-In", $res['reservation_number'], $otherCheckin['agent_id'], 0 - $res['balance'], 4);
                            } else {
                                $this->add_account_detail($res['arrival_date'], "Cobrado en Check-In", $res['reservation_number'], $feeAgent['agent_id'], 0 - $res['balance'], 4);
                            }
                            
                            // payments
                            if($json !== NULL && key_exists('payments', $json)) {
                                foreach ($json['payments'] as $payment) {
                                    if(strlen($payment['detail']) > 0 && is_numeric($payment['value']) === true && strlen($payment['agent']) > 0) {
                                        $agentPayment = $this->get_agent_by_keyword($payment['agent']);
                                        $this->add_account_detail($res['arrival_date'], "Pagos: " . $payment['detail'], $res['reservation_number'], $agentPayment['agent_id'], 0 - $payment['value'], 3);
                                    }
                                }
                            }
                            $processed++;
                        }
                        // switch deafult break
                        break;
                }
                if(!$hasError) {
                    $res['done'] = true;
                    $this->Reservation_model->update($res);
                } else {
                    $this->Account_detail_model->delete_by_reservation_number($res['reservation_number']);
                }
            }
            
        }
        $results = array();
        $results['success'] = $error > 0 ? false : true;
        $results['processed'] = $processed;
        $results['errors'] = $error;
        $results['messages'] = $errorMessages;
        return json_encode($results);
    }
    
    public function get_process_accounts() {
        echo $this->process_accounts();
    }
    
    public function get_agent_by_apartment($apartment) {
        if(array_key_exists($apartment, $this->agents_apartments)) {
            return $this->agents_apartments[$apartment];
        } else {
            $this->load->model('Agent_model');
            $agent = $this->Agent_model->get_appartment_owner($apartment);
            $this->agents_apartments[$apartment] = $agent;
            return $agent;
        }
    }
    
    public function get_agent_by_keyword($keyword) {
        if(array_key_exists($keyword, $this->agents_keyword)) {
            return $this->agents_keyword[$keyword];
        } else {
            $this->load->model('Agent_model');
            $agent = $this->Agent_model->get_by_keyword($keyword);
            if(!empty($agent)) {
                $this->agents_keyword[$keyword] = $agent;
            }
            return $agent;
        }
    }
    
    private function get_agent_by_id($id) {
        foreach ($this->agents_keyword as $agent) {
            if($agent['agent_id'] === $id) {
                return $agent;
            }
        }
        $this->load->model('Agent_model');
        $agent = $this->Agent_model->get($id);
        $this->agents_keyword[$agent['agent_keyword']] = $agent;
        return $agent;
    }
    
    private function get_check_in_agent_by_appartment($apartment) {
        if(array_key_exists($apartment, $this->fees_apartments)) {
            return $this->fees_apartments[$apartment];
        } else {
            $this->load->model('Agent_model');
            $agent = $this->Agent_model->get_appartment_checkin_agent($apartment);
            $this->fees_apartments[$apartment] = $agent;
            return $agent;
        }
    }
    
    public function add_account_detail($date, $description, $reservation, $agent_id, $amount, $type_id) {
        $this->load->model('Account_detail_model');
        // date format yyyy/mm/dd
        $detail = array();
        $detail['date'] = $date;
        $detail['detail'] = $description;
        // puede ser NULL
        $detail['reservation_number'] = $reservation;
        $detail['agent_id'] = $agent_id;
        $detail['amount'] = $amount;
        $detail['type_id'] = $type_id;
        return $this->Account_detail_model->add($detail);
    }


    /**
     * Function called after confirming uploading file and confirming changes
     */
    public function overwrite_reservations($force_reprocess) {
        $result['success'] = true;
        $result = array_merge($result, $this->process_reservations($force_reprocess));
        echo json_encode($result);
    }
    
    public function send_account_resume_email($agent_id) {
        $this->load->model('Agent_model');
        $this->load->model('Account_detail_model');
        
        $from = $this->input->post('from', true);
        $to = $this->input->post('to', true);
        
        $data['agent'] = $this->Agent_model->get($agent_id);
        $data['details'] = $details = $this->Account_detail_model->get_details_by_agent_id($agent_id, $from, $to);
        $data['total_before'] = $total_before = $this->Account_detail_model->get_total_before_date($agent_id, $from);
        $data['from'] = $from;
        $data['to'] = $to;
        
        $body = $this->load->view('manager/account_resume_email_template', $data, true);
        
        $this->load->helper('send_email');
        $tosend = array(
            'to' => $data['agent']['agent_email'],
            'subject' => 'Resumen de cuenta',
            'message' => $body);
        
        $result['sent'] = $this->send_email_phpmailer($tosend);
        echo json_encode($result);
    }
    
    function send_email_phpmailer($data) {
        // Credits: https://github.com/RajeshRP/phpmailer-codeigniter
        $this->load->library("PhpMailerLib");
        $mail = $this->phpmailerlib->load();
	try {
            //Server settings
            $mail->SMTPDebug = 0; // Enable verbose debug output
            $mail->isSMTP(); // Set mailer to use SMTP
            $mail->Host = 'smtp.gmail.com'; // Specify main and backup SMTP servers
            $mail->SMTPAuth = true; // Enable SMTP authentication
            $mail->Username = 'mtenoreply@gmail.com'; // SMTP username
            $mail->Password = 'eSbd9P158f'; // SMTP password
            $mail->SMTPSecure = 'ssl'; // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 465; // TCP port to connect to
            //Recipients
            $mail->setFrom('mtenoreply@gmail.com', 'Miami Te Espera');
            
            $to = explode(",", $data['to']);
            for($i = 0; $i < count($to); $i++) {
                $mail->addAddress(trim($to[$i]));     // Add a recipient
            }
            // $mail->addAddress('RECEIPIENTEMAIL02');               // Name is optional
            // $mail->addReplyTo('RECEIPIENTEMAIL03', 'Ganesha');
            // $mail->addCC('cc@example.com');
            $mail->addReplyTo('miamiteespera@gmail.com', 'Miami Te Espera');
            //$mail->addBCC('miamiteespera@gmail.com');

            //Attachments
            //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $data['subject'];
            $mail->Body    = $data['message'];
            //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

            $mail->send();
            //echo 'Message has been sent';
            return true;
        } catch (Exception $e) {
            //echo 'Message could not be sent.';
            //echo 'Mailer Error: ' . $mail->ErrorInfo;
            return false;
        }
    }
    
    public function delete_reservation_by_number() {
        if($this->session->userdata('user_type') !== 'admin') {
            redirect('/manager', 'refresh');
        }
        $this->load->model('Reservation_model');
        $this->load->model('Account_detail_model');
        $reservation = $this->input->post('reservation', true);
        $result['status'] = 'fail';
        $reservations = array();
        $reservations[] = $reservation;
        if($this->Account_detail_model->delete_by_reservation_number($reservation)) {
            if($this->Reservation_model->delete_by_reservation_numbers($reservations)) {
                $result['status'] = 'success';
            } else {
                $result['message'] = 'Error while deleting reservations';
            }
        } else {
            $result['message'] = 'Error while deleting details';
        }
        echo json_encode($result);
    }
    
    public function get_reservation_details() {
        $this->load->model('Reservation_model');
        $result['status'] = 'error';
        $reservation = $this->input->post('reservation', true);
        $row = $this->Reservation_model->get_by_reservation_number($reservation);
        if(!empty($row)) {
            $result['status'] = 'success';
            $result['reservation'] = $row;
        }
        echo json_encode($result);
    }
}
