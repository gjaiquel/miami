<?php

if (!defined('BASEPATH')){
    exit('No direct script access allowed');
}

class User extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        redirect('/home', 'refresh');
    }
    
    /**
     * METHOD to access to the system
     */
    public function get_login() {
        $this->load->model('User_model');
        $this->load->library('form_validation');

        $this->form_validation->set_rules($this->User_model->login_validation());
        $logtry['status'] = 'error';

        if ($this->form_validation->run() == TRUE) {
            $user_name = strtolower($this->input->post('user'));
            $user = $this->User_model->get_by_username($user_name);
            //load session
            $this->User_model->start_session($user);

            //creating cookie 
            if ($this->input->post('createcookie') == 1) {
                $cookie_hash = md5($user['user_name'] . 'validate' . time());
                $cookie = array(
                    'name' => 'user_logged',
                    'value' => $cookie_hash,
                    'expire' => 2592000,
                );
                $toupdate['user_cookie_hash'] = $cookie_hash;
                $toupdate['user_cookie_time'] = time();
                $this->input->set_cookie($cookie);

                //save user cookie record
                $this->User_model->update($user['user_id'], $toupdate);
            }

            if ($this->session->userdata('user_logged')) {
                $logtry['status'] = 'success';
            }
        } else {
            $logtry['errors'] = validation_errors();
        }
        echo json_encode($logtry);
        die();
    }

    /**
     * METHOD
     */
    public function logout() {
        $tfcookie = array(
            'name' => 'user_logged',
            'value' => '',
            'expire' => '',
        );
        $this->input->set_cookie($tfcookie);
        $this->session->sess_destroy();
        redirect('/', 'refresh');
    }

    public function user_exists() {
        $username = strtolower($this->input->post('user'));

        $user = $this->User_model->get_by_username($username);
        if (empty($user)) {
            return true;
        } else {
            $this->form_validation->set_message('user_exists', lang('home.index.signup.msg.user.exists'));
            return false;
        }
    }

    
    
    public function password_check() {
        $this->load->model('User_model');
        $user_name = strtolower($this->input->post('user'));
        $password = $this->input->post('password');
        
        $user = $this->User_model->get_by_username($user_name);
        if (empty($user) || $user['user_active'] == 0) {
            $this->form_validation->set_message('password_check', lang('home.index.login.msg.wrong.user'));
            return false;
        }
        if (empty($password) && trim($password) == '') {
            $this->form_validation->set_message('password_check', lang('home.index.login.msg.type.pass'));
            return false;
        }
        if ($user['user_login_attempts'] > 5) {
            $this->form_validation->set_message('password_check', 'Usuario bloqueado');
            return false;
        }
        if (empty($user['user_password']) || ($user['user_password']) != md5($password)) {
            $this->form_validation->set_message('password_check', lang('home.index.login.msg.wrong.pass'));
            $user['user_login_attempts'] = $user['user_login_attempts'] + 1;
            $this->User_model->update($user['user_id'], $user);
            return false;
        } else {
            $user['user_login_attempts'] = 0;
            $this->User_model->update($user['user_id'], $user);
            return true;
        }
    }
    
    public function login_attempts_check() {
        $this->load->model('User_model');
        $user_name = strtolower($this->input->post('user'));
        
        $user = $this->User_model->get_by_username($user_name);
        if (!empty($user) && $user['user_login_attempts'] > 5) {
            $this->form_validation->set_message('login_attempts_check', 'Usuario bloqueado');
            return false;
        }
        return true;
    }
     /*
    public function create_user(){
        $response=$this->get_google_captcha();
        if($response['success']){
            $this->load->model('User_model');
            $this->load->library('form_validation');
            $user = $this->User_model->read_create_user_post();
            $this->form_validation->set_rules($this->User_model->new_validation());
            $user_id = 0;
            $logtry['status'] = 'error';
            if ($this->form_validation->run() == TRUE) {
                $user_id = $this->User_model->add($user);
            } else {
                $logtry['errors'] = validation_errors();
            }
            if ($user_id > 0) {
                $logtry['status'] = 'success';
                $user['user_id'] = $user_id;
                $this->send_activation_email($user);
            }
            echo json_encode($logtry);
        } else {
            $logtry['status'] = 'error';
            $logtry['errors'] = 'Please try again';
            echo json_encode($logtry);
        }
        die();
    }
    */
    
    /**
     * METHOD called from home to get email and start reset password procedure
     */
    public function get_password_reset(){
        $this->load->model('User_model');
        $this->load->library('form_validation');

        $this->form_validation->set_rules($this->User_model->start_reset_password_validation());
        $logtry['status'] = 'error';
        
        if ($this->form_validation->run() == TRUE) {
            $user_email = strtolower($this->security->xss_clean($this->input->post('email')));
            $user = $this->User_model->get_by_email($user_email);
            if(!empty($user)){
                $time = date('Y-m-d H:i:s', time());
                $hash = md5($time.'hash'.$user_email);
                $data = array('user_password_reset_hash' => $hash,
                              'user_password_reset_time' => $time);
                $this->User_model->update($user['user_id'], $data);
                $user['user_password_reset_hash'] = $hash;
                $this->send_reset_password_email($user);
                $logtry['status'] = 'success';
            } else {
                $logtry['errors'] = 'No existe usuario con el email ingresado';
            }
        } else {
            $logtry['errors'] = validation_errors();
        }
        echo json_encode($logtry);
    }
    
    /**
     * PAGE acceded from url, to draw reset password form
     * 
     * @param type $user_id
     * @param type $reset_hash
     * @return boolean
     */
    public function reset_password($user_id, $reset_hash){
        $this->load->model('User_model');
        $user = $this->User_model->get_by_id_reset_password_hash(
                            $this->security->xss_clean($user_id), 
                            $this->security->xss_clean($reset_hash));
        $this->data['user_id'] = $user_id;
        $this->data['reset_hash'] = $reset_hash;

        if(empty($user)){
            redirect(base_url());
            return false;
        } else {
            $phpdate = strtotime( $user['user_password_reset_time'] );
            if( (time()-$phpdate) < 24*60*60 ){
                $data['user_id'] = $user_id;
                $data['reset_hash'] = $reset_hash;
                $this->load->view('manager/reset_password', $data);
            } else {
                $data['user_id'] = $user_id;
                $data['reset_hash'] = '';
                $this->load->view('manager/reset_password', $data);
            }
            return true;
        }
    }
    
    /**
     * METHOD called from reset password form, to finish reset password procedure
     */
    public function set_password(){
        $this->load->model('User_model');
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules($this->User_model->reset_password_validation());
        $logtry['status'] = 'error';
        if ($this->form_validation->run() == TRUE) {
            $user_id = $this->security->xss_clean($this->input->post('user_id'));
            $reset_hash = $this->security->xss_clean($this->input->post('reset_hash'));
            $password = $this->security->xss_clean($this->input->post('password'));
            $user = $this->User_model->get($user_id);
            $phpdate = strtotime( $user['user_password_reset_time'] );
            if(!empty($user) && $user['user_password_reset_hash'] == $reset_hash && (time()-$phpdate)<24*60*60){
                $data = array(  'user_password' => md5($password), 
                                'user_password_reset_hash' => '',
                                'user_login_attempts' => 0);
                $this->User_model->update($user_id, $data);
                $logtry['status'] = 'success';
            } else {
                $logtry['errors'] = 'No se ha podido reestablecer la contraseña, inicie el proceso de nuevo por favor';
            }
            
        } else {
            $logtry['errors'] = validation_errors();
        }
        echo json_encode($logtry);
    }

    public function keep_alive() {
        $return['status'] = 'error';
        if ($this->session->userdata('user_logged')) {
            $return['status'] = 'alive';
        }
        echo json_encode($return);
    }
    
    public function send_reset_password_email($user) {
        $link = base_url().'user/reset_password/'.$user['user_id'].'/'.$user['user_password_reset_hash'];
        $name = $user['user_firstname'];
        $body = '<p>'.$name.'</p><p>Para reestablecer la contraseña de Miami Te Espera, haga click en el siguiente <a href="' . $link . '">link</a>';
        
        $this->load->helper('send_email');
        $tosend = array(
            'to' => $user['user_email'],
            'subject' => 'Reestablecer contraseña',
            'message' => $body);
        $response_email = send_email($tosend);
    }
    

}
