<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends MY_Controller {

    public function __construct() {
        parent::__construct();
        
        if ($this->session->userdata('user_logged')) {
            redirect('/manager', 'refresh');
        }
    }

    public function index() {
        $this->load->view('manager/login');
    }
}
