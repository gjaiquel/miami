-- MySQL dump 10.13  Distrib 5.6.23, for osx10.8 (x86_64)
--
-- Host: localhost    Database: mte
-- ------------------------------------------------------
-- Server version	5.6.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account_detail_types`
--

DROP TABLE IF EXISTS `account_detail_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_detail_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_detail_types`
--

LOCK TABLES `account_detail_types` WRITE;
/*!40000 ALTER TABLE `account_detail_types` DISABLE KEYS */;
INSERT INTO `account_detail_types` VALUES (1,'Otros'),(2,'Transferencia'),(3,'Seña'),(4,'Reserva'),(5,'Gastos');
/*!40000 ALTER TABLE `account_detail_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_details`
--

DROP TABLE IF EXISTS `account_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `detail` varchar(1000) NOT NULL,
  `reservation_number` int(11) DEFAULT NULL,
  `agent_id` int(11) NOT NULL,
  `amount` float NOT NULL,
  `type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=261 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_details`
--

LOCK TABLES `account_details` WRITE;
/*!40000 ALTER TABLE `account_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agent_types`
--

DROP TABLE IF EXISTS `agent_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agent_types` (
  `agent_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `agent_type_name` varchar(100) NOT NULL,
  PRIMARY KEY (`agent_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agent_types`
--

LOCK TABLES `agent_types` WRITE;
/*!40000 ALTER TABLE `agent_types` DISABLE KEYS */;
INSERT INTO `agent_types` VALUES (1,'Propietario'),(2,'Agente'),(3,'Comisionista'),(4,'Personal'),(5,'Empresa');
/*!40000 ALTER TABLE `agent_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agents`
--

DROP TABLE IF EXISTS `agents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agents` (
  `agent_id` int(11) NOT NULL AUTO_INCREMENT,
  `agent_firstname` varchar(100) NOT NULL,
  `agent_lastname` varchar(100) NOT NULL,
  `agent_email` varchar(255) NOT NULL,
  `agent_type_id` int(11) NOT NULL,
  `agent_keyword` varchar(50) NOT NULL,
  `agent_predefined_value` varchar(50) NOT NULL,
  `agent_active` tinyint(1) NOT NULL DEFAULT '1',
  `agent_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `agent_note` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`agent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agents`
--

LOCK TABLES `agents` WRITE;
/*!40000 ALTER TABLE `agents` DISABLE KEYS */;
INSERT INTO `agents` VALUES (1,'MTE','Co','miamiteespera@hotmail.com',5,'mte','',1,0,''),(2,'Eniele','D','eniele@gmail.com',2,'eniele','',1,0,''),(3,'Natalia','N','natalia@gmail.com',2,'naty','',1,0,''),(4,'Gustavo','Aquino','dos@two.com',1,'gustavo','',1,0,''),(5,'Patricia','Maggi','patri@gmail.com',2,'patricia','',1,0,''),(8,'Pamela','S','uno@one.com',2,'pamela','',1,0,''),(12,'Marine','Gonzalez','sad@asd.com',1,'marine','',1,0,''),(13,'Isabel','Ponse','isa@gmail.com',4,'isabel','',1,0,''),(14,'Agueda','Adamo','asd@asd.com',1,'agueda','',1,0,''),(15,'Marcelo','Slonick','asd@sad.com',2,'marcelo','',1,0,''),(16,'Norma','Corigliano','asd@sdfs.com',1,'norma','',1,0,''),(17,'Laura','Crosetti','asd@dsf.com',1,'laura','',1,0,''),(18,'Sabrina','Carranza','asd@sad.com',4,'sabrina','',1,0,''),(19,'Airbnb','Web','asd@asd.com',2,'airbnb','',1,0,''),(20,'Athi','Pantalef','asd@sad.com',3,'athi','',1,0,'');
/*!40000 ALTER TABLE `agents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apartments`
--

DROP TABLE IF EXISTS `apartments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apartments` (
  `apartment_id` int(11) NOT NULL AUTO_INCREMENT,
  `apartment_reservation_name` varchar(100) NOT NULL,
  `apartment_short_name` varchar(100) NOT NULL,
  `apartment_agent_id` int(11) NOT NULL,
  `apartment_agent_checkin` int(11) NOT NULL,
  `apartment_active` tinyint(1) NOT NULL DEFAULT '1',
  `apartment_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`apartment_id`),
  UNIQUE KEY `apartment_reservation_name` (`apartment_reservation_name`),
  UNIQUE KEY `apartment_short_name` (`apartment_short_name`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apartments`
--

LOCK TABLES `apartments` WRITE;
/*!40000 ALTER TABLE `apartments` DISABLE KEYS */;
INSERT INTO `apartments` VALUES (5,'3901 7W 7W','7W',12,13,1,0),(13,'3801 8E 8E','8E',16,13,1,0),(14,'3801 5V 5V','5V',4,13,1,0),(15,'Crossetti Beach Collins Beach','CBCB',17,18,1,0),(17,'3801 3P 3P','3P',4,13,1,0),(19,'3801 6T 6T','6T',12,13,1,0),(20,'3801 5D 5D','5D',14,13,1,0),(21,'3901 10C 10C','10C',16,13,1,0),(22,'3801 5U 5U','5U',12,13,1,0);
/*!40000 ALTER TABLE `apartments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservations`
--

DROP TABLE IF EXISTS `reservations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservations` (
  `reservation_number` int(10) unsigned NOT NULL,
  `status` varchar(100) NOT NULL,
  `room` varchar(100) NOT NULL,
  `fraction` varchar(100) NOT NULL,
  `guest_first_name` varchar(100) NOT NULL,
  `guest_last_name` varchar(100) NOT NULL,
  `second_guest_first_name` varchar(100) NOT NULL,
  `second_guest_last_name` varchar(100) NOT NULL,
  `guest_email` varchar(255) NOT NULL,
  `guest_address` varchar(255) NOT NULL,
  `guest_city` varchar(255) NOT NULL,
  `guest_state` varchar(100) NOT NULL,
  `guest_zip_code` varchar(50) NOT NULL,
  `guest_country` varchar(100) NOT NULL,
  `guest_home_phone` varchar(100) NOT NULL,
  `guest_business_phone` varchar(100) NOT NULL,
  `guest_mobile_phone` varchar(100) NOT NULL,
  `guest_company` varchar(100) NOT NULL,
  `contact_notes` text NOT NULL,
  `check_in_time` datetime NOT NULL,
  `reservation_notes` text NOT NULL,
  `date_booked` date NOT NULL,
  `arrival_date` date NOT NULL,
  `departure_date` date NOT NULL,
  `number_of_nights` int(11) NOT NULL,
  `total_room_price` float NOT NULL,
  `discounts` float NOT NULL,
  `fees` float NOT NULL,
  `expenses` float NOT NULL,
  `taxes` float NOT NULL,
  `items_for_sale` float NOT NULL,
  `total` float NOT NULL,
  `payments` float NOT NULL,
  `balance` float NOT NULL,
  `number_of_adults` int(11) NOT NULL,
  `number_of_children` int(11) NOT NULL,
  `created_by_user_name` varchar(100) NOT NULL,
  `pago_con_tarjeta_py` varchar(100) NOT NULL,
  `pago_con_paypal` varchar(100) NOT NULL,
  `Miami` varchar(100) NOT NULL,
  `done` tinyint(1) NOT NULL,
  PRIMARY KEY (`reservation_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservations`
--

LOCK TABLES `reservations` WRITE;
/*!40000 ALTER TABLE `reservations` DISABLE KEYS */;
/*!40000 ALTER TABLE `reservations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservations_temp`
--

DROP TABLE IF EXISTS `reservations_temp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservations_temp` (
  `reservation_number` int(10) unsigned NOT NULL,
  `status` varchar(100) NOT NULL,
  `room` varchar(100) NOT NULL,
  `fraction` varchar(100) NOT NULL,
  `guest_first_name` varchar(100) NOT NULL,
  `guest_last_name` varchar(100) NOT NULL,
  `second_guest_first_name` varchar(100) NOT NULL,
  `second_guest_last_name` varchar(100) NOT NULL,
  `guest_email` varchar(255) NOT NULL,
  `guest_address` varchar(255) NOT NULL,
  `guest_city` varchar(255) NOT NULL,
  `guest_state` varchar(100) NOT NULL,
  `guest_zip_code` varchar(50) NOT NULL,
  `guest_country` varchar(100) NOT NULL,
  `guest_home_phone` varchar(100) NOT NULL,
  `guest_business_phone` varchar(100) NOT NULL,
  `guest_mobile_phone` varchar(100) NOT NULL,
  `guest_company` varchar(100) NOT NULL,
  `contact_notes` text NOT NULL,
  `check_in_time` datetime NOT NULL,
  `reservation_notes` text NOT NULL,
  `date_booked` date NOT NULL,
  `arrival_date` date NOT NULL,
  `departure_date` date NOT NULL,
  `number_of_nights` int(11) NOT NULL,
  `total_room_price` float NOT NULL,
  `discounts` float NOT NULL,
  `fees` float NOT NULL,
  `expenses` float NOT NULL,
  `taxes` float NOT NULL,
  `items_for_sale` float NOT NULL,
  `total` float NOT NULL,
  `payments` float NOT NULL,
  `balance` float NOT NULL,
  `number_of_adults` int(11) NOT NULL,
  `number_of_children` int(11) NOT NULL,
  `created_by_user_name` varchar(100) NOT NULL,
  `pago_con_tarjeta_py` varchar(100) NOT NULL,
  `pago_con_paypal` varchar(100) NOT NULL,
  `Miami` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservations_temp`
--

LOCK TABLES `reservations_temp` WRITE;
/*!40000 ALTER TABLE `reservations_temp` DISABLE KEYS */;
/*!40000 ALTER TABLE `reservations_temp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(200) NOT NULL,
  `user_firstname` varchar(200) NOT NULL,
  `user_lastname` varchar(200) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_password` varchar(64) NOT NULL,
  `user_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_login_attempts` int(11) NOT NULL DEFAULT '0',
  `user_password_reset_hash` varchar(64) DEFAULT NULL,
  `user_password_reset_time` timestamp NULL DEFAULT NULL,
  `user_active` tinyint(1) NOT NULL DEFAULT '1',
  `user_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','Admin','MTE','admin@mte.com','d8578edf8458ce06fbc5bb76a58c5ca4','2017-02-16 04:46:25',0,'',NULL,1,0),(5,'guille','Guillermo','Aiquel','gjaiquel@gmail.com','efe6398127928f1b2e9ef3207fb82663','2017-02-19 14:30:03',0,'532b56ee9c04becb44116f9f8949efd0','2017-12-01 23:51:20',1,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-11 14:58:31
