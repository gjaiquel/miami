var app = app || {};
app.language = 'spanish';

app.sessionAlertPop = false;
app.sessionErrorAttempting = false;
app.sessionErrorWait = 5000; //original value 5000
app.keepAliveWait = 240000; //original value 240000

app.keepAlive = function () {
    $.get(app.baseUrl + 'user/keep_alive', '', function (data) {
        if (data.status === 'alive') {
            if (app.sessionErrorAttempting) {
                app.sessionRestored();
                app.sessionErrorWait = 5000;
            }
            window.setTimeout(app.keepAlive, app.keepAliveWait);
        } else {
            window.location.href = app.baseUrl;
        }
    }, 'json').error(function () {
        if (app.sessionErrorAttempting) {
            app.sessionErrorWait = (app.sessionErrorWait * 2);
        } else {
            window.setTimeout(app.sessionError, app.sessionErrorWait);
        }
        window.setTimeout(app.keepAlive, app.sessionErrorWait);
    });
};
app.sessionError = function () {
    app.sessionErrorAttempting = true;
    if (!app.sessionAlertPop) {
        app.sessionAlertPop = app.sessionAlertCreate();
        $('body').append(app.sessionAlertPop);
    }
    app.sessionAlertPop.modal('show');
};
app.sessionAlertCreate = function () {
    return $(app.popCreate({
        body: '<p>Attempting to restore connection. Please wait...</p>',
        title: 'Connection Error',
        isStatic: true
    }));
};
app.popCreate = function (p) {
    modal = '<div class="modal fade" ' + (p.isStatic ? 'data-backdrop="static"' : '') + '>';
    modal += '<div class="modal-dialog">';
    modal += '<div class="modal-content main">';
    modal += '<div class="modal-header">';
    modal += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
    modal += '<h4 class="modal-title">' + (p.title ? p.title : '') + '</h4>';
    modal += '</div>';
    modal += '<div class="modal-body">' + (p.body ? p.body : '') + '</div>';
    if (p.footerBtn) {
        modal += '<div class="modal-footer"><button type="button" class="btn btn-blue" data-dismiss="modal">Cancel</button><button type="button" class="btn btn-default" data-dismiss="modal" onclick="' + p.footerBtnClick + '">' + p.footerBtn + '</button></div>';
    }
    modal += '</div></div></div>';
    return $(modal);
};
app.sessionRestored = function () {
    app.sessionErrorAttempting = false;
    app.sessionAlertPop.modal('hide');
};

$(document).ready(function () {
    var datatableobjs = $('.datatable_noconfig');
    if (datatableobjs.length) {
        datatableobjs.DataTable();
    }
    $('input').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
    });
    $('#user_active').on('ifToggled', function(event){
        $('input[name="user_active"]').val($('#user_active').parent().hasClass('checked') === true ? '0' : '1');
    });
    $(document).on("click", "a[data-toggle='popover']", function(e) {
        $(e.target).popover('show');
    });
    if ($('.account-list').length) {
        $('.account-list').DataTable().order([3, 'desc']).draw();
    }
});

app.sendActivationEmail = function(){
    $.ajax({
        url: 'user/resend_activation_email',
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            if (data.status && data.status === 'success') {
                $('#send-email').find('a').remove();
                $('#send-email').append('<p>Please check your inbox, we have sent you an e-mail</p>');
            } else {
                $('#send-email').find('a').remove();
                $('#send-email').append('<p>An error occured, please try again later</p>');
            }
        }
    });
};

app.deleteUserConfirm = function(userId, text) {
    var modal = $('#delete-user');
    modal.find('.modal-body p').text('Seguro quiere eliminar al usuario ' + text);
    modal.find('.btn-primary').attr('href', 'user_remove/' + userId);
    modal.show();
};
app.closeDeleteUserConfirm = function() {
    var modal = $('#delete-user');
    modal.find('.modal-body p').text('');
    modal.find('.btn-primary').attr('href', '');
    modal.hide();
};
app.deleteAgentConfirm = function(userId, text) {
    var modal = $('#delete-agent');
    modal.find('.modal-body p').text('Seguro quiere eliminar al agente ' + text);
    modal.find('.btn-primary').attr('href', 'agent_remove/' + userId);
    modal.show();
};
app.closeDeleteAgentConfirm = function() {
    var modal = $('#delete-agent');
    modal.find('.modal-body p').text('');
    modal.find('.btn-primary').attr('href', '');
    modal.hide();
};
app.deleteApartmentConfirm = function(userId, text) {
    var modal = $('#delete-apartment');
    modal.find('.modal-body p').text('Seguro quiere eliminar el departamento ' + text);
    modal.find('.btn-primary').attr('href', 'apartment_remove/' + userId);
    modal.show();
};
app.closeDeleteApartmentConfirm = function() {
    var modal = $('#delete-apartment');
    modal.find('.modal-body p').text('');
    modal.find('.btn-primary').attr('href', '');
    modal.hide();
};
app.deleteAccountDetailConfirm = function(agentId, userId) {
    var modal = $('#delete-account-detail');
    modal.find('.modal-body p').text('Seguro quiere eliminar el detalle de la cuenta');
    modal.find('.btn-primary').attr('href', '../account_detail_remove/' + agentId + '/'+ userId);
    modal.show();
};
app.closeAccountDetailConfirm = function() {
    var modal = $('#delete-account-detail');
    modal.find('.modal-body p').text('');
    modal.find('.btn-primary').attr('href', '');
    modal.hide();
};
app.showReservationDetails = function(resNumber) {
    var modal = $('#reservation-details');
    modal.find('h4').text('Reserva ' + resNumber);
    modal.find('.overlay').show();
    modal.show();
    $.ajax({
        url: app.baseUrl + 'manager/get_reservation_details',
        data: {reservation: resNumber},
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            if (data.status && data.status === 'success') {
                modal.find('.modal-body').append("<p><strong>Appartment:</strong> " + data.reservation.room + "</p>");
                modal.find('.modal-body').append("<p><strong>Guest 1:</strong> " + data.reservation.guest_first_name + " " + data.reservation.guest_last_name + "</p>");
                modal.find('.modal-body').append("<p><strong>Guest 2:</strong> " + data.reservation.second_guest_first_name + " " + data.reservation.second_guest_last_name + "</p>");
                modal.find('.modal-body').append("<p><strong>Arrival:</strong> " + data.reservation.arrival_date + "</p>");
                modal.find('.modal-body').append("<p><strong>Departure:</strong> " + data.reservation.departure_date + "</p>");
                modal.find('.modal-body').append("<p><strong>Room price:</strong> " + data.reservation.total_room_price + "</p>");
                modal.find('.modal-body').append("<p><strong>Discounts:</strong> " + data.reservation.discounts + "</p>");
                modal.find('.modal-body').append("<p><strong>Fees:</strong> " + data.reservation.fees + "</p>");
                modal.find('.modal-body').append("<p><strong>Items for sale:</strong> " + data.reservation.items_for_sale + "</p>");
                modal.find('.modal-body').append("<p><strong>Total:</strong> " + data.reservation.total + "</p>");
                modal.find('.modal-body').append("<p><strong>Payments:</strong> " + data.reservation.payments + "</p>");
                modal.find('.modal-body').append("<p><strong>Balance:</strong> " + data.reservation.balance + "</p>");
                modal.find('.modal-body').append("<p><strong>Expenses:</strong> " + data.reservation.expenses + "</p>");
            } else {
                modal.find('.moda-body').html('<p>No se pudo obtener los datos de la reserva.</p>');
            }
            modal.find('.overlay').hide();
        },
        error: function () {
            modal.find('.moda-body').html('<p>Error al obtener los datos de la reserva.</p>');
            modal.find('.overlay').hide();
        }
    });
};
app.closeReservationDetails = function() {
    var modal = $('#reservation-details');
    modal.find('h4').text('');
    modal.find('.modal-body').html('');
    modal.hide();
};

$(function () {
  $('[data-toggle="tooltip"]').tooltip();
});