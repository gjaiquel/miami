var app = app || {};
app.resetPassword = function() {
    var password = $('[name="password"').val();
    var password_repeat = $('[name="password_repeat"').val();
    var user_id = $('[name="user_id"').val();
    var reset_hash = $('[name="reset_hash"').val();
    
    $('.alert').remove();
    
    if($.trim(password) === '' || $.trim(password_repeat) === ''){
        $('.register-box-body').append('<div class="alert alert-danger">Por favor, escribe la contraseña</div>');
        return;
    }
    if(password !== password_repeat){
        $('.register-box-body').append('<div class="alert alert-danger">Las contraseñas ingresadas no coinciden</div>');
        return;
    }
    
    $.ajax({
        url: app.baseUrl + 'user/set_password',
        data: {password: password, password_repeat: password_repeat, user_id: user_id, reset_hash: reset_hash},
        type: 'POST',
        dataType: 'json',
        success: function(data) {
            if(data.status && data.status === 'success') {
                $('.register-box-body').append('<div class="alert alert-success">Contraseña actualizada correctamente</div>');
                $('.btn-primary').attr('disabled', 'disabled');
            } else {
                if(data.errors){
                    $('.register-box-body').append('<div class="alert alert-danger">'+data.errors+'</div>');
                } else {
                    $('.register-box-body').append('<div class="alert alert-danger">Algo ha fallado, intenta nuevamente por favor</div>');
                }
            }
        }
    });
};
