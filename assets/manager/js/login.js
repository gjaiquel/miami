var app = app || {};

app.login = function () {
    var username = $('[name="user"]').val();
    var password = $('[name="password"]').val();

    if ($.trim(username) === '' || $.trim(password) === '') {
        return;
    }

    $('.login-box-body').remove('.alert');
    $.ajax({
        url: 'user/get_login',
        data: {user: username, password: password, createcookie: $('[name="createcookie"]').is(':checked') ? 1 : 0},
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            if (data.status && data.status === 'success') {
                window.location.href = app.baseUrl + 'manager';
            } else {
                $('.alert.alert-danger').remove();
                if (data.errors) {
                    $('.login-box-body').append('<div class="alert alert-danger">' + data.errors + '</div>');
                } else {
                    $('.login-box-body').append('<div class="alert alert-danger">Hubo un error. Intente de nuevo</div>');
                }
            }
        }
    });
};
app.openForgotModal = function() {
    var modal = $('#forgot-password');
    var emailInput = $('#email');
    emailInput.val('');
    modal.show();
    emailInput.focus();
};
app.closeForgotModal = function() {
    var modal = $('#forgot-password');
    modal.hide();
};
app.forgotPassword = function () {
    var forgot_modal = $('#forgot-password');
    var email = forgot_modal.find('[name="email"]').val();

    if ($.trim(email) === '') {
        forgot_modal.find('.modal-body').append('<div class="alert alert-danger">Escriba su email.</div>');
        return;
    }

    forgot_modal.find('.alert').remove();
    $.ajax({
        url: 'user/get_password_reset',
        data: {email: email},
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            if (data.status && data.status === 'success') {
                forgot_modal.find('.modal-body').append('<div class="alert alert-success">Un email ha sido enviado a su casilla</div>');
            } else {
                if (data.errors) {
                    forgot_modal.find('.modal-body').append('<div class="alert alert-danger">' + data.errors + '</div>');
                } else {
                    forgot_modal.find('.modal-body').append('<div class="alert alert-danger">Algo ha fallado, intente nuevamente</div>');
                }
            }
        }
    });
};