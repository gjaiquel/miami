var app = app || {};

app.checkDatesValues = function () {
    var val1 = $(".datepicker:nth(0)").val();
    var val2 = $(".datepicker:nth(1)").val();
    var arr1 = val1.split("/");
    var arr2 = val2.split("/");
    var ret = true;
    
    if(arr1.length === 3 && arr2.length === 3) {
        val1 = arr1[1] + "/" + arr1[0] + "/" + arr1[2];
        val2 = arr2[1] + "/" + arr2[0] + "/" + arr2[2];
        
        val1 = Date.parse(val1);
        val2 = Date.parse(val2);
        if (isNaN(val1)===true && val!==''){
            $('.form .form-group:nth(0)').addClass('has-error');
            ret = false;
        } else {
            $('.form .form-group:nth(0)').removeClass('has-error');
        }
        if (isNaN(val2)===true && val!==''){
            $('.form .form-group:nth(1)').addClass('has-error');
            ret = false;
        } else {
            $('.form .form-group:nth(1)').removeClass('has-error');
        }
        if(val1 > val2) {
            $('.form .form-group:nth(0)').addClass('has-error');
            $('.form .form-group:nth(1)').addClass('has-error');
            ret = false;
        }
    } else {
        if(arr1.length !== 3) {
            $('.form .form-group:nth(0)').addClass('has-error');
        }
        if(arr2.length !== 3) {
            $('.form .form-group:nth(1)').addClass('has-error');
        }
        ret = false;
    }
    return ret;
};

$(document).ready(function () {
    $('.datepicker').datepicker();
    $('.form').submit(function(event){
        if(app.checkDatesValues() === false) {  
            event.preventDefault();
        }
    });
    $('#email-btn').click(function() {
        var thiz = $(this);
        var text = $(this).text();
        var agentId = $(this).attr('data-agent');
        var from = $(this).attr('data-from');
        var to = $(this).attr('data-to');
        $('#popup').html('');
        $(this).text('Enviando...');
        $(this).attr('disabled', true);
        $.ajax({
            url: app.baseUrl + 'manager/send_account_resume_email/' + agentId,
            data: {from: from, to: to},
            type: 'POST',
            dataType: 'json'
        }).success(function(data){
            if(data.sent) {
                thiz.text('Enviado');
                app.showPopup('alert-success', 'Enviado!', 'El email ha sido enviado.');
            } else {
                thiz.text(text);
                app.showPopup('alert-danger', 'Error!', 'No se pudo enviar el e-mail, intente de nuevo.');
                thiz.attr('disabled', false);
            }
        }).fail(function() {
            thiz.text(text);
            app.showPopup('alert-danger', 'Error!', 'No se pudo enviar el e-mail, intente de nuevo.');
            thiz.attr('disabled', false);
        });
    });
});
app.showPopup = function(clazz, title, text) {
    var html = '<div class="alert '+clazz+' alert-dismissable" id="success-popup" style="padding: 10px;position: fixed;top: 70px;right: 10px;width: 300px;z-index: 99999;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="position: initial;">×</button><h4>	<i class="icon fa fa-check"></i> '+title+'</h4>'+text+'</div>';
    $('#popup').html(html);
};