// Version: 0.[(yyyy-2016)mmdd].[hhmm]
var mteApp = mteApp || {};
console.log("MTE ReservationKey extension loaded");
mteApp.init = function() {
    var notesTextarea = document.getElementById("notes");
    var mteRows = document.getElementsByClassName('mte-row');
    var existingNote = '';
    if(notesTextarea !== undefined && notesTextarea !== null) {
        existingNote = notesTextarea.value;
    }
    var json = {};
    var loadData = true;
    if(existingNote.length > 0) {
        try {
            json = JSON.parse(existingNote);
        } catch(e) {
            loadData = false;
        }
    }
    if(loadData && notesTextarea !== undefined && notesTextarea !== null && mteRows.length === 0) {

        document.getElementById("ccformrow1").getElementsByClassName("small")[0].innerText = "Pasajero: First";
        document.getElementById("ccformrow2").getElementsByClassName("small")[0].innerText = "Agente";
        document.getElementById("ccformrow2").getElementsByClassName("small")[1].innerText = "Agente (nota opcional)";

        var container = notesTextarea.parentNode;
        notesTextarea.style.display = 'none';
        var row = mteApp.getDiv('', 'mte-row title-1', 'MTE Datos', undefined);
        container.appendChild(row);



        var input = mteApp.getInput('number', '0.01', '', 'mte-night-1-value', json.nightOneValue || '', 'textbox form-control mte-input');
        var title = mteApp.getDiv('', 'small', 'Valor Noche 1 (usd)', undefined);
        var innerRow = mteApp.getDiv('', 'mte-td-left', '', title);
        innerRow.appendChild(input);
        row = mteApp.getDiv('', 'mte-row', '', innerRow);
        container.appendChild(row);

        input = mteApp.getInput('number', '0.01', '', 'mte-night-2-value', json.nightTwoValue || '', 'textbox form-control mte-input');
        title = mteApp.getDiv('', 'small', 'Valor Noche 2 (usd)', undefined);
        innerRow = mteApp.getDiv('', 'mte-td-left', '', title);
        innerRow.appendChild(input);
        row = mteApp.getDiv('', 'mte-row', '', innerRow);
        input = mteApp.getInput('number', '', '', 'mte-night-2-days', json.nightsTwoCount || '', 'textbox form-control mte-input');
        title = mteApp.getDiv('', 'small', 'Dias a valor 2', undefined);
        innerRow = mteApp.getDiv('', 'mte-td-right', '', title);
        innerRow.appendChild(input);
        row.appendChild(innerRow);
        container.appendChild(row);
        
        input = mteApp.getInput('number', '0.01', '', 'mte-night-3-value', json.nightThreeValue || '', 'textbox form-control mte-input');
        title = mteApp.getDiv('', 'small', 'Valor Noche 3 (usd)', undefined);
        innerRow = mteApp.getDiv('', 'mte-td-left', '', title);
        innerRow.appendChild(input);
        row = mteApp.getDiv('', 'mte-row', '', innerRow);
        input = mteApp.getInput('number', '', '', 'mte-night-3-days', json.nightsThreeCount || '', 'textbox form-control mte-input');
        title = mteApp.getDiv('', 'small', 'Dias a valor 3', undefined);
        innerRow = mteApp.getDiv('', 'mte-td-right', '', title);
        innerRow.appendChild(input);
        row.appendChild(innerRow);
        container.appendChild(row);

        input = mteApp.getInput('text', '', '', 'mte-commission-agent', json.commissionAgent || '', 'textbox form-control mte-input');
        title = mteApp.getDiv('', 'small', 'Comisionista', undefined);
        innerRow = mteApp.getDiv('', 'mte-td-left', '', title);
        innerRow.appendChild(input);
        row = mteApp.getDiv('', 'mte-row', '', innerRow);
        container.appendChild(row);

        input = mteApp.getInput('number', '0.01', '', 'mte-earlier', json.earlier || '', 'textbox form-control mte-input');
        title = mteApp.getDiv('', 'small', 'Earlier check in (usd)', undefined);
        innerRow = mteApp.getDiv('', 'mte-td-left', '', title);
        innerRow.appendChild(input);
        row = mteApp.getDiv('', 'mte-row', '', innerRow);
        input = mteApp.getInput('number', '0.01', '', 'mte-late', json.late || '', 'textbox form-control mte-input');
        title = mteApp.getDiv('', 'small', 'Late check out (usd)', undefined);
        innerRow = mteApp.getDiv('', 'mte-td-right', '', title);
        innerRow.appendChild(input);
        row.appendChild(innerRow);
        container.appendChild(row);
        
        row = mteApp.getDiv('', 'mte-row title-2', 'Payments', undefined);
        container.appendChild(row);

        var aux = json.payments && json.payments[0] && json.payments[0].agent;
        input = mteApp.getInput('text', '', '', 'mte-payment-agent', aux || '', 'textbox form-control mte-input');
        title = mteApp.getDiv('', 'small', 'Agente', undefined);
        innerRow = mteApp.getDiv('', 'mte-td-left col-3', '', title);
        innerRow.appendChild(input);
        row = mteApp.getDiv('mte-payments', 'mte-row', '', innerRow);
        aux = json.payments && json.payments[0] && json.payments[0].detail;
        input = mteApp.getInput('text', '', '', 'mte-payment-detail', aux || '', 'textbox form-control mte-input');
        title = mteApp.getDiv('', 'small', 'Detalle', undefined);
        innerRow = mteApp.getDiv('', 'mte-td-left col-3', '', title);
        innerRow.appendChild(input);
        row.appendChild(innerRow);
        aux = json.payments && json.payments[0] && json.payments[0].value;
        input = mteApp.getInput('number', '0.01', '', 'mte-payment-value', aux || '', 'textbox form-control mte-input');
        title = mteApp.getDiv('', 'small', 'Valor', undefined);
        innerRow = mteApp.getDiv('', 'mte-td-right col-3', '', title);
        innerRow.appendChild(input);
        row.appendChild(innerRow);
        container.appendChild(row);

        if(json.payments && json.payments.length > 1) {
            for(var i = 1; i < json.payments.length; i++) {
                mteApp.addPaymentRow(json.payments[i].agent, json.payments[i].detail, json.payments[i].value);
            }
        }

        var button = document.createElement('button');
        button.innerText = 'Agregar payment';
        button.addEventListener("click", function(event){
            event.preventDefault();
            mteApp.addPaymentRow('', '', '');
        });
        innerRow = mteApp.getDiv('', 'mte-td-left', '', button);
        row = mteApp.getDiv('', 'mte-row', '', innerRow);
        container.appendChild(row);
        
        row = mteApp.getDiv('', 'mte-row title-2', 'Gastos', undefined);
        container.appendChild(row);

        aux = json.expenses && json.expenses[0] && json.expenses[0].paidBy;
        input = mteApp.getInput('text', '', '', 'mte-expense-paid-by', aux || '', 'textbox form-control mte-input');
        title = mteApp.getDiv('', 'small', 'Pagado por', undefined);
        innerRow = mteApp.getDiv('', 'mte-td-left col-4', '', title);
        innerRow.appendChild(input);
        row = mteApp.getDiv('mte-expenses', 'mte-row', '', innerRow);
        aux = json.expenses && json.expenses[0] && json.expenses[0].ofAgent;
        input = mteApp.getInput('text', '', '', 'mte-expense-of-agent', aux || '', 'textbox form-control mte-input');
        title = mteApp.getDiv('', 'small', 'Gasto de', undefined);
        innerRow = mteApp.getDiv('', 'mte-td-left col-4', '', title);
        innerRow.appendChild(input);
        row.appendChild(innerRow);
        aux = json.expenses && json.expenses[0] && json.expenses[0].detail;
        input = mteApp.getInput('text', '', '', 'mte-expense-detail', aux || '', 'textbox form-control mte-input');
        title = mteApp.getDiv('', 'small', 'Detalle', undefined);
        innerRow = mteApp.getDiv('', 'mte-td-left col-4', '', title);
        innerRow.appendChild(input);
        row.appendChild(innerRow);
        aux = json.expenses && json.expenses[0] && json.expenses[0].value;
        input = mteApp.getInput('number', '0.01', '', 'mte-expense-value', aux || '', 'textbox form-control mte-input');
        title = mteApp.getDiv('', 'small', 'Valor', undefined);
        innerRow = mteApp.getDiv('', 'mte-td-right col-4', '', title);
        innerRow.appendChild(input);
        row.appendChild(innerRow);
        container.appendChild(row);

        if(json.expenses && json.expenses.length > 1) {
            for(var i = 1; i < json.expenses.length; i++) {
                mteApp.addExtraExpenseRow(json.expenses[i].paidBy, json.expenses[i].ofAgent, json.expenses[i].detail, json.expenses[i].value);
            }
        }

        button = document.createElement('button');
        button.innerText = 'Agregar gasto';
        button.addEventListener("click", function(event){
            event.preventDefault();
            mteApp.addExtraExpenseRow('', '', '', '');
        });
        innerRow = mteApp.getDiv('', 'mte-td-left', '', button);
        row = mteApp.getDiv('', 'mte-row', '', innerRow);
        container.appendChild(row);

        input = mteApp.getInput('number', '0.01', '', 'mte-canceled-price', json.canceledPrice || '', 'textbox form-control mte-input');
        title = mteApp.getDiv('', 'small', 'Precio por cancelación para el dueño', undefined);
        innerRow = mteApp.getDiv('', 'mte-td-left', '', title);
        innerRow.appendChild(input);
        row = mteApp.getDiv('', 'mte-row', '', innerRow);
        container.appendChild(row);

        row = mteApp.getDiv('', 'mte-row title-2', 'OTROS Departamentos', undefined);
        container.appendChild(row);
        
        input = mteApp.getInput('text', '', '', 'mte-other-owner', json.otherOwner || '', 'textbox form-control mte-input');
        title = mteApp.getDiv('', 'small', 'Propietario', undefined);
        innerRow = mteApp.getDiv('', 'mte-td-left col-3', '', title);
        innerRow.appendChild(input);
        row = mteApp.getDiv('', 'mte-row', '', innerRow);
        input = mteApp.getInput('text', '', '', 'mte-other-checkin', json.otherCheckin || '', 'textbox form-control mte-input');
        title = mteApp.getDiv('', 'small', 'Check-In', undefined);
        innerRow = mteApp.getDiv('', 'mte-td-left col-3', '', title);
        innerRow.appendChild(input);
        row.appendChild(innerRow);
        input = mteApp.getInput('text', '', '', 'mte-other-name', json.otherName || '', 'textbox form-control mte-input');
        title = mteApp.getDiv('', 'small', 'Nombre departamento', undefined);
        innerRow = mteApp.getDiv('', 'mte-td-right col-3', '', title);
        innerRow.appendChild(input);
        row.appendChild(innerRow);
        container.appendChild(row);
    }
};

mteApp.addPaymentRow = function(agent, detail, value) {
    var container = document.getElementById('mte-payments');
    var input = mteApp.getInput('text', '', '', 'mte-payment-agent', agent || '', 'textbox form-control mte-input');
    var innerRow = mteApp.getDiv('', 'mte-td-left col-3', '', input);
    container.appendChild(innerRow);
    input = mteApp.getInput('text', '', '', 'mte-payment-detail', detail || '', 'textbox form-control mte-input');
    innerRow = mteApp.getDiv('', 'mte-td-left col-3', '', input);
    container.appendChild(innerRow);
    input = mteApp.getInput('number', '0.01', '', 'mte-payment-value', value || '', 'textbox form-control mte-input');
    innerRow = mteApp.getDiv('', 'mte-td-right-sm col-3', '', input);
    container.appendChild(innerRow);
    var button = document.createElement('button');
    button.innerText = 'X';
    button.addEventListener("click", function(event){
        event.preventDefault();
        mteApp.removeRow(event.target, 3);

    });
    innerRow = mteApp.getDiv('', 'mte-td-right-btn', '', button);
    container.appendChild(innerRow);
	
};

mteApp.addExtraExpenseRow = function(paidBy, ofAgent, detail, value) {
    var container = document.getElementById('mte-expenses');
    var input = mteApp.getInput('text', '', '', 'mte-expense-paid-by', paidBy || '', 'textbox form-control mte-input');
    var innerRow = mteApp.getDiv('', 'mte-td-left col-4', '', input);
    container.appendChild(innerRow);
    input = mteApp.getInput('text', '', '', 'mte-expense-of-agent', ofAgent || '', 'textbox form-control mte-input');
    innerRow = mteApp.getDiv('', 'mte-td-left col-4', '', input);
    container.appendChild(innerRow);
    input = mteApp.getInput('text', '', '', 'mte-expense-detail', detail || '', 'textbox form-control mte-input');
    innerRow = mteApp.getDiv('', 'mte-td-left col-4', '', input);
    container.appendChild(innerRow);
    input = mteApp.getInput('number', '0.01', '', 'mte-expense-value', value || '', 'textbox form-control mte-input');
    innerRow = mteApp.getDiv('', 'mte-td-right-sm col-4', '', input);
    container.appendChild(innerRow);
    var button = document.createElement('button');
    button.innerText = 'X';
    button.addEventListener("click", function(event){
        event.preventDefault();
        mteApp.removeRow(event.target, 4);

    });
    innerRow = mteApp.getDiv('', 'mte-td-right-btn', '', button);
    container.appendChild(innerRow);
};

mteApp.removeRow = function(btn, count) {
    for(var i = 0; i < count; i++) {
        btn.parentElement.previousSibling.outerHTML = "";
    }
    btn.parentElement.outerHTML = "";
    mteApp.inputToJson();
};

mteApp.getDiv = function(id, clazz, text, child) {
    var div = document.createElement('div');
    div.id = id;
    div.className = clazz;
    div.innerText = text;
    if(child) {
        div.appendChild(child);
    }
    return div;
};

mteApp.getInput = function(type, step, placeholder, name, value, clazz) {
    var input = document.createElement('input');
    input.type = type;
    input.step = step;
    input.placeholder = placeholder;
    input.name = name;
    input.value = value;
    input.className = clazz;
    input.addEventListener("blur", function(event) {
        mteApp.inputToJson(event);
    });
    return input;
};

mteApp.inputToJson = function() {
    var json = {};
    var notesTextarea = document.getElementById("notes");
    var night1 = document.getElementsByName('mte-night-1-value')[0].value;
    json.nightOneValue = night1;
    var night2 = document.getElementsByName('mte-night-2-value')[0].value;
    json.nightTwoValue = night2;
    var days2 = document.getElementsByName('mte-night-2-days')[0].value;
    json.nightsTwoCount = days2;
    var night3 = document.getElementsByName('mte-night-3-value')[0].value;
    json.nightThreeValue = night3;
    var days3 = document.getElementsByName('mte-night-3-days')[0].value;
    json.nightsThreeCount = days3;
    var commissionAgent = document.getElementsByName('mte-commission-agent')[0].value;
    json.commissionAgent = commissionAgent;
    var earlier = document.getElementsByName('mte-earlier')[0].value;
    json.earlier = earlier;
    var late = document.getElementsByName('mte-late')[0].value;
    json.late = late;

    var inputs = document.getElementsByName('mte-payment-agent');
    var payments = [];
    for(var i=0; i < inputs.length; i++) {
        var payment = {};
        payment.agent = inputs[i].value;
        payments.push(payment);
    }
    inputs = document.getElementsByName('mte-payment-detail');
    for(var i=0; i < inputs.length; i++) {
        payments[i].detail = inputs[i].value;
    }
    inputs = document.getElementsByName('mte-payment-value');
    for(var i=0; i < inputs.length; i++) {
        payments[i].value = inputs[i].value;
    }
    json.payments = payments;

    inputs = document.getElementsByName('mte-expense-paid-by');
    var expenses = [];
    for(var i=0; i < inputs.length; i++) {
        var expense = {};
        expense.paidBy = inputs[i].value;
        expenses.push(expense);
    }
    inputs = document.getElementsByName('mte-expense-of-agent');
    for(var i=0; i < inputs.length; i++) {
        expenses[i].ofAgent = inputs[i].value;
    }
    inputs = document.getElementsByName('mte-expense-detail');
    for(var i=0; i < inputs.length; i++) {
        expenses[i].detail = inputs[i].value;
    }
    inputs = document.getElementsByName('mte-expense-value');
    for(var i=0; i < inputs.length; i++) {
        expenses[i].value = inputs[i].value;
    }
    json.expenses = expenses;

    var canceledPrice = document.getElementsByName('mte-canceled-price')[0].value;
    json.canceledPrice = canceledPrice;

    var owner = document.getElementsByName('mte-other-owner')[0].value;
    json.otherOwner = owner;

    var checkin = document.getElementsByName('mte-other-checkin')[0].value;
    json.otherCheckin = checkin;

    var otherName = document.getElementsByName('mte-other-name')[0].value;
    json.otherName = otherName;

    notesTextarea.value = JSON.stringify(json);
};

setInterval(mteApp.init, 1000);
